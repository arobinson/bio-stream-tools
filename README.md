# Bio Stream Tools #

A core set of C++ bioinformatics tools that interconnect using unix/linux pipes.  Uses lib-bio-streams library to provide much of the streaming and command line argument interaction.

## Tools ##

There are a number of tools within this package.  See the following links for documentation on these tools:

1. **Sequence Tools** [https://bitbucket.org/arobinson/bio-stream-tools/src/HEAD/cppsrc/biostreamtools/sequence/README.md?at=master]
    1. *deinterleave-seq-pairs*: merges two sequence files into an alternating stream of sequences
    2. *interleave-seq-pairs*: separates a stream of sequences into two files (i.e. odd-numbered in one, even in another)
    3. *nextseq-trim*: trims the AAAAGGGG tails from poor NextSeqTM reads (with mismatches).
    4. *reverse-complement*: computes the reverse AND/OR complement of reads

## Install ##

1. Install prerequisites:
    1. **lib-bio-streams** (https://bitbucket.org/arobinson/bio-stream-tools/overview)
    2. **cmake**: should come with your computer. (http://www.cmake.org/)
2. Download source: (https://bitbucket.org/arobinson/bio-stream-tools/downloads?tab=tags)
    1. export VERSION=0.5.2 #(update number as needed)
    2. wget https://bitbucket.org/arobinson/bio-stream-tools/get/v$VERSION.tar.gz -O biostreamtools_v$VERSION.tar.gz
3. Build:
    1. tar xf biostreamtools_v$VERSION.tar.gz
    2. mv arobinson-lib-bio-streams-* biostreamtools_v$VERSION
    3. cd biostreamtools_v$VERSION
    4. cmake ..
        1. Check that it found libbiostreams.  If not, you may need to help cmake find it with (e.g. -DCMAKE_FIND_ROOT_PATH="PATH_TO_LIB_ROOT")
        2. If you want to install bio-stream-tools in an alternate PATH (i.e. not /usr/local) add: -DCMAKE_INSTALL_PREFIX=PATH
    5. make
    6. make install