/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <iostream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>
#include <libbiostreams/seq/complement.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, "Print configuration options on log (and continue)");

	conf->addOption("size", 		's', 	new std::string("size"), 				0, 1, new std::string("10"),	"The size of kmers to count  [default: 10]");
//	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"The format received on input:   [default: AUTO]\n AUTO: detect from first character\n FASTQ\n FASTA\n RAW: one sequence per line\n RAWM: single multi-line raw sequence");
//	conf->addOption("format-out", 	'\0', 	new std::string("format-out"), 			0, 1, new std::string("AUTO"),	"Output format                   [default: AUTO]\n AUTO: match '--format' option");

	conf->addInStream("input", 		'i', 	new std::string("input"), 				0, 1, new std::string("-"), 	"Input sequences              [default: -]\n -: standard input");
	conf->addOutStream("count", 	'c', 	new std::string("count"), 				0, 1, new std::string("-"),		"Count summary                [default: -]\n -: standard output\n =: standard error");
	conf->addLogStream("log", 		'l', 	new std::string("log"), 				0, 1, new std::string("="),		"Log message stream/file      [default: =]\n -: standard output\n =: standard error");

	conf->setPreamble("Compute the Reverse AND/OR Complement of Sequences");
	conf->setPostamble("Developed by Andrew Robinson");

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 2;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("count");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
	short size = conf->getOptionInt("size");
	if (size < 1)
		size = 1;
	if (size > 10)
		size = 10;

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	Sequence *seq = NULL;
	SequenceReader *sr = new SequenceReader(ins);
//	sr->setType(conf->getOptionString("format"));

	// create the mask
	unsigned mask = 0;
	for (size_t k = 0; k < size; ++k) {
		mask <<= 1;
		mask += 1;
		mask <<= 1;
		mask += 1;
		mask <<= 1;
		mask += 1;
	}
	*logs << "Size: " << size << std::endl;
	*logs << "Mask: " << (unsigned long) mask << std::endl;

	// create the base to bits mapping
	const char *bitmask = "44444444444444444444444444444444444444444444444444444444444444444041444244444444444434444444444440414442444444444444344444444444";
	int8_t base2bits[128];
	for (size_t b = 0; b < 128; ++b)
		base2bits[b] = bitmask[b] - '0';

	const char *bits2base = "ACGTN---";

	unsigned *counts = new unsigned[mask+1];
	memset(counts, 0, (mask+1) * sizeof(unsigned));

	// loop over sequences
	while ((seq = (Sequence*)sr->next()) != NULL) {

		unsigned kmerId = 0;
		const char* s = seq->seq.c_str();

//		*logs << "Processing: " << s << std::endl;

		for (size_t i = 0; i < seq->seq.size(); ++i) {

			kmerId <<= 3; // move last base right

			kmerId += base2bits[s[i]]; // add new base

			// full kmer?
			if (i >= size) {
				kmerId &= mask; // trim off remaining bits (from previous kmers)
				counts[kmerId] += 1;
			}
		}
		delete seq;
	}

	// print out counts
	*logs << "Counts: " << std::endl;
	unsigned uniquekmer = 0;
	for (size_t i = 0; i <= mask; ++i) {
		if (counts[i] != 0) {
			unsigned ci = i;
			for (size_t j = 0; j < size; ++j) {
				*logs << (char)bits2base[ci & 7]; // << (char)' ' << (unsigned long)ci << (char)' ' << (unsigned long)(ci & 7)<< (char)' ';
				ci >>= 3;
			}
			*logs << " (" << i << "): " << (unsigned long)counts[i] << std::endl;
			uniquekmer += 1;
		}
	}
	*logs << "Total kmers: " << (unsigned long) uniquekmer << std::endl;


//	std::string mode = conf->getOptionString("mode");
//	if (mode.compare("rc") == 0 || mode.compare("cr") == 0) {
//		while ((seq = (Sequence*)sr->next()) != NULL) {
//			reverseComplement(seq);
//			*outs << seq;
//			count++;
//			delete seq;
//		}
//		*logs << "Reverse-complemented " << count << " sequences";
//	} else if (mode.compare("r") == 0) {
//		while ((seq = (Sequence*)sr->next()) != NULL) {
//			reverse(seq);
//			*outs << seq;
//			count++;
//			delete seq;
//		}
//		*logs << "Reversed " << count << " sequences";
//	} else if (mode.compare("c") == 0) {
//		while ((seq = (Sequence*)sr->next()) != NULL) {
//			complement(seq);
//			*outs << seq;
//			count++;
//			delete seq;
//		}
//		*logs << "Complemented " << count << " sequences";
//	} else {
//		*logs << "Unknown mode: '" << mode << "'";
//		logs->endl();
//		return 4;
//	}

	delete counts;
	delete sr;
	delete conf;

    return 0;
}

