/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <iostream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>
#include <libbiostreams/seq/complement.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, "Print configuration options on log (and continue)");

	conf->addOption("mode", 		'm', 	new std::string("mode"), 				0, 1, new std::string("rc"),	"The functions to perform:         [default: rc]\n r:  reverse,\n c:  complement,\n rc: reverse & complement");
	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"The format received on input:   [default: AUTO]\n AUTO: detect from first character\n FASTQ\n FASTA\n RAW: one sequence per line\n RAWM: single multi-line raw sequence");
	conf->addOption("format-out", 	'\0', 	new std::string("format-out"), 			0, 1, new std::string("AUTO"),	"Output format                   [default: AUTO]\n AUTO: match '--format' option");

	conf->addInStream("input", 		'i', 	new std::string("input"), 				0, 1, new std::string("-"), 	"Input stream/file            [default: -]\n -: standard input");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),		"Output stream/file           [default: -]\n -: standard output\n =: standard error");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),		"Log message stream/file      [default: =]\n -: standard output\n =: standard error");

	conf->setPreamble("Compute the Reverse AND/OR Complement of Sequences");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 2;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
	std::string format = conf->getOptionString("format");
	if (format != "AUTO" &&
			format != "FASTQ" &&
			format != "FASTA" &&
			format != "RAW" &&
			format != "RAWM") {
		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
		format = "AUTO";
	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	Sequence *seq = NULL;
	SequenceReader *sr = new SequenceReader(ins);
	sr->setType(format);
	unsigned long count = 0;

	std::string mode = conf->getOptionString("mode");
	if (mode.compare("rc") == 0 || mode.compare("cr") == 0) {
		while ((seq = (Sequence*)sr->next()) != NULL) {
			reverseComplement(seq);
			*outs << seq;
			count++;
			delete seq;
		}
		*logs << "Reverse-complemented " << count << " sequences";
	} else if (mode.compare("r") == 0) {
		while ((seq = (Sequence*)sr->next()) != NULL) {
			reverse(seq);
			*outs << seq;
			count++;
			delete seq;
		}
		*logs << "Reversed " << count << " sequences";
	} else if (mode.compare("c") == 0) {
		while ((seq = (Sequence*)sr->next()) != NULL) {
			complement(seq);
			*outs << seq;
			count++;
			delete seq;
		}
		*logs << "Complemented " << count << " sequences";
	} else {
		*logs << "Unknown mode: '" << mode << "'";
		logs->endl();
		return 4;
	}
	delete sr;
	logs->endl();
	delete conf;

    return 0;
}

