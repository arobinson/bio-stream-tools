/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <iostream>
#include <vector>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/io/PlainInput.h>
#include <libbiostreams/readers/SequenceReader.h>
#include <libbiostreams/seq/complement.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);

	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),	"Status messages (warning, errors etc.)");

	conf->addAnonymousOption("infiles", 1, CONFIGURATOR_UNLIMITED, NULL, "1 or more Input files");
	conf->addAnonymousOption("outfiles", 1, 1, NULL, "Output file");

	conf->setPreamble("A program to quicktest the anonymous options");
	conf->setPostamble("Developed by Andrew Robinson");

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 2;

	OutputBase* logs = conf->getLogStream("log");

	/// do the work ///
	*logs << "You gave me:" << std::endl;
	*logs << "Inputs:" << std::endl;
	const std::vector<std::string> inputs = conf->getAnonymousOptionStrings("infiles");
	for (std::vector<std::string>::const_iterator iit=inputs.begin(); iit != inputs.end(); ++iit) {
		*logs << " - " << *iit << std::endl;
	}
	*logs << "Outputs:" << std::endl;
	const std::vector<std::string> outputs = conf->getAnonymousOptionStrings("outfiles");
	for (std::vector<std::string>::const_iterator oit=outputs.begin(); oit != outputs.end(); ++oit) {
		*logs << " - " << *oit << std::endl;
	}




    return 0;
}

