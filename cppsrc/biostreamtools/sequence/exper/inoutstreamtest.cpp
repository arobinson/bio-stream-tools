/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <iostream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/io/PlainInput.h>
#include <libbiostreams/readers/SequenceReader.h>
#include <libbiostreams/seq/complement.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);

	conf->addInStream("input", 		'i', 	new std::string("input"), 				0, 1, new std::string("-"),	"Input Sequences");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),	"Status messages (warning, errors etc.)");

	conf->addSection("Hello world", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla quis semper arcu. \nVestibulum imperdiet blandit porta. Nunc quam dui, aliquam quis libero nec, \nfeugiat pulvinar dolor. Etiam nec pharetra nunc, vel consequat justo. Mauris pretium \nposuere efficitur. Nam non elementum tellus. Suspendisse a efficitur erat, ac \nornare lorem. Nulla facilisi. Suspendisse molestie odio vel semper sagittis. \nEtiam ut quam a nunc tristique iaculis vel quis lectus. Phasellus enim nunc, congue \net enim porttitor, fringilla efficitur lectus. Suspendisse vel feugiat mauris.\n\nInteger porttitor aliquam pellentesque. Nulla ac faucibus ante, ut sodales nisl. \nInteger tempor blandit bibendum. Sed lobortis, nibh ac euismod volutpat, lorem \nante accumsan odio, sit amet blandit risus lorem at eros. Morbi consectetur \nlacus vel dui malesuada bibendum. Aenean pharetra risus sit amet nunc rhoncus, in \ntempus ligula varius. Suspendisse lacinia est vel volutpat porttitor. Duis semper \nornare dolor a scelerisque. Vivamus eget dictum ante, et lobortis nulla. Etiam \nac lobortis eros. Nunc quis diam odio. Aenean mattis tortor ac sem venenatis \nconsectetur. Mauris dui metus, luctus vel magna nec, vestibulum euismod mauris. \nMorbi sem dolor, finibus sit amet vestibulum vel, mollis vel massa.");

	conf->setPreamble("A program to quicktest the in and outstream configuration option");
	conf->setPostamble("Developed by Andrew Robinson");

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 2;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// do the work ///
	*logs << "Starting reverse complement" << std::endl;

	// read sequences
	Sequence *seq = NULL;
	SequenceReader *sr = new SequenceReader(ins);
	while ((seq = (Sequence*)sr->next()) != NULL) {
		reverseComplement(seq);
		*outs << seq;
		delete seq;
	}
	delete sr;

	*logs << "Done" << std::endl;

    return 0;
}

