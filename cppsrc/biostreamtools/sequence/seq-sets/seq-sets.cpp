/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * seq-sets.cpp
 *
 *  Created on: 5 Dec 2014
 *      Author: arobinson
 *
 *  A tool that produces the union/intersection or 'in first but not others' sets
 *  of a number of sequence files.  If source file(s) provided, then it will
 *  output the source sequence (by id) for each required sequence.
 */


#include <iostream>
#include <sstream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	int returncode = 0;
	Configurator *conf = new Configurator(argc, argv);

	conf->addOption("mode", 		'm', 	new std::string("mode"),				0, 1, new std::string("UNION"),	"Combination method\n"
																													" UNION or U: mathematical union of input sequence files\n"
																													" ISECT or I: mathematical intersection of input sequence files\n"
																													" DUNIQ or D: in first input sequence file but not others\n"
																													" NOTUNION or NU*: sequences not in mathematical union of input sequence files\n"
																													"  * requires source sequences");


//	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"Set the input format, Accepts AUTO (default), FASTQ, FASTA, RAW, RAWM");
//	conf->addOption("name", 		'n', 	new std::string("name"), 				0, 1, NULL,						"Override the name used to describe the sequences (in stats)");
//	conf->addOption("quality", 		'q', 	new std::string("quality-offset"),		0, 1, new std::string("33"),	"Set the quality offset (33=Solexa/Ilumina 1.8+, 64=Older)");

    conf->addInStream("source",     's',    new std::string("source"),              0, 1, NULL,                 "Source sequences (to use in output)");
	conf->addInStream("input", 		'\0', 	NULL, 				                    0, 1, NULL,	                "Input Sequences");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				1, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'l', 	new std::string("log"), 				0, 1, new std::string("="),	"Statistics summary");

	conf->addAnonymousOption("input", 1, CONFIGURATOR_UNLIMITED, NULL, "1 or more sequence files to operate on.");

	conf->setPreamble(
			"A tool that produces the union/intersection or 'in first but not others' sets\n"
	        "of a number of sequence files.  If source file(s) provided, then it will\n"
	        "output the source sequence (by id) for each required sequence.");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams(OUT | LOG) != 0)
		return 4;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
//	std::string format = conf->getOptionString("format");
//	if (format != "AUTO" &&
//			format != "FASTQ" &&
//			format != "FASTA" &&
//			format != "RAW" &&
//			format != "RAWM") {
//		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
//		format = "AUTO";
//	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	std::string mode = conf->getOptionString("mode");
	if (mode == "UNION" || mode == "U") {
		*logs << "Performing Union on:" << std::endl;
		InputBase* srcs = conf->getInStream("source");
		std::vector<InputBase*> instreams = conf->getInStreams("input");
		std::vector<InputBase*>::iterator iss;
		for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
			*logs << " - " << (*iss)->getFilename() << std::endl;
		}

		// using source sequences?
		if (srcs->getFilename() != "") {
			*logs << "Outputting sequences from source: " << srcs->getFilename() << std::endl;

			std::set<std::string> keys;
			unsigned long seqCount = 0;
			Sequence *seq = NULL;

			// for each file
			for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

				SequenceReader *sr = new SequenceReader(*iss);

				// loop over sequences
				while ((seq = (Sequence*)sr->next()) != NULL) {
					keys.insert(seq->id);
					seqCount++;
					delete seq;
				}

				delete sr;
			}

			*logs << "Read " << seqCount << " sequences (" << keys.size() << " unique)" << std::endl;

			// for each source seq
			srcs->open();
			SequenceReader *sr = new SequenceReader(srcs);
			while ((seq = (Sequence*)sr->next()) != NULL) {
				if (keys.find(seq->id) != keys.end()) {// output sequence if needed
					*outs << seq;
					keys.erase(seq->id);
				}
				delete seq;
			}
			delete sr;

			// check for missing sequences
			if (keys.size() > 0) {
				*logs << "Warning: some sequences not present in source" << std::endl;
				short printcount = 0;
				for (std::set<std::string>::iterator key = keys.begin(); key != keys.end(); ++key) {
					*logs << " - " << *key << std::endl;
					printcount++;
					if (printcount >= 5 && (keys.size() - printcount) > 0) {
						*logs << " - ... (" << (keys.size() - printcount) << " more) ..." << std::endl;
						break;
					}
				}
			}
		} else { // no source file (i.e. just output first occurrence)
			*logs << "Outputting input sequences (first occurrence of each id)" << std::endl;

			std::set<std::string> keys;
			unsigned long seqCount = 0;
			Sequence *seq = NULL;

			// for each file
			for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

				SequenceReader *sr = new SequenceReader(*iss);

				// loop over sequences
				while ((seq = (Sequence*)sr->next()) != NULL) {

					// only write unseen sequences
					if (keys.find(seq->id) == keys.end()) {
						*outs << seq;
						keys.insert(seq->id);
					}
					seqCount++;
					delete seq;
				}

				delete sr;
			}

			*logs << "Read " << seqCount << " sequences (" << keys.size() << " unique)" << std::endl;
		}

	} else if (mode == "ISECT" || mode == "I") {
		*logs << "Performing Intersection on:" << std::endl;
		InputBase* srcs = conf->getInStream("source");
		std::vector<InputBase*> instreams = conf->getInStreams("input");
		std::vector<InputBase*>::iterator iss;
		for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
			*logs << " - " << (*iss)->getFilename() << std::endl;
		}

		// using source sequences?
		if (srcs->getFilename() != "") {
			*logs << "Outputting sequences from source: " << srcs->getFilename() << std::endl;

			std::set<std::string> *keys = new std::set<std::string>();
			unsigned long seqCount = 0;
			Sequence *seq = NULL;

			// for each file
			bool first = true;
			for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

				SequenceReader *sr = new SequenceReader(*iss);
				if ((*iss)->eof())
					(*iss)->reset();
				std::set<std::string> *newkeys = new std::set<std::string>();

				// loop over sequences
				while ((seq = (Sequence*)sr->next()) != NULL) {
					if (first || keys->find(seq->id) != keys->end())
						newkeys->insert(seq->id);
					seqCount++;
					delete seq;
				}
				delete sr;

				delete keys;
				keys = newkeys;

				first = false;
			}

			*logs << "Read " << seqCount << " sequences (" << keys->size() << " in intersection)" << std::endl;

			// for each source seq
			srcs->open();
			SequenceReader *sr = new SequenceReader(srcs);
			while ((seq = (Sequence*)sr->next()) != NULL) {
				if (keys->find(seq->id) != keys->end()) {// output sequence if needed
					*outs << seq;
					keys->erase(seq->id);
				}
				delete seq;
			}
			delete sr;

			// check for missing sequences
			if (keys->size() > 0) {
				*logs << "Warning: some sequences not present in source" << std::endl;
				short printcount = 0;
				for (std::set<std::string>::iterator key = keys->begin(); key != keys->end(); ++key) {
					*logs << " - " << *key << std::endl;
					printcount++;
					if (printcount >= 5 && (keys->size() - printcount) > 0) {
						*logs << " - ... (" << (keys->size() - printcount) << " more) ..." << std::endl;
						break;
					}
				}
			}

			delete keys;

		} else { // using input sequences
			*logs << "Outputting input sequences (from last input file)" << std::endl;

			std::set<std::string> *keys = new std::set<std::string>();
			unsigned long seqCount = 0;
			Sequence *seq = NULL;

			// for each file
			bool first = true;
			size_t i = 1;
			unsigned long keysSize = 0;
			for (iss = instreams.begin(); iss != instreams.end(); ++iss, ++i) {
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

				SequenceReader *sr = new SequenceReader(*iss);
				if ((*iss)->eof())
					(*iss)->reset();
				std::set<std::string> *newkeys = new std::set<std::string>();

				if (i != instreams.size()) { // all but last file
					// loop over sequences
					while ((seq = (Sequence*)sr->next()) != NULL) {
						if (first || keys->find(seq->id) != keys->end())
							newkeys->insert(seq->id);
						seqCount++;
						delete seq;
					}
				} else { // last file
					// loop over sequences
					while ((seq = (Sequence*)sr->next()) != NULL) {
						if (first || keys->find(seq->id) != keys->end()) {
							*outs << seq;
							keysSize++;
						}
						seqCount++;
						delete seq;
					}
				}
				delete sr;

				delete keys;
				keys = newkeys;

				first = false;
			}

			*logs << "Read " << seqCount << " sequences (" << keysSize << " in intersection)" << std::endl;
		}
	} else if (mode == "NOTUNION" || mode == "NU") {
		*logs << "Performing Not-in-Union on:" << std::endl;
		InputBase* srcs = conf->getInStream("source");
		std::vector<InputBase*> instreams = conf->getInStreams("input");
		std::vector<InputBase*>::iterator iss;
		for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
			*logs << " - " << (*iss)->getFilename() << std::endl;
		}

		if (srcs->getFilename() != "") {

			Sequence *seq = NULL;
			std::set<std::string> keys;

			// compute union of input sequences
			unsigned long lastKeyCount = 0;
			for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

				SequenceReader *sr = new SequenceReader(*iss);
				unsigned long seqCount = 0;

				// loop over sequences
				while ((seq = (Sequence*)sr->next()) != NULL) {
					keys.insert(seq->id);
					seqCount++;
					delete seq;
				}

				*logs << " - Sequences:     " << seqCount << std::endl;
				*logs << " - New sequences: " << (keys.size() - lastKeyCount) << std::endl;

				lastKeyCount = keys.size();

				delete sr;
			}

			// for each source seq
			*logs << "Outputting sequences from source: " << srcs->getFilename() << std::endl;
			srcs->open();
			SequenceReader *sr = new SequenceReader(srcs);
			unsigned long totalSequences = 0;
			unsigned long outputSequences = 0;
			while ((seq = (Sequence*)sr->next()) != NULL) {
				if (keys.find(seq->id) == keys.end()) {// output sequence if not in union (keys)
					*outs << seq;
					outputSequences++;
				}
				totalSequences++;
				delete seq;
			}
			delete sr;
			*logs << " - Sequences:     " << totalSequences << std::endl;
			if (totalSequences > 0)
				*logs << " - Filtered:      " << (totalSequences - outputSequences) << " (" << (((double)totalSequences) - outputSequences) / totalSequences * 100 << "%)" << std::endl;
			else
				*logs << " - Filtered:      " << (totalSequences - outputSequences) << std::endl;


		} else { // no source sequences provided
			*logs << "Error: Not-in-Union mode requires source sequences" << std::endl;
			returncode = 1;
		}

	} else if (mode == "DUNIQ" || mode == "D") {
		*logs << "Performing directional unique on:" << std::endl;
		*logs << "Error: directional unique not implemented" << std::endl;
	} else {
		*logs << "Error: unknown mode '" << mode << "'" << std::endl;
	}


	// cleanup
	delete conf;

    return returncode;
}



