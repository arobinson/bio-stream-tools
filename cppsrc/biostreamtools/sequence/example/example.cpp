/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <iostream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/io/PlainInput.h>
#include <libbiostreams/readers/SequenceReader.h>
#include <libbiostreams/seq/complement.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("test", 			't', 	new std::string("test"),	0, 1, "Perform self-test options");
	conf->addFlag("quit", 			'q', 	NULL,						0, 1, "Stop the background process (and exit)");
	conf->addFlag("print", 			'\0', 	new std::string("print"),	0, 1, "Print configuration options on log stream");

	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("FASTQ"),	"Set the input format, Accepts FASTQ, FASTA, RAW");
	conf->addOption("format-out", 	'\0', 	new std::string("format-out"), 			0, 1, NULL,						"Alter output format (defaults to input format)");
	conf->addOption("format-rlon0",	'\0', 	new std::string("format-really-long"), 	0, 1, NULL,						"Test, should wrap");
	conf->addOption("format-rlon1",	'\0', 	new std::string("format-really"), 		0, 1, NULL,						"Test, should wrap");
	conf->addOption("format-rlon2",	'\0', 	new std::string("format-reall"), 		0, 1, NULL,						"Test, shouldn't wrap");

	conf->addInStream("input", 		'i', 	new std::string("input"), 				0, 1, new std::string("-"),	"Input Sequences");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),	"Status messages (warning, errors etc.)");

	conf->addSection("My custom section", "This is some text for my custom section.  It includes a newline\nwhich should be indented automatically!");

	conf->setPreamble("An example application using many of the configurator features");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 2;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// do the work ///
	*logs << "This program is about to start doing something" << std::endl;
	*logs << std::endl;
	int lines = 0;

	std::string line = ins->readLine();
	while (!ins->eof() || !line.empty()) {
		*outs << "You typed: '" << line << "'" << std::endl;
		lines++;
		line = ins->readLine();
	}

	*logs << "Echoed " << lines << " lines" << std::endl;
	*logs << "OK, Done now" << std::endl;

	delete conf;

    return 0;
}

