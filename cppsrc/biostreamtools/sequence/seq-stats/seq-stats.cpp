/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * seq-stats.cpp
 *
 *  Created on: 30 Oct 2014
 *      Author: arobinson
 */


#include <iostream>
#include <sstream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("length",			'\0', 	new std::string("length"),				0, 1, "Print sequence length statistics as well");
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, "Print configuration options on log (and continue)");

	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"Set the input format, Accepts AUTO (default), FASTQ, FASTA, RAW, RAWM");
	conf->addOption("name", 		'n', 	new std::string("name"), 				0, 1, NULL,						"Override the name used to describe the sequences (in stats)");
	conf->addOption("quality", 		'q', 	new std::string("quality-offset"),		0, 1, new std::string("33"),	"Set the quality offset (33=Solexa/Ilumina 1.8+, 64=Older)");

	conf->addInStream("input", 		'i', 	new std::string("input"), 				0, 1, new std::string("-"),	"Input Sequences");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'l', 	new std::string("log"), 				0, 1, new std::string("="),	"Statistics summary");

	conf->setPreamble(
			"Calculates statistics on sequences that are received on input.  It re-transmits\n"
			"the sequences on the output so they can be used in later processes if required.\n"
			"\n"
			"Computes the following statistics:\n"
			" Totals:\n"
			"  - Sequence count\n"
			"    - # of sequences <20 average quality\n"
			"    - # of sequences <28 average quality\n"
			"  - Bases\n"
			"    - ACGT\n"
			"    - GC\n"
			"    - N\n"
			"  - GC%\n"
			"  - Average quality\n"
			" Sequence distributions by:\n"
			"  - Length\n"
			"  - GC%\n"
			"  - Average quality\n"
			" .. more to come");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 4;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
	std::string format = conf->getOptionString("format");
	if (format != "AUTO" &&
			format != "FASTQ" &&
			format != "FASTA" &&
			format != "RAW" &&
			format != "RAWM") {
		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
		format = "AUTO";
	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	bool doSeqLen = conf->isFlagSet("length");
	Sequence *seq = NULL;
	SequenceReader *sr = new SequenceReader(ins);
	sr->setType(format);
	bool retransmit = (outs->getFilename() != "!" && outs->getFilename() != "/dev/null");

	std::string name = conf->getOptionString("name");
	if (name.size() == 0)
		name = ins->getFilename();
	int qualityOffset = conf->getOptionInt("quality");

	bool seqQualAvailable = false;
	unsigned long gcTotal = 0;
	unsigned long qualTotal = 0;
	unsigned long baseTotal = 0;
	unsigned long otherTotal = 0;
	unsigned long nTotal = 0;
	unsigned long nSequence = 0;

	size_t nSeqLenCounts = 300;
	unsigned long *seqLenCounts = new unsigned long[nSeqLenCounts];
	unsigned long minLen = 100000000000;
	unsigned long maxLen = 0;

	memset(seqLenCounts, 0, sizeof(unsigned long) * nSeqLenCounts);
	unsigned long seqGCCounts[101]; // (0 to 100%)
	memset(seqGCCounts, 0, sizeof(unsigned long) * 101);
	unsigned long seqQualCounts[64]; // (0 to 63 qual score)
	memset(seqQualCounts, 0, sizeof(unsigned long) * 64);

#define BaseOth	0
#define BaseN	1
#define BaseAT	2
#define BaseGC	3
	char basegroup[257] = "0000000000000000000000000000000000000000000000000000000000000000"
			"0000000000000000000000000000000000000000000000000000000000000000"
			"0000000000000000000000000000000000000000000000000000000000000000"
			"0000000000000000000000000000000000000000000000000000000000000000";
	for (unsigned i = 0; i < 256; ++i) {
		if (i == 'n' || i == 'N')
			basegroup[i] = BaseN;
		else if (i == 'a' || i == 'A' || i == 't' || i == 'T')
			basegroup[i] = BaseAT;
		else if (i == 'c' || i == 'C' || i == 'g' || i == 'G')
			basegroup[i] = BaseGC;
		else
			basegroup[i] = BaseOth;
	}
	char otherseen[257] = "0000000000000000000000000000000000000000000000000000000000000000"
				"0000000000000000000000000000000000000000000000000000000000000000"
				"0000000000000000000000000000000000000000000000000000000000000000"
				"0000000000000000000000000000000000000000000000000000000000000000";

	// loop over sequences
	while ((seq = (Sequence*)sr->next()) != NULL) {

		// retransmit sequence if required
		if (retransmit)
			*outs << seq;

		size_t seqLen = seq->seq.size();
		seqQualAvailable = seq->qual.size() == seqLen;

		// calculate base based stats
		unsigned long qualCount = 0;
		unsigned long counts[4] = {0,0,0,0};
		for (size_t i = 0; i < seqLen; ++i) {
			char baseg = basegroup[seq->seq[i]];
			counts[baseg]++;
			if (baseg == 0)
				otherseen[seq->seq[i]] = '1';
			if (seqQualAvailable)
				qualCount += (unsigned long)(((int)seq->qual[i]) - qualityOffset);
		}
		gcTotal += counts[BaseGC];
		nTotal += counts[BaseN];
		otherTotal += counts[BaseOth];
		qualTotal += qualCount;
		baseTotal += seqLen;

		// calculate sequence based stats
		nSequence++;

		// Seq length counts
		if (doSeqLen){
			if (seqLen > nSeqLenCounts) {
				size_t nSeqLenCountsNew = seqLen * 2.0;
				unsigned long *seqLenCountsNew = new unsigned long[nSeqLenCountsNew];
				memset(seqLenCountsNew, 0, sizeof(unsigned long) * nSeqLenCountsNew);
				for (size_t i = 0; i < nSeqLenCounts; ++i)
					seqLenCountsNew[i] = seqLenCounts[i];
				delete seqLenCounts;
				seqLenCounts = seqLenCountsNew;
				nSeqLenCounts = nSeqLenCountsNew;
			}
			seqLenCounts[seqLen]++;
			if (seqLen > maxLen)
				maxLen = seqLen;
			if (seqLen < minLen)
				minLen = seqLen;
		}

		// Seq GC% counts
		if (counts[BaseGC] > 0 || counts[BaseAT] > 0)
			seqGCCounts[(int) (100.0 * counts[BaseGC]/(counts[BaseGC]+counts[BaseAT])+0.5)]++;
//		if (seqLen - nCount - otherCount > 0)
//			seqGCCounts[(int) (100.0 * gcCount/(seqLen-nCount)+0.5)]++;
//		else
//			seqGCCounts[0]++;

		// Seq Quality counts
		if (seqLen > 0)
			seqQualCounts[(qualCount/seqLen) & 63]++; // bitwise constrain to 0 to 63

		delete seq;
	}

	// compute poor qual stats
	unsigned long avgQualLT20 = 0;
	unsigned long avgQualLT28 = 0;
	if (seqQualAvailable) {
		for (size_t i = 0; i < 28; ++i) {
			if (i < 20)
				avgQualLT20 += seqQualCounts[i];
			avgQualLT28 += seqQualCounts[i];
		}
	}

	/// print stats ///
	unsigned long acgtTotal = baseTotal - nTotal - otherTotal;
	*logs << "[Sequence statistics for: " << name << "]" << std::endl;

	*logs << " Summary:" << std::endl;
	*logs << " - Sequences:       " << nSequence << std::endl;
	if (seqQualAvailable && nSequence != 0) {
		*logs << "   - <20 avg qual:  " << avgQualLT20 << " (" << (100.0 * avgQualLT20) / nSequence << "%)" << std::endl;
		*logs << "   - <28 avg qual:  " << avgQualLT28 << " (" << (100.0 * avgQualLT28) / nSequence << "%)" << std::endl;
	}
	*logs << " - Bases:           " << baseTotal << std::endl;
	*logs << "   - ACGT:          " << acgtTotal << std::endl;
	*logs << "   - GC:            " << gcTotal << std::endl;
	*logs << "   - N's:           " << nTotal << std::endl;
	if (otherTotal > 0) {
		*logs << "   - Other:         " << otherTotal << " (saw '";
		for (char i = ' '; i <= '~'; ++i)
			if (otherseen[i] == '1')
				*logs << i;
		*logs << "')" << std::endl;
	}
	if (acgtTotal != 0)
		*logs << " - GC%:             " << (100.0 * gcTotal) / acgtTotal << std::endl;
	else
		*logs << " - GC%:             N/a" << std::endl;
	if (seqQualAvailable) {
		*logs << " - Average quality: " << ((double)qualTotal) / baseTotal << std::endl;
	}
	if (doSeqLen) {
		*logs << " - Length: " << std::endl;
		if (nSequence != 0) {
			*logs << "   - Minimum:       " << minLen << std::endl;
			*logs << "   - Average:       " << ((double)baseTotal) / nSequence << std::endl;
			*logs << "   - Maximum:       " << maxLen << std::endl;

			// N?? values (e.g. N50)
			// N?? is calculated by ordering sequences by length (largest to smallest), summing
			// the each length until ??% of total bases is passed and reporting the length of
			// the smallest.  In the special case where the length set exactly equals the required
			// base count the smallest and next smallest are averaged.
			size_t longestSequenceLen = nSeqLenCounts;
			int n = 10;
			unsigned long cumCount = 0;
			size_t cumSeqs = 0;
			size_t lastLen = maxLen;

			// NOTE: seqLenCounts[idx] is a dense array so contains lengths with no candidates
			//       idx is sequence length and value is the number of sequences of that length

			for (size_t len = longestSequenceLen-1; len > 0; --len) {

				// complete last average change (if it's needed, i.e. cumulative count is exactly at n cutoff)
				if (cumCount == (baseTotal * n / 100.0) && seqLenCounts[len] > 0) {
					*logs << "   + N" << n << ":           " << ((len+lastLen)/2) << "bp  (" << (cumSeqs + 1) << " seqs)" << std::endl;
					n+=10;
				}

				// cumulate values
				cumCount += len * seqLenCounts[len];
				cumSeqs += seqLenCounts[len];

				// output all N-statistics we passed (completed, exactly positioned we will cover the next iteration with a 'real' count)
				while (cumCount > (baseTotal * n / 100.0)) {
					unsigned offset = (cumCount - (baseTotal * n / 100.0)) / len;
					*logs << "   - N" << n << ":           " << len << "bp  (" << (cumSeqs - offset) << " seqs)" << std::endl; //<< "\t" << ((unsigned long)(baseTotal * n / 100.0))
					n+=10;
				}

				// remember last 'real' length (for average cutoff)
				if (seqLenCounts[len] > 0)
					lastLen = len;
			}
			while (n < 100) {
				*logs << "   - N" << n << ":           0bp  (" << nSequence << " seqs)" << std::endl;
				n+=10;
			}



		} else {
			*logs << "   - Minimum:       " << "N/a" << std::endl;
			*logs << "   - Average:       " << "N/a" << std::endl;
			*logs << "   - Maximum:       " << "N/a" << std::endl;
		}
	}
	*logs << std::endl;

	/// Sequence lengths ///
//	if (doSeqLen) {
//		*logs << " Sequence lengths:" << std::endl;
//		size_t longestSequenceLen = nSeqLenCounts;
//		for (size_t i = 0; i < longestSequenceLen; ++i) {
//			if (seqLenCounts[i] > 0)
//				*logs << " - " << i << "bp: " << seqLenCounts[i] << " (" << (100.0 * seqLenCounts[i]) / nSequence << "%)" << std::endl;
//		}
//	}

	*logs << " Sequence count graphs:" << std::endl;

	/// Sequences per GC % ///
	unsigned long seqGCCounts2[51]; // 0 to 100% (2% buckets)
	memset(seqGCCounts2, 0, sizeof(unsigned long) * 51);
	for (size_t i = 0; i < 101; ++i) {
		seqGCCounts2[i/2] += seqGCCounts[i];
	}
	unsigned long maxSeqGCCount = 0;
	for (size_t p = 0; p < 51; ++p) {
		if (seqGCCounts2[p] > maxSeqGCCount)
			maxSeqGCCount = seqGCCounts2[p];
	}

	if (maxSeqGCCount != 0 && nSequence != 0) {

		// ascii plot
		const char *gcYAxis = "   # of sequences    ";
		*logs << std::endl;
		*logs << "               Number of sequences per GC %             " << std::endl;
		*logs << "   +.---------.---------.---------.---------.---------.+" << std::endl;
		for (size_t i = 0; i < 21; ++i) {
			*logs << " " << gcYAxis[i] << " " << "|";
			for (size_t p = 0; p < 51; ++p) {
				if ((20 - i) <= ((20 * seqGCCounts2[p]) / maxSeqGCCount))
					*logs << "*";
				else
					*logs << " ";
			}
			if (i == 0)
				*logs << "- " << maxSeqGCCount;
			else if (i == 10)
				*logs << "- " << maxSeqGCCount / 2;
			else if (i == 20)
				*logs << "- 0";
			else
				*logs << "|";
			*logs << std::endl;
		}
		*logs << "   +.---------.---------.---------.---------.---------.+" << std::endl;
		*logs << "    0        20        40        60        80        100" << std::endl;
		*logs << "                       Average GC %                     " << std::endl;

		// raw values
		*logs << std::endl;
		for (size_t i = 0, ii = 26; i < 26; ++i, ++ii) {
			std::stringstream ss;
			ss << "  " << i * 2 << "%: " << seqGCCounts2[i] << " (" << (100.0 * seqGCCounts2[i]) / nSequence << "%)";
			*logs << ss.str();
			for (size_t p = ss.str().size(); p < 27; ++p)
				*logs << " ";
			if (ii < 51)
				*logs << "  " << ii * 2 << "%: " << seqGCCounts2[ii] << " (" << (100.0 * seqGCCounts2[ii]) / nSequence << "%)";
			*logs << std::endl;
		}
	}
	else
		*logs << std::endl << "  'Number of sequences per GC %' graph requires at least one reasonable sequence" << std::endl;

	/// Sequence quality ///
	if (seqQualAvailable) {
#define DEFAULT_SEQQUAL_WIDTH 40
		unsigned long maxSeqQualCount = 0;
		size_t maxSeqQual = DEFAULT_SEQQUAL_WIDTH;
		for (size_t i = 0; i < 64; ++i) {
			if (seqQualCounts[i] > maxSeqQualCount)
				maxSeqQualCount = seqQualCounts[i];
			if (seqQualCounts[i] > 0 && i > maxSeqQual)
				maxSeqQual = i;
		}


		if (maxSeqQualCount != 0 && nSequence != 0) {
			// ascii plot
			const char *qualYAxis = "   # of sequences    ";
			*logs << std::endl << std::endl;
			*logs << "     Number of sequences per average quality" << std::endl;
			*logs << "   +.---------.---------|-------|-.---------.";
			for (size_t i = DEFAULT_SEQQUAL_WIDTH; i < maxSeqQual; ++i)
				*logs << "-";
			*logs << "+" << std::endl;
			for (size_t i = 0; i < 21; ++i) { // rows
				*logs << " " << qualYAxis[i] << " " << "|";
				for (size_t p = 0; p <= maxSeqQual; ++p) { // cols
					if ((20 - i) <= ((20 * seqQualCounts[p]) / maxSeqQualCount))
						*logs << "*";
					else
						*logs << " ";
				}
				if (i == 0)
					*logs << "- " << maxSeqQualCount;
				else if (i == 10)
					*logs << "- " << maxSeqQualCount / 2;
				else if (i == 20)
					*logs << "- 0";
				else
					*logs << "|";
				*logs << std::endl;
			}
			*logs << "   +.---------.---------|-------|-.---------.";
			for (size_t i = DEFAULT_SEQQUAL_WIDTH; i < maxSeqQual; ++i)
				*logs << "-";
			*logs << "+" << std::endl;
			*logs << "    0        10        20      28          40" << std::endl;
			*logs << "            Average sequence quality" << std::endl;

			// raw counts
			*logs << std::endl;
			for (size_t i = 0, ii = (maxSeqQual+1)/2; i <= (maxSeqQual+1)/2; ++i, ++ii) {
				std::stringstream ss;
				ss << "  " << i << ": " << seqQualCounts[i] << " (" << (100.0 * seqQualCounts[i]) / nSequence << "%)";
				*logs << ss.str();
				for (size_t p = ss.str().size(); p < 27; ++p)
					*logs << " ";
				if (ii < 64)
					*logs << "  " << ii << ": " << seqQualCounts[ii] << " (" << (100.0 * seqQualCounts[ii]) / nSequence << "%)";
				*logs << std::endl;
			}
		}
		else
			*logs << "  'Number of sequences per average quality' graph requires at least one reasonable sequence" << std::endl;
	}

	*logs << "--End--" << std::endl;

	delete sr;
	delete conf;

    return 0;
}



