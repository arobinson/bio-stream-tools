/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * seq-sets.cpp
 *
 *  Created on: 6 Jul 2015
 *      Author: arobinson
 *
 *  A tool that filters sequences so they are unique (or duplicate)
 */


#include <iostream>
#include <sstream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	int returncode = 0;
	Configurator *conf = new Configurator(argc, argv);

	conf->addFlag("quiet",			'q', 	new std::string("quiet"),				0, 1, "Don't print status/progress messages on log stream");

	conf->addOption("mode", 		'm', 	new std::string("mode"),				0, 1, new std::string("UNIQ"),	"Output mode:\n"
																													" UNIQ or U: once per sequence\n"
																													" DUP or D: on second occurrence only"
																													);


//	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"Set the input format, Accepts AUTO (default), FASTQ, FASTA, RAW, RAWM");

	conf->addInStream("input", 		'\0', 	NULL, 				                    0, 1, NULL,	                "Input Sequences");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				1, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'l', 	new std::string("log"), 				0, 1, new std::string("="),	"Statistics summary");

	conf->addAnonymousOption("input", 1, CONFIGURATOR_UNLIMITED, NULL, "1 or more sequence files to operate on.");

	conf->setPreamble(
			"A tool that filters sequences so they are unique (or duplicate) based on their ID.\n"
			"In UNIQ mode, it keeps the first occurrence of each sequence id and in DUP mode it\n"
			"keeps only the second occurrence.\n"
			"\n"
			"[Examples]:\n"
			"\n"
			"1) Remove duplicates:\n"
			"  seq-uniq input.fa > uniq.fa\n"
			"\n"
			"2) Find duplicates:\n"
			"  seq-uniq -m D input.fa > dups.fa\n"
			"\n"
			"3) Multiple input files:\n"
			"  seq-uniq in1.fa in2.fa > uniq.fa\n"
			"\n"
			"4) FastQ files: it auto detects fastq file format (by contents not extension)\n"
			"  seq-uniq input.fq > uniq.fq\n"
			"\n"
			"5) Piped input: '-' input file is shorthand for standard input (or piped input)\n"
			"  cat input.fa | seq-uniq - > uniq.fa\n");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams(OUT | LOG) != 0)
		return 4;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
//	std::string format = conf->getOptionString("format");
//	if (format != "AUTO" &&
//			format != "FASTQ" &&
//			format != "FASTA" &&
//			format != "RAW" &&
//			format != "RAWM") {
//		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
//		format = "AUTO";
//	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	std::string mode = conf->getOptionString("mode");
	if (mode == "UNIQ" || mode == "U") {
		if (!conf->isFlagSet("quiet"))
			*logs << "Outputting first occurrence of each sequence:" << std::endl;
		std::vector<InputBase*> instreams = conf->getInStreams("input");
		std::vector<InputBase*>::iterator iss;

		std::set<std::string> keys;
		unsigned long seqCountTotal = 0;
		Sequence *seq = NULL;
		for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
			if (!conf->isFlagSet("quiet"))
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

			SequenceReader *sr = new SequenceReader(*iss);

			// loop over sequences
			while ((seq = (Sequence*)sr->next()) != NULL) {

				if (keys.find(seq->id) == keys.end()) { // ID not seen before
					*outs << seq;
					keys.insert(seq->id);
				}
				seqCountTotal++;
				delete seq;
			}

			delete sr;
		}

		if (!conf->isFlagSet("quiet"))
			*logs << "Read " << seqCountTotal << " sequences (" << keys.size() << " unique)" << std::endl;

	} else if (mode == "DUP" || mode == "D") {
		if (!conf->isFlagSet("quiet"))
			*logs << "Outputting duplicates (once per duplicate):" << std::endl;
		std::vector<InputBase*> instreams = conf->getInStreams("input");
		std::vector<InputBase*>::iterator iss;

		std::map<std::string, unsigned> seqcounts;
		unsigned long seqCountTotal = 0;
		unsigned long seqCountOutput = 0;
		Sequence *seq = NULL;
		for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
			if (!conf->isFlagSet("quiet"))
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

			SequenceReader *sr = new SequenceReader(*iss);

			// loop over sequences
			while ((seq = (Sequence*)sr->next()) != NULL) {

				std::map<std::string, unsigned>::iterator seqcount = seqcounts.find(seq->id);

				if (seqcount == seqcounts.end()) { // ID not seen before
					seqcounts[seq->id] = 1;
				} else {
					seqcount->second++;
					if (seqcount->second == 2) {
						*outs << seq;
						seqCountOutput++;
					}
				}
				seqCountTotal++;
				delete seq;
			}

			delete sr;
		}

		if (!conf->isFlagSet("quiet"))
			*logs << "Read " << seqCountTotal << " sequences (" << seqCountOutput << " duplicated)" << std::endl;

	} else {
		*logs << "Error: unknown mode '" << mode << "'" << std::endl;
	}


	// cleanup
	delete conf;

    return returncode;
}



