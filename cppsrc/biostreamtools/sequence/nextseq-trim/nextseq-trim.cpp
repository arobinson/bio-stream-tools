/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 14/01/2015
 *      Author: arobinson
 */

#include <iostream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>
//#include <libbiostreams/seq/trimming.h>

#include <biostreamtools/version.h>

#include "trimming.h"

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, "Print configuration options on log (and continue)");
	conf->addFlag("interleaved",	'I', 	new std::string("interleaved"),			0, 1, "Input sequences are paired (interleaved)");
	conf->addFlag("paired",			'p', 	new std::string("paired"),				0, 1, "Input sequences are paired (file pairs)");

	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"The format received on input:   [default: AUTO]\n AUTO: detect from first character\n FASTQ\n FASTA\n RAW: one sequence per line\n RAWM: single multi-line raw sequence");
	conf->addOption("min-length", 	'l', 	new std::string("min-length"),			0, 1, new std::string("0"),		"The minimum length sequence to keep after trimming      [default: 0]");
	conf->addOption("max-errors", 	'm', 	new std::string("max-errors"),			0, 1, new std::string("5"),		"The maximum base mismatches to tolerate in A..G.. tail  [default: 5]");

	conf->addInStream("input", 		'\0', 	NULL,						1, CONFIGURATOR_UNLIMITED, new std::string("-"), 	"Input raw sequences          [default: -]\n -: standard input");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),		"Output trimmed sequences     [default: -]\n -: standard output\n =: standard error");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),		"Log message stream/file      [default: =]\n -: standard output\n =: standard error");

	conf->addAnonymousOption("input", 		0, CONFIGURATOR_UNLIMITED, NULL, 				"N/a");

	conf->setPreamble("Trims the A..G.. tails from sequences that are typically found on NextSeqTM data");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams(OUT|LOG) != 0)
		return 2;

	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
	if (conf->isFlagSet("paired") && conf->isFlagSet("interleaved")) {
		conf->printUsageSummary();
		*logs << "Error: Data files cannot be Interleaved AND Paired" << std::endl;
		return 3;
	}
	std::vector<InputBase*> infiles = conf->getInStreams("input");
	if ((infiles.size() % 2) != 0 && conf->isFlagSet("paired")) { // even number of files
		conf->printUsageSummary();
		*logs << "Error: Expects an even number of input files in paired mode" << std::endl;
		return 4;
	}
	std::string format = conf->getOptionString("format");
	if (format != "AUTO" &&
			format != "FASTQ" &&
			format != "FASTA" &&
			format != "RAW" &&
			format != "RAWM") {
		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
		format = "AUTO";
	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	unsigned long count = 0;
	unsigned long kept = 0;
	int minLength = conf->getOptionInt("min-length");
	int maxErrors = conf->getOptionInt("max-errors");

	if (conf->isFlagSet("paired")) { // paired forward/reverse files

		Sequence *seq1 = NULL;
		Sequence *seq2 = NULL;
		std::vector<InputBase*> infiles = conf->getInStreams("input");

		// for each input file pair
		for (size_t i = 0; i+1 < infiles.size(); i+=2) {
			SequenceReader *sr1 = new SequenceReader(infiles[i]);
			SequenceReader *sr2 = new SequenceReader(infiles[i+1]);
			sr1->setType(format);
			sr2->setType(format);

			// for each sequence in these files
			while ((seq1 = (Sequence*)sr1->next()) != NULL &&
					(seq2 = (Sequence*)sr2->next()) != NULL) {

				// trim tail
				trimAGTail(seq1, maxErrors);
				trimAGTail(seq2, maxErrors);

				// length check
				if (seq1->seq.length() >= minLength &&
						seq2->seq.length() >= minLength) {
					kept++;
					*outs << seq1 << seq2;
				}

				count++;

				delete seq1;
				delete seq2;
				seq1 = NULL;
				seq2 = NULL;
			}

			if (seq1 != NULL)
				delete seq1;
			if (seq2 != NULL)
				delete seq2;
			delete sr1;
			delete sr2;
		}

		if (minLength > 0) {
			*logs << "Kept " <<  kept << " of " << count << " pairs";
		} else {
			*logs << "Trimmed " << count << " pairs";
		}

	} else if (conf->isFlagSet("interleaved")) { // paired interleave

		InputBase* ins = conf->getInStream("input");
		Sequence *seq1 = NULL;
		Sequence *seq2 = NULL;

		std::vector<InputBase*> infiles = conf->getInStreams("input");

		// for each input file
		for (size_t i = 0; i < infiles.size(); i++) {
			SequenceReader *sr = new SequenceReader(infiles[i]);
			sr->setType(format);

			// for each sequence pair in file
			while ((seq1 = (Sequence*)sr->next()) != NULL &&
					(seq2 = (Sequence*)sr->next()) != NULL) {

				// trim tail
				trimAGTail(seq1, maxErrors);
				trimAGTail(seq2, maxErrors);

				// length check
				if (seq1->seq.length() >= minLength &&
						seq2->seq.length() >= minLength) {
					kept++;
					*outs << seq1 << seq2;
				}
				count++;

				delete seq1;
				delete seq2;
				seq1 = NULL;
				seq2 = NULL;
			}

			if (seq1 != NULL)
				delete seq1;
			if (seq2 != NULL)
				delete seq2;
			delete sr;
		}

		if (minLength > 0) {
			*logs << "Kept " <<  kept << " of " << count << " pairs";
		} else {
			*logs << "Trimmed " << count << " pairs";
		}

	} else { // single end data

//		InputBase* ins = conf->getInStream("input");
		Sequence *seq = NULL;

		// for each input file
		for (size_t i = 0; i < infiles.size(); i++) {
			SequenceReader *sr = new SequenceReader(infiles[i]);
			sr->setType(format);

			// for each sequence in file
			while ((seq = (Sequence*)sr->next()) != NULL) {
				trimAGTail(seq, maxErrors);
				if (seq->seq.length() >= minLength) {
					kept++;
					*outs << seq;
				}
				count++;
				delete seq;
			}

			delete sr;
		}

		if (minLength > 0) {
			*logs << "Kept " <<  kept << " of " << count << " sequences";
		} else {
			*logs << "Trimmed " << count << " sequences";
		}

	}

	logs->endl();
	delete conf;

    return 0;
}

