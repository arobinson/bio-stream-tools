/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <vector>

#include <cstring>
#include <cstdio>

#include "trimming.h"

#ifndef BIOSTREAMTOOLS_SEQUENCE_NEXTSEQ_TRIM_TRIMMING_CPP_
#define BIOSTREAMTOOLS_SEQUENCE_NEXTSEQ_TRIM_TRIMMING_CPP_

/**
 * Class to store the state of a search
 */
class State {
public:
	char phase; ///< are we looking for G's or A's
	int errors; ///< number of mismatches found so-far
//	int passes; ///< number of matches found so-far
	char * seq; ///< the match history
	int seqn;   ///< size of memory allocated in seq
	int seqstart; ///< the start position of data within seq
//	int trimpoint; ///< the point at which the tail should be trimmed according to this path

	State(State &s) {
		this->phase = s.phase;
		this->errors = s.errors;
//		this->passes = s.passes;
		this->seqn = s.seqn;
		this->seq = new char[this->seqn];
		memcpy(this->seq, s.seq, this->seqn);
//		this->trimpoint = 0;
		this->seqstart = s.seqstart;
	}

	State(char * seq=NULL, int seqn=0, char phase='G', int errors=0/*, int passes=0*/) {
		this->phase = phase;
		this->errors = errors;
//		this->passes = passes;
		this->seq = seq;
		this->seqn = seqn;
//		this->trimpoint = 0;
		this->seqstart = 0;
	}

	~State() {
		if (this->seq != NULL)
			delete this->seq;
	}
};

#endif

/**
 * Trims the A..G.. tail from the given sequence (in-place) that is typically
 * found on NextSeqTM data
 */
void trimAGTail(Sequence* seq, int maxErrors) {

	int i = seq->seq.size() - 1;
	if (i < 0)
		return;

	std::vector<State*> active;
	char * _s = new char[seq->seq.size() + 1];
	memset(_s, 0, seq->seq.size() + 1);
	active.push_back(new State(_s, seq->seq.size() + 1));

	std::vector<State*> complete;

	// for each base (from end to start)
	for (; i >= 0; --i) {
		char b = seq->seq[i];
		std::vector<State*> newactive;

		// for each active state
		std::vector<State*>::iterator it = active.begin();
		for (; it != active.end(); ++it) {
			State *s = *it;
			if (s->phase == 'G') {
				if (b == 'G') {
//					s->passes++;
					s->seq[i] = 'G';
				} else if (b == 'A') {
					State *s2 = new State(*s);
//					s2->passes++;
					s2->seq[i] = 'A';
					s2->phase = 'A';
					newactive.push_back(s2);
					s->errors++;
					s->seq[i] = 'x';
				} else { // b == 'C' or 'T'
					s->errors++;
					s->seq[i] = 'x';
				}
				if (s->errors >= maxErrors) {
					s->seqstart = i;
					complete.push_back(s);
				} else
					newactive.push_back(s);
			} else { // phase == 'A'
				if (b == 'A') {
//					s->passes++;
					s->seq[i] = 'A';
				} else {
					s->errors++;
					s->seq[i] = 'x';
				}
				if (s->errors >= maxErrors) {
					s->seqstart = i;
					complete.push_back(s);
				} else
					newactive.push_back(s);
			}
		}

		active = newactive;

		if (active.size() == 0)
			break;
	}

	// calculate the best cut point
	if (active.size() > 0)
		complete.insert(complete.end(), active.begin(), active.end());

	int smallest = seq->seq.size();
	std::vector<State*>::iterator it = complete.begin();
	for (; it != complete.end(); ++it) {
		State *s = *it;
		int miscount=0, hitcount=0, hitstart=-1, okstart=-1, pos=s->seqstart;

		while (s->seq[pos] != '\0') {
			if (s->seq[pos] == 'x') {
				miscount++;
				hitstart = -1;
			} else {
				hitcount++;
				if (hitstart < 0) {
					hitstart = pos;
				}
			}
			if (hitcount > miscount || miscount >= s->errors) {
				if (okstart == -1 && hitstart != -1) {
					okstart = hitstart;
				}
			} else if (hitcount < miscount) {
				okstart = -1;
			}
			++pos;
		}

		if (okstart >= 0) {
			int trimpoint = okstart;
			if (trimpoint < smallest)
				smallest = trimpoint;
		}
	}

	// perform the cut
	seq->seq = seq->seq.substr(0, smallest);
	if (seq->qual.size() != 0)
		seq->qual = seq->qual.substr(0, smallest);

	// cleanup
	it = complete.begin();
	for (; it != complete.end(); ++it) {
		State *s = *it;
		delete s;
	}
}



















