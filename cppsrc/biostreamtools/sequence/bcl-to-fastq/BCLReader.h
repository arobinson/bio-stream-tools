/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * BCLReader.h
 *
 *  Created on: 26 Nov 2015
 *      Author: arobinson
 */


#include <cstdio>


#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_BCLREADER_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_BCLREADER_H_

namespace bcltofastq {

const char baseLookup[4] = {'A','C','G','T'};

/**
 * Reads the bases and quality scores from the given BCL file
 */
class BCLReader {

protected:
	FILE *pFile;
	bool headerRead;
	unsigned int baseCount;
	unsigned int basePos;

	bool read4UBytes(unsigned char ** buffer);

public:
	BCLReader(const char* file);
	virtual ~BCLReader();

	/**
	 * Reads the header (i.e. the number of sequences in this BCL file)
	 */
	virtual bool readHeader(unsigned int * result);

	/**
	 * Reads a single base (& qual) from BCL file
	 *
	 * Returns (via params) the base (ASCII encoded) and quality (Phred+33 encoded)
	 */
	virtual bool readBaseQual(char *base, char *qual);

};

/**
 * A BCLReader that returns null characters instead of a real base/qual.
 */
class NullBCLReader : public BCLReader {
public:

	NullBCLReader();
	virtual ~NullBCLReader();

	/**
	 * Not implemented
	 */
	virtual bool readHeader(unsigned int * result);

	/**
	 * Returns null characters for both base and quality
	 */
	virtual bool readBaseQual(char *base, char *qual);

};

} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_BCLREADER_H_ */
