/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ConfigReader.cpp
 *
 *  Created on: 25 Nov 2015
 *      Author: arobinson
 */

#include "tinyxml2/tinyxml2.h"

#include "ConfigReader.h"

namespace bcltofastq {

/**
 * Constructor.
 *
 * Opens and reads data from file.  Call isOk() to see if it was successful.
 */
ConfigReader::ConfigReader(const char *file) {
	loadConfigFile(file);
}

ConfigReader::~ConfigReader() {
}

/**
 * Extracts key information from the named config file
 */
bool ConfigReader::loadConfigFile(const char *file) {

	// clear old values (if any)
	ok = false;
	lanes.clear();
	tiles.clear();
	cycles.clear();
	instrument = "";
	runNumber = "";
	flowcell = "";

	// temp variables
	std::set<std::string> tilesSet;

	// open file
	tinyxml2::XMLDocument doc;
	doc.LoadFile(file);

	tinyxml2::XMLElement* nodeBaseCallAnalysis = doc.FirstChildElement("BaseCallAnalysis");
	if (nodeBaseCallAnalysis != NULL) {
		tinyxml2::XMLElement* nodeRun = nodeBaseCallAnalysis->FirstChildElement("Run");
		if (nodeRun != NULL) {
			tinyxml2::XMLElement* nodeTileSelection = nodeRun->FirstChildElement("TileSelection");
			if (nodeTileSelection != NULL) {

				// find how many lanes
				tinyxml2::XMLElement* lane = nodeTileSelection->FirstChildElement("Lane");
				while (lane != NULL) {
					lanes.push_back(atoi(lane->Attribute("Index")));

					tinyxml2::XMLElement* tile = lane->FirstChildElement("Tile");
					while (tile != NULL) {
						std::string tileNum = tile->GetText();
						tilesSet.insert(tileNum);
						tile = tile->NextSiblingElement("Tile");
					}

					lane = lane->NextSiblingElement("Lane");
				}
			}

			// get constants for read names
			tinyxml2::XMLElement* nodeRunParameters = nodeRun->FirstChildElement("RunParameters");
			if (nodeRunParameters != NULL) {
				tinyxml2::XMLElement* nodeInstrument = nodeRunParameters->FirstChildElement("Instrument");
				if (nodeInstrument != NULL)
					instrument = nodeInstrument->GetText();
				tinyxml2::XMLElement* nodeRunFolderId = nodeRunParameters->FirstChildElement("RunFolderId");
				if (nodeRunFolderId != NULL) {
					runNumber = nodeRunFolderId->GetText();
					while (runNumber.size() > 0 && runNumber.at(0) == '0') // remove leading 0's
						runNumber = runNumber.substr(1);
				}
				tinyxml2::XMLElement* nodeRunFlowcellId = nodeRunParameters->FirstChildElement("RunFlowcellId");
				if (nodeRunFlowcellId != NULL)
					flowcell = nodeRunFlowcellId->GetText();
			}

			// <Cycles First="1" Last="251" Number="251" />
			tinyxml2::XMLElement* nodeCycles = nodeRun->FirstChildElement("Cycles");
			if (nodeCycles != NULL) {
				int f = atoi(nodeCycles->Attribute("First"));
				int l = atoi(nodeCycles->Attribute("Last"));
				for (int i = f; i <= l; ++i)
					cycles.push_back(i);
			}
		}
	}

	tiles.assign(tilesSet.begin(), tilesSet.end());

	doc.Clear();

	// check if everything was found
	ok = lanes.size() > 0 &&
			tiles.size() > 0 &&
			cycles.size() > 0 &&
			instrument.size() > 0 &&
			runNumber.size() > 0 &&
			flowcell.size() > 0;

	return ok;
}


/**
 * Returns true if the file was openable and all config was found
 */
bool ConfigReader::isOk() {
	return ok;
}

/**
 * Get a vector of the lane numbers that are in the config
 */
std::vector<int> ConfigReader::getLanes() {
	return lanes;
}

/**
 * Get a vector of the tiles that are in the config
 */
std::vector<std::string> ConfigReader::getTiles() {
	return tiles;
}

/**
 * Get a vector of the cycles that are in the config
 */
std::vector<int> ConfigReader::getCycles() {
	return cycles;
}

/**
 * Get the Instrument Name from the config file
 */
std::string ConfigReader::getInstrumentName() {
	return instrument;
}

/**
 * Get the Run Folder ID from the config file (aka Run Number)
 */
std::string ConfigReader::getRunFolderId() {
	return runNumber;
}

/**
 * Get the Flowcell ID for this run
 */
std::string ConfigReader::getFlowcellId() {
	return flowcell;
}


} /* namespace bcltofastq */
