/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ConfigReader_unittest.cpp
 *
 *  Created on: 27 Nov 2015
 *      Author: arobinson
 */

#include "Directory.h"
#include <gtest/gtest.h>
#include <cerrno>
#include <sys/stat.h>
#include <unistd.h>


TEST(Directory, NewDelete) {

	bcltofastq::Directory *dir = new bcltofastq::Directory("data/test/Directory/");

	EXPECT_TRUE(dir != NULL);

	delete dir;
}

TEST(Directory, ExistsPresent) {

	bcltofastq::Directory *dir = new bcltofastq::Directory("data/test/Directory/");

	EXPECT_TRUE(dir->exists());

	delete dir;
}

TEST(Directory, ExistsMissing) {

	bcltofastq::Directory *dir = new bcltofastq::Directory("data/test/Directory/notexists/");

	EXPECT_FALSE(dir->exists());

	delete dir;
}

TEST(Directory, ExistsFile) {

	bcltofastq::Directory *dir = new bcltofastq::Directory("data/test/Directory/file");

	EXPECT_FALSE(dir->exists());

	delete dir;
}

TEST(Directory, Parent1a) {

	bcltofastq::Directory dir("/tmp/hello");
	bcltofastq::Directory result = dir.parent();

	EXPECT_EQ(0, strcmp(result.name(), "/tmp"));
}
TEST(Directory, Parent1b) {

	bcltofastq::Directory dir("/tmp/hello/");
	bcltofastq::Directory result = dir.parent();

	EXPECT_EQ(0, strcmp(result.name(), "/tmp"));
}
TEST(Directory, Parent3) {

	bcltofastq::Directory dir("/tmp");
	bcltofastq::Directory result = dir.parent();
	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), "/"));
}
TEST(Directory, Parent4) {

	bcltofastq::Directory dir("/");
	bcltofastq::Directory result = dir.parent();

	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), ""));
}
TEST(Directory, Parent5) {

	bcltofastq::Directory dir("./hello");
	bcltofastq::Directory result = dir.parent();

	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), "."));
}
TEST(Directory, Parent6a) {

	bcltofastq::Directory dir(".");
	bcltofastq::Directory result = dir.parent();

	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), ""));
}
TEST(Directory, Parent6b) {

	bcltofastq::Directory dir("./");
	bcltofastq::Directory result = dir.parent();

	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), ""));
}
TEST(Directory, Parent8a) {

	bcltofastq::Directory dir("hello");
	bcltofastq::Directory result = dir.parent();

	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), "."));
}
TEST(Directory, Parent8b) {

	bcltofastq::Directory dir("h");
	bcltofastq::Directory result = dir.parent();

	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), "."));
}
TEST(Directory, Parent9) {

	bcltofastq::Directory dir("");
	bcltofastq::Directory result = dir.parent();

	ASSERT_NE((const char *)NULL, result.name());
	EXPECT_EQ(0, strcmp(result.name(), ""));
}
TEST(Directory, Parent10) {

	bcltofastq::Directory dir(NULL);
	bcltofastq::Directory result = dir.parent();

	EXPECT_EQ(NULL, result.name());
}

// ------------------------------------
TEST(Directory, ListAll) {

	bcltofastq::Directory dir("data/test/Directory/list");

	std::vector<std::string> files = dir.listDirectory();

	EXPECT_EQ(6, files.size());
}
TEST(Directory, ListEndsTxt) {

	bcltofastq::Directory dir("data/test/Directory/list");

	const std::vector<std::string> files = dir.listDirectory(NULL, ".txt");

	EXPECT_EQ(2, files.size());
}
TEST(Directory, ListEndsFile) {

	bcltofastq::Directory dir("data/test/Directory/list");

	const std::vector<std::string> files = dir.listDirectory(NULL, "file");

	EXPECT_EQ(1, files.size());
}
TEST(Directory, ListEndsNotExist) {

	bcltofastq::Directory dir("data/test/Directory/list");

	std::vector<std::string> files = dir.listDirectory(NULL, "notexisting");

	EXPECT_EQ(0, files.size());
}
TEST(Directory, ListStartsH) {

	bcltofastq::Directory dir("data/test/Directory/list");

	std::vector<std::string> files = dir.listDirectory("h");

	EXPECT_EQ(2, files.size());
}
TEST(Directory, ListStartsFile) {

	bcltofastq::Directory dir("data/test/Directory/list");

	std::vector<std::string> files = dir.listDirectory("file");

	EXPECT_EQ(4, files.size());
}
TEST(Directory, ListStartsFiles) {

	bcltofastq::Directory dir("data/test/Directory/list");

	std::vector<std::string> files = dir.listDirectory("files");

	EXPECT_EQ(1, files.size());
}
TEST(Directory, ListStartsNotExist) {

	bcltofastq::Directory dir("data/test/Directory/list");

	std::vector<std::string> files = dir.listDirectory("notexisting");

	EXPECT_EQ(0, files.size());
}


// ------------------------------------
bool rm(const char *dname) {

	struct stat st;
	if (stat(dname, &st) != 0)
		return errno == 2; // No such file
	if (S_ISDIR(st.st_mode))
		return rmdir(dname) == 0;
	else
		return remove(dname) == 0;
}
bool isDir(const char* filename) {
	struct stat st;
	return stat(filename, &st) == 0 && S_ISDIR(st.st_mode);
}


TEST(Directory, CreateParentsFalse) {

	EXPECT_TRUE(rm("/tmp/DirectoryTest"));

	bcltofastq::Directory dir("/tmp/DirectoryTest");

	EXPECT_TRUE(dir.create(false));
	EXPECT_TRUE(isDir("/tmp/DirectoryTest"));

	EXPECT_TRUE(rm("/tmp/DirectoryTest"));
}
TEST(Directory, CreateParentsFalseNoParent) {

	EXPECT_TRUE(rm("/tmp/DirectoryTest/subdir"));
	EXPECT_TRUE(rm("/tmp/DirectoryTest"));

	bcltofastq::Directory dir("/tmp/DirectoryTest/subdir");

	EXPECT_FALSE(dir.create(false));

	EXPECT_TRUE(rm("/tmp/DirectoryTest/subdir"));
	EXPECT_TRUE(rm("/tmp/DirectoryTest"));
}
TEST(Directory, CreateParentsTrue) {

	EXPECT_TRUE(rm("/tmp/DirectoryTest"));

	bcltofastq::Directory dir("/tmp/DirectoryTest");

	EXPECT_TRUE(dir.create(true));
	EXPECT_TRUE(isDir("/tmp/DirectoryTest"));

	EXPECT_TRUE(rm("/tmp/DirectoryTest"));
}
TEST(Directory, CreateParentsTrueNoParent) {

	EXPECT_TRUE(rm("/tmp/DirectoryTest/subdir"));
	EXPECT_TRUE(rm("/tmp/DirectoryTest"));

	bcltofastq::Directory dir("/tmp/DirectoryTest/subdir");

	EXPECT_TRUE(dir.create(true));
	EXPECT_TRUE(isDir("/tmp/DirectoryTest/subdir"));

	EXPECT_TRUE(rm("/tmp/DirectoryTest/subdir"));
	EXPECT_TRUE(rm("/tmp/DirectoryTest"));
}





