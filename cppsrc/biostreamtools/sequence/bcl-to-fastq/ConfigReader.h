/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ConfigReader.h
 *
 *  Created on: 25 Nov 2015
 *      Author: arobinson
 */

#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_CONFIGREADER_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_CONFIGREADER_H_

#include <set>
#include <string>
#include <vector>

namespace bcltofastq {

class ConfigReader {
protected:
	bool ok;

	std::vector<int> lanes;
	std::vector< std::string > tiles;
	std::vector<int> cycles;
	std::string instrument;
	std::string runNumber;
	std::string flowcell;

	bool loadConfigFile(const char *file);

public:
	ConfigReader(const char *file);
	virtual ~ConfigReader();

	/**
	 * Returns true if the file was openable and all config was found
	 */
	bool isOk();

	/**
	 * Get a vector of the lane numbers that are in the config
	 */
	std::vector<int> getLanes();

	/**
	 * Get a vector of the tiles that are in the config
	 */
	std::vector<std::string> getTiles();

	/**
	 * Get a vector of the cycles that are in the config
	 */
	std::vector<int> getCycles();

	/**
	 * Get the Instrument Name from the config file
	 */
	std::string getInstrumentName();

	/**
	 * Get the Run Folder ID from the config file (aka Run Number)
	 */
	std::string getRunFolderId();

	/**
	 * Get the Flowcell ID for this run
	 */
	std::string getFlowcellId();
};

} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_CONFIGREADER_H_ */
