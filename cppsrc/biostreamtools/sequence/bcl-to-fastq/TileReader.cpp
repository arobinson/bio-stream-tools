/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * TileReader.cpp
 *
 *  Created on: 7 Dec 2015
 *      Author: arobinson
 */

#include <algorithm>
#include <iomanip>
#include <string>
#include <sstream>

#include <sys/stat.h>

#include "BCLReader.h"
#include "ClocsReader.h"
#include "Read.h"
#include "TileReader.h"

namespace bcltofastq {

TileReader::TileReader(std::string tileName, LaneInfo* lane, ExperimentConfig *exp) :
		missingBase('N'),
		missingQual('!')
	{
	this->tileName = tileName;
	this->lane = lane;
	this->exp = exp;

	open = false;
	readNumber = 0;
	readCount = 0;
	//@HWI-M01178:19:000000000-D0L4W:1:1101:15820:1333 1:N:0:GAATCT
	// <Instrument>HWI-M01178</Instrument>
	//            <RunFolderId>0019</RunFolderId>
	//               <RunFlowcellId>000000000-D0L4W</RunFlowcellId>
	//                               *itLane
	//                                 tile
	std::stringstream ss;
	ss << "@" << exp->instrumentName << ":" << exp->runId << ":" << exp->flowcellId << ":" << lane->number << ":" << tileName << ":";
	readHeaderBase = ss.str();
	filterReader = NULL;
	locationReader = NULL;


	/// Check configuration ///
	// Check for paired-end data
	paired = false;
	if (exp->r2start > 0 && exp->r2start <= exp->r2end)
		paired = true;

	// Count number of barcodes
	barcodeCount = 0;
	if (exp->b1start > 0 && exp->b1start <= exp->b1end) {
		barcodeCount = 1;
		if (exp->b2start > 0 && exp->b2start <= exp->b2end)
			barcodeCount = 2;
	}

	// Check for longest read length
	maxReadLength = exp->r1end - exp->r1start + 1;
	if (exp->r2end - exp->r2start + 1 > maxReadLength)
		maxReadLength = exp->r2end - exp->r2start + 1;

	// Check for longest barcode length
	maxBarcodeLength = exp->b1end - exp->b1start + 1;
	if (exp->b2end - exp->b2start + 1 > maxReadLength)
		maxBarcodeLength = exp->b2end - exp->b2start + 1;

	// Check for longest possible header length
	//@HWI-M01178:19:000000000-D0L4W:1:1101:15820:1333 1:N:0:GAATCT
	// = 1 + MachLen + 1 + RunNumLen + 1 + FlowcellLen + 1 + 1 + 4 + XOffLen + 1 + YOffLen + 1 + 1 + 1 + 1 + 1 + BC1Len + BC2Len + wriggle room
	headerLength = 1 + exp->instrumentName.size() + 1 + exp->runId.size() + 1 + exp->flowcellId.size() + 6 + 10 + 1 + 10 + 5 + maxBarcodeLength + maxBarcodeLength + 10;
}

TileReader::~TileReader() {

	// cleanup
	std::vector<bcltofastq::BCLReader*>::iterator ri;
	for (ri = readersRead1.begin(); ri != readersRead1.end(); ++ri)
		delete (*ri);
	readersRead1.clear();
	for (ri = readersRead2.begin(); ri != readersRead2.end(); ++ri)
		delete (*ri);
	readersRead2.clear();
	for (ri = readersBarcode1.begin(); ri != readersBarcode1.end(); ++ri)
		delete (*ri);
	readersBarcode1.clear();
	for (ri = readersBarcode2.begin(); ri != readersBarcode2.end(); ++ri)
		delete (*ri);
	readersBarcode2.clear();

	if (filterReader != NULL) {
		delete filterReader;
		filterReader = NULL;
	}

	if (locationReader != NULL) {
		delete locationReader;
		locationReader = NULL;
	}

}



/**
 * Opens the Tile's files for reading and reads the number of reads that
 * should be contained within.
 */
bool TileReader::openTile() {
	if (!open) {
		// create the BCL file readers
		// ./Data/Intensities/BaseCalls/L001/C1.1/s_1_1101.bcl
		// ./Data/Intensities/BaseCalls/L001/C2.1/s_1_1101.bcl
		// ...
		std::stringstream bclFilenameBase;
		bclFilenameBase << exp->baseCallsDir << "/" << lane->getName() << "/C";
		std::vector<int>::iterator ci;
		for (ci = lane->cycles.begin(); ci != lane->cycles.end(); ++ci) {

			int cycleNum = (*ci);
			unsigned int size = 0;
			std::stringstream bclFilename;
			bclFilename << bclFilenameBase.str() << (*ci) << ".1/s_" << lane->number << "_" << tileName << ".bcl";

			// a Read 1 cycle ?
			if (cycleNum >= exp->r1start && cycleNum <= exp->r1end) {
				BCLReader *bclr = new bcltofastq::BCLReader(bclFilename.str().c_str());
				readersRead1.push_back(bclr);

				bclr->readHeader(&size);
				if (size > readCount)
					readCount = size;
			}

			// a Read 2 cycle ?
			if (cycleNum >= exp->r2start && cycleNum <= exp->r2end) {
				BCLReader *bclr = new bcltofastq::BCLReader(bclFilename.str().c_str());
				readersRead2.push_back(bclr);

				bclr->readHeader(&size);
				if (size > readCount)
					readCount = size;
			}

			// a Barcode 1 cycle ?
			if (cycleNum >= exp->b1start && cycleNum <= exp->b1end) {
				BCLReader *bclr = new bcltofastq::BCLReader(bclFilename.str().c_str());
				readersBarcode1.push_back(bclr);

				bclr->readHeader(&size);
				if (size > readCount)
					readCount = size;
			}

			// a Barcode 2 cycle ?
			if (cycleNum >= exp->b2start && cycleNum <= exp->b2end) {
				BCLReader *bclr = new bcltofastq::BCLReader(bclFilename.str().c_str());
				readersBarcode2.push_back(bclr);

				bclr->readHeader(&size);
				if (size > readCount)
					readCount = size;
			}

		}
		readersRead1.push_back(new bcltofastq::NullBCLReader());
		if (readersRead2.size() > 0)
			readersRead2.push_back(new bcltofastq::NullBCLReader());
		if (readersBarcode1.size() > 0)
			readersBarcode1.push_back(new bcltofastq::NullBCLReader());
		if (readersBarcode2.size() > 0)
			readersBarcode2.push_back(new bcltofastq::NullBCLReader());

		// create the read header constant base
		std::stringstream readHeaderBase;
		readHeaderBase << "@" << exp->instrumentName << ":" << exp->runId << ":" << exp->flowcellId << ":" << lane->number << ":" << tileName << ":";
		this->readHeaderBase = readHeaderBase.str();

		// create the filter file reader
		std::stringstream filterFilenameSS;
		filterFilenameSS << exp->baseCallsDir << "/" << lane->getName() << "/s_" << lane->number << "_" << tileName << ".filter";
		filterReader = new FilterReader(filterFilenameSS.str());

		if (filterReader->size() != readCount) {
			//TODO: do something (warning) here
		}

		// create location file reader (depending on what type of file exists)
		std::stringstream locationFilenameBaseSS;
		locationFilenameBaseSS << exp->locationDir << "/" << lane->getName() << "/s_" << lane->number << "_" << tileName;
		std::string locationFilenameBase = locationFilenameBaseSS.str();
		if (isFile((locationFilenameBase + ".clocs").c_str()))
			locationReader = new ClocsReader((locationFilenameBase + ".clocs").c_str());
//		else if (isFile((locationFilenameBase + ".locs").c_str()))
//			locationReader = new LocsReader((locationFilenameBase + ".locs").c_str());
		if (locationReader == NULL) {
			//TODO: do something (warning) here
			std::cerr << "Failed find Location file: " << ((locationFilenameBase + ".clocs").c_str()) << std::endl;
		} else if (!locationReader->isOpen()) {
			delete locationReader;
			locationReader = NULL;
			//TODO: do something (warning) here
			std::cerr << "Failed to open Location Reader: " << ((locationFilenameBase + ".clocs").c_str()) << std::endl;
		}

		open = true;
	}

	return open;
}

/**
 * Returns the number of Reads in this Tile.
 */
size_t TileReader::size() {

	if (!open)
		openTile();

	if (open)
		return readCount;
	return 0;
}

/**
 * Gets the next Read from the Readers.
 *
 * Returns NULL if reading read failed (or files exhausted)
 * If 'out' was provided read information is added to it otherwise and new
 * instance of PairedRead or SingleRead is returned (and caller must delete it)
 */
Read *TileReader::next(Read* out /*=NULL*/) {

	if (!open)
		openTile();

	if (readNumber >= readCount) {
		return NULL;
	}

	if (out == NULL)
		out = new Read();

	// make sure enough memory is available
	out->allocate(paired, barcodeCount, maxReadLength, headerLength, maxBarcodeLength);

	// read base/qual from each cycle
	//TODO: use openmp to make this parallel
	size_t bi = 0;
	std::vector<bcltofastq::BCLReader*>::iterator ri;

	// read 1
	char *outReadBuffer = out->getRead1();
	char *outQualBuffer = out->getQual1();
	for (bi = 0, ri = readersRead1.begin(); ri != readersRead1.end(); ++ri, ++bi) {
		BCLReader *bcl = *ri;
		if (!bcl->readBaseQual(&outReadBuffer[bi], &outQualBuffer[bi])) {
			outReadBuffer[bi] = missingBase;
			outQualBuffer[bi] = missingQual;
		}
	}

	// read 2
	outReadBuffer = out->getRead2();
	outQualBuffer = out->getQual2();
	for (bi = 0, ri = readersRead2.begin(); ri != readersRead2.end(); ++ri, ++bi) {
		BCLReader *bcl = *ri;
		if (!bcl->readBaseQual(&outReadBuffer[bi], &outQualBuffer[bi])) {
			outReadBuffer[bi] = missingBase;
			outQualBuffer[bi] = missingQual;
		}
	}

	// barcode 1
	char buf;
	outReadBuffer = out->getBarcode1();
	for (bi = 0, ri = readersBarcode1.begin(); ri != readersBarcode1.end(); ++ri, ++bi) {
		BCLReader *bcl = *ri;
		if (!bcl->readBaseQual(&outReadBuffer[bi], &buf)) {
			outReadBuffer[bi] = missingBase;
		}
	}

	// barcode 2
	outReadBuffer = out->getBarcode2();
	for (bi = 0, ri = readersBarcode2.begin(); ri != readersBarcode2.end(); ++ri, ++bi) {
		BCLReader *bcl = *ri;
		if (!bcl->readBaseQual(&outReadBuffer[bi], &buf)) {
			outReadBuffer[bi] = missingBase;
		}
	}


	// get the chastity flag
	char chast = filterReader->next();
	out->setChastityFiltered(chast == 'Y');

	// read location data
	unsigned int loc[2];
	if (locationReader == NULL || !(locationReader->readLocation(loc))) {
		loc[0] = readNumber;
		loc[1] = 999;
	}

	// construct read header
	//@HWI-M01178:19:000000000-D0L4W:1:1101:15820:1333 1:N:0:GAATCT
	char *header = out->getHeader();

	size_t offset = 0;
	strcpy(header, readHeaderBase.c_str());
	offset = readHeaderBase.size();

	// write the location
	int c = sprintf(&header[offset], "%i:%i ", loc[0], loc[1]);
	if (c >= 0)
		offset += c;
	out->setHeaderReadNumberPosition(offset);

	// write read info
	c = sprintf(&header[offset], "?:%c:0:", chast);

	// increment position
	++readNumber;

	return out;

}

/**
 * Checks if filename exists and is a file (regular, symlink or fifo
 */
bool TileReader::isFile(const char* filename) {
	struct stat st;
	return stat(filename,&st) == 0 && (st.st_mode & (S_IFREG | S_IFLNK | S_IFIFO)) != 0;
}


} /* namespace bcltofastq */
