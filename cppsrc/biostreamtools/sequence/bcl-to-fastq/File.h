/* 
 *  Created on: 27/11/2015
 *      Author: arobinson
 */

#include <cstring>

#ifndef FILE_H_
#define FILE_H_

namespace bcltofastq {

/**
 * Abstract super-class for file/directory objects which contains many
 * (protected) supporting functions that are useful for common tasks
 */
class AbstractFile {
protected:
	char* filename;

	/**
	 * Checks if filename exists or not
	 */
	bool _exists(const char* filename);

	/**
	 * Checks if filename is readable
	 */
	bool _readable(const char* filename);

	/**
	 * Checks if filename is writable
	 */
	bool _writable(const char* filename);

	/**
	 * Checks if filename is writable (and executable)
	 */
	bool _writableDir(const char* filename);

	/**
	 * Checks if filename is a file
	 */
	bool _file(const char* filename);

	/**
	 * Checks if filename is a directory
	 */
	bool _directory(const char* filename);

	/**
	 * Makes a copy of the filename
	 */
	char *_cloneFilename(const char* filename);

public:
	AbstractFile(const char* filename);
	AbstractFile(const AbstractFile &filename);
	virtual ~AbstractFile();
};

/**
 * Basic file object for manipulating files
 */
class File : public AbstractFile {
public:
	File(const char* filename);
	virtual ~File();

	/**
	 * Checks if filename exists
	 */
	bool exists();

	/**
	 * Returns the filename
	 */
	const char* name();

	/**
	 * Gets filename as a string
	 */
	std::string str();
};

} /* namespace bcltofastq */
#endif /* FILE_H_ */
