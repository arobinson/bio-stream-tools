/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ConfigReader_unittest.cpp
 *
 *  Created on: 25 Nov 2015
 *      Author: arobinson
 */

#include "ConfigReader.h"
#include <gtest/gtest.h>


TEST(ConfigReader, NewDelete) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	EXPECT_TRUE(reader != NULL);

	delete reader;
}

TEST(ConfigReader, DataOk) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	EXPECT_TRUE(reader->isOk());

	delete reader;
}

TEST(ConfigReader, MissingFile) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/notexists.xml");

	EXPECT_FALSE(reader->isOk());

	delete reader;
}

TEST(ConfigReader, CorruptFile) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/corrupt.xml");

	EXPECT_FALSE(reader->isOk());

	delete reader;
}

TEST(ConfigReader, MissingData) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/missing.xml");

	EXPECT_FALSE(reader->isOk());

	delete reader;
}

TEST(ConfigReader, MissingDataPartOk) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/missing.xml");

	std::string value = reader->getFlowcellId();
	EXPECT_EQ(value, "000000000-D0L4W");

	delete reader;
}

TEST(ConfigReader, ReadLanes) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	std::vector<int> lanes = reader->getLanes();
	EXPECT_EQ(lanes.size(), 1);
	EXPECT_EQ(*lanes.begin(), 1);

	delete reader;
}

TEST(ConfigReader, ReadCycles) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	std::vector<int> cycles = reader->getCycles();
	EXPECT_EQ(cycles.size(), 251);
	EXPECT_EQ(*cycles.begin(), 1);
	EXPECT_EQ(*cycles.rbegin(), 251);

	delete reader;
}

TEST(ConfigReader, ReadTiles) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	std::vector<std::string> tiles = reader->getTiles();
	EXPECT_EQ(tiles.size(), 2);
	EXPECT_EQ(*tiles.begin(), "1101");
	EXPECT_EQ(*tiles.rbegin(), "1102");

	delete reader;
}

TEST(ConfigReader, ReadInstrument) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	std::string value = reader->getInstrumentName();
	EXPECT_EQ(value, "HWI-M01178");

	delete reader;
}

TEST(ConfigReader, ReadRunFolderId) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	std::string value = reader->getRunFolderId();
	EXPECT_EQ(value, "19");

	delete reader;
}

TEST(ConfigReader, ReadRunFlowcellId) {

	bcltofastq::ConfigReader *reader = new bcltofastq::ConfigReader("data/test/ConfigReader/config.xml");

	std::string value = reader->getFlowcellId();
	EXPECT_EQ(value, "000000000-D0L4W");

	delete reader;
}

