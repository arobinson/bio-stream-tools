/**
 * Copyright (c) 2016, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * IntegerRange_test.cpp
 *
 *  Created on: 8 Jan 2016
 *      Author: arobinson
 */

#include "IntegerRange.h"

#include <gtest/gtest.h>


TEST(IntegerRange, NewDelete1) {
	bcltofastq::IntegerRange *o = new bcltofastq::IntegerRange("0-1");
	EXPECT_TRUE(NULL != o);
	delete o;
}
TEST(IntegerRange, NewDelete2) {
	bcltofastq::IntegerRange *o = new bcltofastq::IntegerRange(0,1);
	EXPECT_TRUE(NULL != o);
	delete o;
}

TEST(IntegerRange, MinMaxOrdered) {
	bcltofastq::IntegerRange o(1,3);
	EXPECT_EQ(1, o.min());
	EXPECT_EQ(3, o.max());
	EXPECT_EQ(1, o.begin());
	EXPECT_EQ(4, o.end());
	EXPECT_TRUE(o.ok());
}
TEST(IntegerRange, MinMaxSame) {
	bcltofastq::IntegerRange o(2,2);
	EXPECT_EQ(2, o.min());
	EXPECT_EQ(2, o.max());
	EXPECT_EQ(2, o.begin());
	EXPECT_EQ(3, o.end());
	EXPECT_TRUE(o.ok());
}
TEST(IntegerRange, MinMaxReverse) {
	bcltofastq::IntegerRange o(3,2);
	EXPECT_FALSE(o.ok());
}
TEST(IntegerRange, MinMaxMinNegative) {
	bcltofastq::IntegerRange o(-1,2);
	EXPECT_FALSE(o.ok());
}
TEST(IntegerRange, MinMaxMaxNegative) {
	bcltofastq::IntegerRange o(1,-2);
	EXPECT_FALSE(o.ok());
}
TEST(IntegerRange, MinMaxBothNegative) {
	bcltofastq::IntegerRange o(-2,-1);
	EXPECT_FALSE(o.ok());
}

TEST(IntegerRange, ParseOrdered) {
	bcltofastq::IntegerRange o("1-3");
	EXPECT_EQ(1, o.min());
	EXPECT_EQ(3, o.max());
	EXPECT_EQ(1, o.begin());
	EXPECT_EQ(4, o.end());
	EXPECT_TRUE(o.ok());
}
TEST(IntegerRange, ParseSame) {
	bcltofastq::IntegerRange o("2-2");
	EXPECT_EQ(2, o.min());
	EXPECT_EQ(2, o.max());
	EXPECT_EQ(2, o.begin());
	EXPECT_EQ(3, o.end());
	EXPECT_TRUE(o.ok());
}
TEST(IntegerRange, ParseReverse) {
	bcltofastq::IntegerRange o("3-2");
	EXPECT_FALSE(o.ok());
}
TEST(IntegerRange, ParseSingle0) {
	bcltofastq::IntegerRange o("0");
	EXPECT_EQ(0, o.min());
	EXPECT_EQ(0, o.max());
	EXPECT_EQ(0, o.begin());
	EXPECT_EQ(1, o.end());
	EXPECT_TRUE(o.ok());
}
TEST(IntegerRange, ParseSingle1) {
	bcltofastq::IntegerRange o("1");
	EXPECT_EQ(1, o.min());
	EXPECT_EQ(1, o.max());
	EXPECT_EQ(1, o.begin());
	EXPECT_EQ(2, o.end());
	EXPECT_TRUE(o.ok());
}
TEST(IntegerRange, ParseSingleN1) {
	bcltofastq::IntegerRange o("-1");
	EXPECT_FALSE(o.ok());
}

TEST(IntegerRange, InRange1) {
	bcltofastq::IntegerRange o("2-4");
	EXPECT_FALSE(o.in(1));
	EXPECT_TRUE(o.in(2));
	EXPECT_TRUE(o.in(3));
	EXPECT_TRUE(o.in(4));
	EXPECT_FALSE(o.in(5));
}
TEST(IntegerRange, InRange2) {
	bcltofastq::IntegerRange o("2");
	EXPECT_FALSE(o.in(1));
	EXPECT_TRUE(o.in(2));
	EXPECT_FALSE(o.in(3));
}



