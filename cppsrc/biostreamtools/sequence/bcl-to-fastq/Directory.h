/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 27/11/2015
 *      Author: arobinson
 */

#include <string>
#include <vector>

#include <cstring>

#include <libbiostreams/io/OutputBase.h>

#include "File.h"

#ifndef DIRECTORY_H_
#define DIRECTORY_H_

namespace bcltofastq {

class Directory: public bcltofastq::AbstractFile {

protected:

	/**
	 * Returns the parent directory of a filename
	 */
	char* dirname(const char* filename);

	/**
	 * Checks if str ends with search.
	 */
	bool endsWith(std::string &str, const char *search);

	/**
	 * Attempts to create directory called dirname.  If parents is true then it
	 * will attempt to create any necessary parents
	 */
	bool _createDirectory(const char* dirname, bool parents);
public:
	Directory(const char* filename);
	Directory(const Directory& dir);
	virtual ~Directory();

	/**
	 * Checks if filename exists
	 */
	bool exists();

	/**
	 * Checks if this directory can be created.
	 *
	 * I.e. checks
	 * parents == false
	 * - parent directory exists
	 * - parent directory is a directory
	 * - parent directory has writable permission
	 *
	 * parents == true
	 * - last existing parent is directory
	 * - last existing parent has writable permission
	 */
	bool canCreate(bool parents = false);

	/**
	 * Create the directory (if not already)
	 *
	 * If parents is true then it will create any required parent directories too.
	 */
	bool create(bool parents = false);

	/**
	 * Gets a list of files in a directory.  If starts and/or ends is specified then it
	 * filters the results so it only contains files that start and/or end with the
	 * provided string.
	 */
	const std::vector<std::string> listDirectory(const char *starts = NULL, const char *ends = NULL);

	/**
	 * Gets the parent directory of this directory
	 */
	Directory parent();

	/**
	 * Returns the directory name
	 */
	const char* name();

	/**
	 * Gets filename as a string
	 */
	std::string str();

	/**
	 * Creates a new output stream for the specified filename.  Files are
	 * relative to the directory.
	 *
	 * TODO: make this create sub-directories automatically.
	 */
	OutputBase *makeOutputStream(std::string filename);

	/**
	 * Creates a new file relative to this directory
	 */
	File *newFile(std::string filename);
};

} /* namespace bcltofastq */
#endif /* DIRECTORY_H_ */
