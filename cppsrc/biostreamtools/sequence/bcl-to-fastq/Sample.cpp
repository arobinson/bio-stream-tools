/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Sample.cpp
 *
 *  Created on: 18 Dec 2015
 *      Author: arobinson
 */

#include <cstdlib>

#include "Sample.h"

namespace bcltofastq {

Sample::Sample() {
	laneId = 0;
}

Sample::~Sample() {
}



/**
 * Returns the laneId for this lane.  Requires that a column with name 'Lane'.
 */
int Sample::getLaneId() {
	if (laneId == 0) {
		std::map<std::string, std::string>::iterator l = values.find("Lane");
		if (l != values.end()) {
			laneId = atoi(l->second.c_str());
		}
	}
	return laneId;
}

/**
 * Adds a value to the sample annotation
 */
void Sample::addValue(std::string name, std::string value) {
	values[name] = value;
}

/**
 * Gets the value for the given annotation
 */
std::string Sample::getValue(std::string name) {
	std::map<std::string, std::string>::iterator mit = values.find(name);
	if (mit != values.end())
		return mit->second;
	return "";
}


} /* namespace bcltofastq */
