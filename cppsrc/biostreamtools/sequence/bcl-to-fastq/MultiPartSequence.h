/*
 * MultiPartSequence.h
 *
 *  Created on: 26 Nov 2015
 *      Author: arobinson
 */

#include <iostream>
#include <libbiostreams/io/OutputBase.h>

#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_MULTIPARTSEQUENCE_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_MULTIPARTSEQUENCE_H_

namespace bcltofastq {

class MultiPartSequence;
class Part;

/**
 * Output printer class to select a section of a MultiPartSequence instance
 */
class Part {
protected:
	MultiPartSequence *seq;
	size_t part;

public:
	Part(MultiPartSequence *seq, size_t part);

    friend std::ostream &bcltofastq::operator<<(std::ostream &out, bcltofastq::Part &c);
    friend std::ostream &bcltofastq::operator<<(std::ostream &out, bcltofastq::Part *c);

    friend OutputBase &bcltofastq::operator<<(OutputBase &out, bcltofastq::Part &c);
    friend OutputBase &bcltofastq::operator<<(OutputBase &out, bcltofastq::Part *c);

};

class MultiPartSequence {
	friend class Part;
public:

	char* header;
	char* baseSequence;
	char* baseQuality;

	size_t nParts; ///< number of parts that are used
	char** sequenceParts;
	char** qualityParts;
protected:

	size_t headerLength;
	size_t length; ///< number of sequence bases required (baseSequence size will be length + nParts)

public:
	MultiPartSequence(size_t headerLength, size_t length, size_t numberParts, size_t* partIndexes);
	virtual ~MultiPartSequence();

    friend std::ostream &bcltofastq::operator<<(std::ostream &out, bcltofastq::Part &c);
    friend std::ostream &bcltofastq::operator<<(std::ostream &out, bcltofastq::Part *c);

    friend OutputBase &bcltofastq::operator<<(OutputBase &out, bcltofastq::Part &c);
    friend OutputBase &bcltofastq::operator<<(OutputBase &out, bcltofastq::Part *c);
};

} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_MULTIPARTSEQUENCE_H_ */
