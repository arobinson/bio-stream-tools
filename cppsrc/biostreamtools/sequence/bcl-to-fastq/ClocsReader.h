/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ClocsReader.h
 *
 *  Created on: 24 Nov 2015
 *      Author: arobinson
 */

#include <string>

#include <cstdio>

#include "LocationReader.h"

#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_CLOCSREADER_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_CLOCSREADER_H_

namespace bcltofastq {

/**
 * Reads XY Locations from a CLOCS file
 */
class ClocsReader : public LocationReader {
protected:
	FILE *pFile;
	bool headerRead;

	unsigned int xOffset;
	unsigned int yOffset;
//	const int imageWidth = 2048;
//	const int blockSize = 25;
//	const int maxXBins = ceil(imageWidth / blockSize);

	unsigned int numBins;
	unsigned int numBlocks;

	size_t iBin;
	size_t iBlock;

	std::string status;

public:
	ClocsReader(const char* file);
	~ClocsReader();

	/**
	 * Returns true if the file is openable and header read successfully
	 */
	bool isOpen();

	/**
	 * Gets the number of Bins in the file
	 */
	unsigned int getNumBins();

	/**
	 * Reads an XY pair from CLOCS file and places it in loc (expects 2 element array)
	 */
	bool readLocation(unsigned int* loc);

	/**
	 * Gets the current status of the reader
	 */
	std::string getStatus();

	/**
	 * Gets the current bin number of the reader
	 */
	size_t getBinNumber();

};

} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_CLOCSREADER_H_ */
