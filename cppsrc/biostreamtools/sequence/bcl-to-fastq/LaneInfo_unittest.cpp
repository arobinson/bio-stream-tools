/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ConfigReader_unittest.cpp
 *
 *  Created on: 8 Dec 2015
 *      Author: arobinson
 */

#include <vector>

#include "LaneInfo.h"
#include <gtest/gtest.h>


TEST(LaneInfo, NewDelete) {

	bcltofastq::LaneInfo *obj = new bcltofastq::LaneInfo(1, std::vector<int>());

	EXPECT_TRUE(obj != NULL);

	delete obj;
}

TEST(LaneInfo, Name1) {
	bcltofastq::LaneInfo obj(1, std::vector<int>());

	EXPECT_EQ("L001", obj.getName());
}

TEST(LaneInfo, Name0) {
	bcltofastq::LaneInfo obj(0, std::vector<int>());

	EXPECT_EQ("L000", obj.getName());
}

TEST(LaneInfo, Name8) {
	bcltofastq::LaneInfo obj(8, std::vector<int>());

	EXPECT_EQ("L008", obj.getName());
}

TEST(LaneInfo, Name10) {
	bcltofastq::LaneInfo obj(10, std::vector<int>());

	EXPECT_EQ("L010", obj.getName());
}

TEST(LaneInfo, Name1000) {
	bcltofastq::LaneInfo obj(1000, std::vector<int>());

	EXPECT_EQ("L1000", obj.getName());
}

TEST(LaneInfo, NameN1) {
	bcltofastq::LaneInfo obj(-1, std::vector<int>());

	EXPECT_EQ("L-01", obj.getName());
}

TEST(LaneInfo, NameN100) {
	bcltofastq::LaneInfo obj(-100, std::vector<int>());

	EXPECT_EQ("L-100", obj.getName());
}



TEST(LaneInfo, Cycles) {
	std::vector<int> cyc;
	cyc.push_back(1);
	cyc.push_back(2);
	bcltofastq::LaneInfo obj(1, cyc);

	EXPECT_EQ(2, obj.cycles.size());
}

TEST(LaneInfo, CyclesCopy) {
	std::vector<int> cyc;
	cyc.push_back(1);
	cyc.push_back(2);
	bcltofastq::LaneInfo obj(1, cyc);
	cyc.push_back(3);

	EXPECT_EQ(2, obj.cycles.size());
}

