/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ConfigReader_unittest.cpp
 *
 *  Created on: 8 Dec 2015
 *      Author: arobinson
 */

#include "Read.h"
#include <gtest/gtest.h>


TEST(Read, NewDelete) {

	bcltofastq::Read *obj = new bcltofastq::Read();

	EXPECT_TRUE(obj != NULL);

	delete obj;
}

TEST(Read, BarcodeCount0) {
	bcltofastq::Read obj(false, 0, 10, 10, 10);

	EXPECT_EQ(0, obj.getBarcodeCount());
}
TEST(Read, BarcodeCount1) {
	bcltofastq::Read obj(false, 1, 10, 10, 10);

	EXPECT_EQ(1, obj.getBarcodeCount());
}
TEST(Read, BarcodeCount2) {
	bcltofastq::Read obj(false, 2, 10, 10, 10);

	EXPECT_EQ(2, obj.getBarcodeCount());
}
TEST(Read, BarcodeCount3) {
	bcltofastq::Read obj(false, 3, 10, 10, 10);

	EXPECT_EQ(2, obj.getBarcodeCount());
}

TEST(Read, PairedTrue) {
	bcltofastq::Read obj(true, 0, 10, 10, 10);

	EXPECT_TRUE(obj.isPaired());
}
TEST(Read, PairedFalse) {
	bcltofastq::Read obj(false, 0, 10, 10, 10);

	EXPECT_FALSE(obj.isPaired());
}

TEST(Read, ExpandCopyHeader) {
	bcltofastq::Read obj(false, 0, 10, 10, 10);
	char *h = obj.getHeader();
	strcpy(h, "@hello");
	obj.allocate(false, 0, 15, 10, 10);
	EXPECT_EQ(0, strcmp(obj.getHeader(), "@hello"));
}
//TEST(Read, ShrinkTruncateHeader) {
//	bcltofastq::Read obj(false, 0, 10, 10, 10);
//	char *h = obj.getHeader();
//	strcpy(h, "@hello");
//	obj.allocate(false, 0, 5, 10, 10);
//	EXPECT_EQ(0, strcmp(obj.getHeader(), "@hell"));
//}

TEST(Read, Allocation1) {
	bcltofastq::Read obj(false, 0, 10, 10, 10);
	EXPECT_EQ(NULL, obj.getBarcode1());
	EXPECT_EQ(NULL, obj.getBarcode2());
	EXPECT_TRUE(NULL != obj.getRead1());
	EXPECT_EQ(NULL, obj.getRead2());
	EXPECT_TRUE(NULL != obj.getQual1());
	EXPECT_EQ(NULL, obj.getQual2());
	EXPECT_TRUE(NULL != obj.getHeader());
}
TEST(Read, Allocation2) {
	bcltofastq::Read obj(true, 0, 10, 10, 10);
	EXPECT_EQ(NULL, obj.getBarcode1());
	EXPECT_EQ(NULL, obj.getBarcode2());
	EXPECT_TRUE(NULL != obj.getRead1());
	EXPECT_TRUE(NULL != obj.getRead2());
	EXPECT_TRUE(NULL != obj.getQual1());
	EXPECT_TRUE(NULL != obj.getQual2());
	EXPECT_TRUE(NULL != obj.getHeader());
}
TEST(Read, Allocation3) {
	bcltofastq::Read obj(false, 1, 10, 10, 10);
	EXPECT_TRUE(NULL != obj.getBarcode1());
	EXPECT_EQ(NULL, obj.getBarcode2());
	EXPECT_TRUE(NULL != obj.getRead1());
	EXPECT_EQ(NULL, obj.getRead2());
	EXPECT_TRUE(NULL != obj.getQual1());
	EXPECT_EQ(NULL, obj.getQual2());
	EXPECT_TRUE(NULL != obj.getHeader());
}
TEST(Read, Allocation4) {
	bcltofastq::Read obj(false, 2, 10, 10, 10);
	EXPECT_TRUE(NULL != obj.getBarcode1());
	EXPECT_TRUE(NULL != obj.getBarcode2());
	EXPECT_TRUE(NULL != obj.getRead1());
	EXPECT_EQ(NULL, obj.getRead2());
	EXPECT_TRUE(NULL != obj.getQual1());
	EXPECT_EQ(NULL, obj.getQual2());
	EXPECT_TRUE(NULL != obj.getHeader());
}
TEST(Read, Allocation5) {
	bcltofastq::Read obj(true, 2, 10, 10, 10);
	EXPECT_TRUE(NULL != obj.getBarcode1());
	EXPECT_TRUE(NULL != obj.getBarcode2());
	EXPECT_TRUE(NULL != obj.getRead1());
	EXPECT_TRUE(NULL != obj.getRead2());
	EXPECT_TRUE(NULL != obj.getQual1());
	EXPECT_TRUE(NULL != obj.getQual2());
	EXPECT_TRUE(NULL != obj.getHeader());
}


















