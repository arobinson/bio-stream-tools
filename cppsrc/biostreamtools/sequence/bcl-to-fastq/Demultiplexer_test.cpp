/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Demultiplexer_test.cpp
 *
 *  Created on: 22 Dec 2015
 *      Author: arobinson
 */

#include "Demultiplexer.h"

#include <vector>
#include <gtest/gtest.h>


TEST(Demultiplexer, ReplaceAll) {

	std::string temp = "Project_PROJECT_Sample_SAMPLE.fq.gz";
	bcltofastq::replaceAll(temp, "PROJECT", "Hello");
	EXPECT_EQ("Project_Hello_Sample_SAMPLE.fq.gz", temp);
}
TEST(Demultiplexer, ReplaceAllStart) {

	std::string temp = "Project_PROJECT_Sample_SAMPLE.fq.gz";
	bcltofastq::replaceAll(temp, "Project", "Hello");
	EXPECT_EQ("Hello_PROJECT_Sample_SAMPLE.fq.gz", temp);
}
TEST(Demultiplexer, ReplaceAllEnd) {

	std::string temp = "Project_PROJECT_Sample_SAMPLE.fq.gz";
	bcltofastq::replaceAll(temp, "gz", "Hello");
	EXPECT_EQ("Project_PROJECT_Sample_SAMPLE.fq.Hello", temp);
}
TEST(Demultiplexer, ReplaceAllEmptySearch) {

	std::string temp = "Project_PROJECT_Sample_SAMPLE.fq.gz";
	bcltofastq::replaceAll(temp, "", "Hello");
	EXPECT_EQ("Project_PROJECT_Sample_SAMPLE.fq.gz", temp);
}
TEST(Demultiplexer, ReplaceAllEmptyReplacement) {

	std::string temp = "Project_PROJECT_Sample_SAMPLE.fq.gz";
	bcltofastq::replaceAll(temp, "Sample", "");
	EXPECT_EQ("Project_PROJECT__SAMPLE.fq.gz", temp);
}

TEST(Demultiplexer, FilterBarcode) {

//	std::string bc = "ATCG";
//	std::string result = bcltofastq::filterBarcode("ATCG");
//	std::string expected = "ATCG";
	EXPECT_EQ("ATCG", bcltofastq::filterBarcode("ATCG"));
}
TEST(Demultiplexer, FilterBarcodeEmpty) {

	std::string bc = "";
	EXPECT_EQ("", bcltofastq::filterBarcode(bc));
}
TEST(Demultiplexer, FilterBarcodeEmptyBad) {

	std::string bc = "-";
	EXPECT_EQ("", bcltofastq::filterBarcode(bc));
}
TEST(Demultiplexer, FilterBarcodePartial) {

	std::string bc = "AT-CG";
	EXPECT_EQ("ATCG", bcltofastq::filterBarcode(bc));
}

TEST(Demultiplexer, FilterBarcodeEnd) {

	std::string bc = "ATCG-";
	EXPECT_EQ("ATCG", bcltofastq::filterBarcode(bc));
}
TEST(Demultiplexer, FilterBarcodeStart) {

	std::string bc = "-ATCG";
	EXPECT_EQ("ATCG", bcltofastq::filterBarcode(bc));
}

TEST(Demultiplexer, GenerateFilenameBlanks) {

	std::string temp = "Project_SAMPLEPROJECT_Sample_SAMPLEID.fq.gz";
	bcltofastq::Sample sample;
	EXPECT_EQ("Project__Sample_.fq.gz", bcltofastq::generateFilename(temp, &sample));
}
TEST(Demultiplexer, GenerateFilenameFilled) {

	std::string temp = "Project_SAMPLEPROJECT_Sample_SAMPLEID.fq.gz";
	bcltofastq::Sample sample;
	sample.addValue("SampleProject", "Hello");
	sample.addValue("Sample_ID", "world");
	EXPECT_EQ("Project_Hello_Sample_world.fq.gz", bcltofastq::generateFilename(temp, &sample));
}



TEST(Demultiplexer, IndexBarcode_TAAA_NULL) {

	EXPECT_EQ(192, bcltofastq::indexBarcode("TAAA", NULL));
}
TEST(Demultiplexer, IndexBarcode_TA_AA) {

	EXPECT_EQ(192, bcltofastq::indexBarcode("TA", "AA"));
}
TEST(Demultiplexer, IndexBarcode_ACGT_EMPTY) {

	EXPECT_EQ(27, bcltofastq::indexBarcode("ACGT", ""));
}
TEST(Demultiplexer, IndexBarcode_acgt_NULL) {

	EXPECT_EQ(27, bcltofastq::indexBarcode("acgt", NULL));
}
TEST(Demultiplexer, IndexBarcode_NNN_NULL) {

	EXPECT_EQ(0, bcltofastq::indexBarcode("NNN", NULL));
}
TEST(Demultiplexer, IndexBarcode_TTTTTTTTTTTTTTTT_NULL) {

	EXPECT_EQ(4294967295, bcltofastq::indexBarcode("TTTTTTTTTTTTTTTT", NULL));
}
TEST(Demultiplexer, IndexBarcode_TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT_NULL) {

	EXPECT_EQ(0x3FFFFFFFFFFFFFFF, bcltofastq::indexBarcode("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT", NULL));
}
TEST(Demultiplexer, IndexBarcode_TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT_NULL) {

	EXPECT_EQ(0xFFFFFFFFFFFFFFFF, bcltofastq::indexBarcode("TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT", NULL));
}


TEST(Demultiplexer, NewDelete) {

	//Demultiplexer(Directory outputDir, std::vector<bcltofastq::Sample*> samples, unsigned short mismatches);

	bcltofastq::Directory dir("/tmp/Demultiplexer_test");
	std::vector<bcltofastq::Sample*> samples;
	bcltofastq::Sample *samp = new bcltofastq::Sample();
	samp->addValue("Lane", "1");
	samp->addValue("Index", "ACTG");
	samp->addValue("Sample_ID", "Samp1");
	samp->addValue("SampleProject", "HelloWorld");
	samples.push_back(samp);

	bcltofastq::Demultiplexer *obj = new bcltofastq::Demultiplexer(dir, samples, 1, 0, true);

	EXPECT_TRUE(obj != NULL);

	delete obj;
	delete samp;
}



