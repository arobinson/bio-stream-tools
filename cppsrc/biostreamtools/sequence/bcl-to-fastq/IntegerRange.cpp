/**
 * Copyright (c) 2016, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * IntegerRange.cpp
 *
 *  Created on: 8 Jan 2016
 *      Author: arobinson
 */

#include <cstdlib>

#include "IntegerRange.h"

namespace bcltofastq {

/**
 * Constructs a range from a string representation.
 *
 * MIN[-MAX] where MIN and MAX are positive integers and are inclusive
 */
IntegerRange::IntegerRange(std::string value) {
	_min = _max = 0;
	_ok = false;

	size_t pos = 0;
	if ((pos = value.find("-")) == value.npos) {
		if (isInt(value)) {
			_min = _max = atoi(value.c_str());
			_ok = true;
		}
	} else {
		std::string from = value.substr(0, pos);
		std::string to = value.substr(pos+1);
		if (isInt(from) && isInt(to)) {
			int fromi = atoi(from.c_str());
			int toi = atoi(to.c_str());
			if (fromi <= toi) {
				_min = fromi;
				_max = toi;
				_ok = true;
			}
		}
	}
}
/**
 * Constructs a range from a minimum and maximum value.
 *
 * min and max are inclusive
 * min <= max must be true
 */
IntegerRange::IntegerRange(int min, int max) {
	_min = _max = 0;
	_ok = false;

	if (min <= max && min >= 0) {
		_ok = true;
		_min = min;
		_max = max;
	}
}


IntegerRange::~IntegerRange() {
}

/**
 * Returns the smallest value in the range
 */
int IntegerRange::min() {
	return _min;
}
/**
 * Returns the largest value in the range
 */
int IntegerRange::max() {
	return _max;
}
/**
 * Returns the smallest value in the range
 */
int IntegerRange::begin() {
	return _min;
}
/**
 * returns 1 larger than the largest value in the range
 */
int IntegerRange::end() {
	return _max + 1;
}

/**
 * Checks if the range was parsed ok
 */
bool IntegerRange::ok() {

}

/**
 * Checks if value is within the range
 */
bool IntegerRange::in(int value) {
	return value >= _min && value <= _max;
}

// --------------------------------------------------

/**
 * Checks if all letters in a string are digits (and at least 1 digit)
 */
bool IntegerRange::isInt(const std::string &value) {
	std::string::const_iterator it;
	if (value.size() == 0)
		return false;
	for (it = value.begin(); it != value.end(); ++it)
		if (*it < '0' || *it > '9')
			return false;
	return true;
}


} /* namespace bcltofastq */
