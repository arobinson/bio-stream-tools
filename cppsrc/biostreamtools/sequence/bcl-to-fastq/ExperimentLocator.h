/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ExperimentLocator.h
 *
 *  Created on: 1 Dec 2015
 *      Author: arobinson
 */

#include <string>

#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_EXPERIMENTLOCATOR_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_EXPERIMENTLOCATOR_H_

namespace bcltofastq {

class ExperimentLocator {
protected:
	std::string basecallsDir;
	std::string basecallsConfig;
	std::string intensitiesConfig;
	std::string sampleSheet;

public:
	ExperimentLocator(const std::string dirhint, const std::string bcchint = "", const std::string ichint = "");
	virtual ~ExperimentLocator();

	/**
	 * Retrieve the directory containing the BaseCalls
	 */
	std::string getBasecallsDir();

	/**
	 * Retrieve the filename of the Basecalls Config file
	 */
	std::string getBasecallsConfig();

	/**
	 * Retrieve the filename of the Intensities Config file
	 */
	std::string getIntensitiesConfig();

	/**
	 * Retrieve the filename of the Sample Sheet file
	 */
	std::string getSampleSheet();

protected:

	/**
	 * Returns true if the path exists and is a directory
	 */
	inline bool isDir(const std::string &path);

	/**
	 * Returns true if a path exists and is a file
	 */
	inline bool isFile(const std::string &path);

	/**
	 * Checks if the named file is a Basecalls Config XML file.
	 *
	 * i.e. is it a valid XML file with a top level node 'BaseCallAnalysis'
	 */
	inline bool isBasecallsConfig(const std::string &filename);

	/**
	 * Checks if the named file is a Intensities Config XML file.
	 *
	 * i.e. is it a valid XML file with a top level node 'ImageAnalysis'
	 */
	inline bool isIntensitiesConfig(const std::string &filename);
};

} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_EXPERIMENTLOCATOR_H_ */
