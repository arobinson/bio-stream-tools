/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 27/11/2015
 *      Author: arobinson
 */

#include <iostream>
#include <sstream>
#include <string>

#include <sys/stat.h>
#include <dirent.h>

#include "Directory.h"

namespace bcltofastq {

Directory::Directory(const char* filename) : AbstractFile(filename) {
}


Directory::Directory(const Directory& dir) : AbstractFile(dir) {
}

Directory::~Directory() {
}

/**
 * Checks if filename exists
 */
bool Directory::exists() {
	return _directory(filename);
}

/**
 * Checks if this directory can be created.
 *
 * I.e. checks
 * parents == false
 * - parent directory exists
 * - parent directory is a directory
 * - parent directory has writable permission
 *
 * parents == true
 * - last existing parent is directory
 * - last existing parent has writable permission
 */
bool Directory::canCreate(bool parents) {

	if (parents) {
		// loop until a parent exists
		char *curdir = _cloneFilename(filename);
		while (curdir != NULL && curdir[0] != '\0') {
			if (_exists(curdir)) {
				// make sure it's a writable directory
				bool res = _directory(curdir) && _writableDir(curdir);
				delete [] curdir;
				return res;
			}
			// next parent
			char *tmp = dirname(curdir);
			delete [] curdir;
			curdir = tmp;
		}
	} else {
		// if it already exists (and is writable)
		if (_directory(filename) && _writableDir(filename))
			return true;

		// check parent directory exists and is writable
		char *curdir = dirname(filename);
		bool res = _directory(curdir) && _writableDir(curdir);
		delete [] curdir;
		return res;
	}
	return false;
}

/**
 * Create the directory (if not already)
 *
 * If parents is true then it will create any required parent directories too.
 */
bool Directory::create(bool parents) {

	// do nothing if already exists
	if (_directory(filename) && _writableDir(filename))
		return true;

	// make sure its possible to create
	if (!canCreate(parents)) {
		return false;
	}

	// create it
	return _createDirectory(filename, parents);
}

/**
 * Gets a list of files in a directory.  If starts and/or ends is specified then it
 * filters the results so it only contains files that start and/or end with the
 * provided string.
 */
const std::vector<std::string> Directory::listDirectory(const char *starts, const char *ends) {
	std::vector<std::string> result;
	struct dirent *entry;
	DIR *dp;
	dp = opendir(filename);
	if (dp != NULL) {

		while ((entry = readdir(dp))) {
			std::string filename(entry->d_name);
			if (filename != "." && filename != "..") {
				if (starts == NULL || filename.find(starts) == 0) {
					if (ends == NULL || endsWith(filename, ends))
						result.push_back(filename);
				}
			}
		}

		closedir(dp);
	}

	return result;
}
//int listdir(const char *path) {
//    struct dirent *entry;
//    DIR *dp;
//
//    dp = opendir(path);
//    if (dp == NULL) {
//        perror("opendir: Path does not exist or could not be read.");
//        return -1;
//    }
//
//    while ((entry = readdir(dp)))
//        puts(entry->d_name);
//
//    closedir(dp);
//    return 0;
//}
/**
 * Gets the parent directory of this directory
 */
Directory Directory::parent() {

	Directory dir(NULL);
	dir.filename = dirname(filename);
	return dir;
}

/**
 * Returns the directory name
 */
const char* Directory::name() {
	return filename;
}

/**
 * Gets filename as a string
 */
std::string Directory::str() {
	return std::string(filename);
}

/**
 * Creates a new output stream for the specified filename.  Files are
 * relative to the directory.
 *
 * TODO: implement this
 * TODO: make this create sub-directories automatically.
 */
OutputBase *Directory::makeOutputStream(std::string filename) {
	return NULL;
}

/**
 * Creates a new file relative to this directory
 *
 * Returns a new instance of File object (that caller owns) OR NULL on error.
 */
File *Directory::newFile(std::string filename) {

	if (filename.size() == 0)
		return NULL;

	if (filename[0] == '/')
		return new File(filename.c_str());

	std::stringstream ss;
	ss << this->filename << "/" << filename;
	return new File(ss.str().c_str());
}

// ----------------------------------------------------------------------------


/**
 * Attempts to create directory called dirname.  If parents is true then it
 * will attempt to create any necessary parents
 */
bool Directory::_createDirectory(const char* dname, bool parents) {
	if (parents) {
		// is already directory
		if (_directory(dname))
			return true;

		// check if parent exists
		char *pdir = dirname(dname);
		bool result = true;
		if (!_exists(pdir)) {
			result = _createDirectory(pdir, parents);
			if (result)
				result = mkdir(dname, 0777) == 0;

		} else if (!_directory(pdir) || !_writableDir(pdir)) {	// parent is a directory?
			result = false;

		} else {	// just create it
			result = mkdir(dname, 0777) == 0;
		}

		// cleanup
		delete [] pdir;
		return result;
	} else { // no parent creation
		return mkdir(dname, 0777) == 0;
	}
	return false;
}
/**
 * Returns the parent directory of a filename (without trailing slash)
 *
 * e.g.
 * "/tmp/hello" -> "/tmp"
 * "/tmp/hello/" -> "/tmp"
 * "/tmp" -> "/"
 * "/" -> ""
 * "./hello" -> "."
 * "." -> ""
 * "hello" -> "."
 * "" -> ""
 * NULL -> NULL
 */
char* Directory::dirname(const char* filename) {
	if (filename != NULL) {
		size_t i = 0;
		size_t lastSlash = 0;
		size_t lastlastSlash = 0;
		bool slashFound = false;

		// scan for the last slash
		for (i = 0; filename[i] != '\0'; ++i) {
			if (filename[i] == '/') {
				lastlastSlash = lastSlash;
				lastSlash = i;
				slashFound = true;
			}
		}

//		std::cerr << "i=" << i << ", lastSlash=" << lastSlash << ", lastlastSlash=" << lastlastSlash << std::endl;

		// check for trailing slash except on '/' (and undo it)
		if (lastSlash == i - 1 && i > 1) {
			lastSlash = lastlastSlash;
//			std::cerr << "Trailing slash" << std::endl;
		}

		// copy string
		char *result = new char[lastSlash + 1];
		if (lastSlash == 0) {
			if (filename[0] == '\0'
					|| ((filename[0] == '/' || filename[0] == '.') && filename[1] == '\0')
					|| (filename[0] == '.' && filename[1] == '/')) {
				result = new char[1];
				result[0] = '\0';
			} else if (filename[0] == '/') {
				result = new char[2];
				result[0] = '/';
				result[1] = '\0';
			} else {
				result = new char[2];
				result[0] = '.';
				result[1] = '\0';
			}
		} else {
			result = new char[lastSlash + 1];
			memcpy((void*)result, filename, lastSlash);
			result[lastSlash] = '\0';
		}

//		std::cerr << "filename='" << filename << "', result='" << result << "'" << std::endl;

		return result;
	}
	return NULL;
}

/**
 * Checks if str ends with search.
 */
bool Directory::endsWith(std::string &str, const char *search) {

	const char* strChar = str.c_str();
	size_t i = strlen(search)-1;
	size_t s = str.length()-1;
	while (true) { // loop (back to front) until we find a mismatch OR exhaust str or search
		if (strChar[s] != search[i]) {
			return false;
		}
		if (i == 0 || s == 0)
			break;
		--i;
		--s;
	}
	return true;
}

} /* namespace bcltofastq */
