/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * FilterReader.cpp
 *
 *  Created on: 7 Dec 2015
 *      Author: arobinson
 */

#include <cstdio>

#include "FilterReader.h"

namespace bcltofastq {

FilterReader::FilterReader(std::string filename) {
	pFile = fopen(filename.c_str(), "rb");
	isOpen = false;
	readCount = 0;
}

FilterReader::~FilterReader() {
	if (pFile != NULL)
		fclose(pFile);
	pFile = NULL;
}

/**
 * Returns the size of the filter file according to the header
 */
size_t FilterReader::size() {
	if (isOpen || readHeader()) {
		return readCount;
	}
	return 0; // ERROR
}

/**
 * Reads the filter status of the next read in the filter.
 *
 * Returns 'Y' (filtered), 'N' (ok) or '?' (error)
 */
char FilterReader::next() {
	if (isOpen || readHeader()) {
		size_t r;
		char b;
		r = fread(&b,1,1,pFile);
		if (r == 1) {
			return (b&1)==1?'N':'Y';
		}
	}
	return '?';
}

bool FilterReader::readHeader() {
	// skip over blank header (8 bytes)
	if (fseek(pFile, 8, SEEK_CUR) != 0)
		return false;

	// read size
	size_t r;
	unsigned char buf[4];
	r = fread(buf,1,4,pFile);
	if (r != 4)
		return false;
	unsigned int s = (unsigned)buf[0] + ((unsigned)buf[1] << 8) + ((unsigned)buf[2] << 16) + ((unsigned)buf[3] << 24);
	readCount = s;

	isOpen = true;

	return true;
}

} /* namespace bcltofastq */
