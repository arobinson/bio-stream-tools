/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ExperimentLocator.cpp
 *
 *  Created on: 1 Dec 2015
 *      Author: arobinson
 */

#include <iostream>
#include <string>
#include <vector>

#include <sys/stat.h>

#include "tinyxml2/tinyxml2.h"

#include "Directory.h"

#include "ExperimentLocator.h"

namespace bcltofastq {

ExperimentLocator::ExperimentLocator(const std::string expDirHint, const std::string basecallConfigHint, const std::string intensitiesConfigHint) {

	basecallsDir = "";
	basecallsConfig = "";
	intensitiesConfig = "";

	/// Find the basecalls ///
	if (isDir(expDirHint + "/Data")) {
		if (isDir(expDirHint + "/Data/Intensities")) {
			if (isDir(expDirHint + "/Data/Intensities/BaseCalls"))
				basecallsDir = expDirHint + "/Data/Intensities/BaseCalls";
		}
		if (basecallsDir.size() == 0 && isDir(expDirHint + "/Data/BaseCalls")) {
			basecallsDir = expDirHint + "/Data/BaseCalls";
		}
	}
	if (basecallsDir.size() == 0 && isDir(expDirHint + "/Intensities")) {
		if (isDir(expDirHint + "/Intensities/BaseCalls"))
			basecallsDir = expDirHint + "/Intensities/BaseCalls";
	}
	if (basecallsDir.size() == 0 && isDir(expDirHint + "/BaseCalls"))
		basecallsDir = expDirHint + "/BaseCalls";

	/// find the Basecalls config file ///
	if (basecallConfigHint.size() > 0) {

		// full spec config.xml file
		if (isFile(basecallConfigHint)
				&& isBasecallsConfig(basecallConfigHint)) {
			basecallsConfig = basecallConfigHint;
		}
		// relative (to dirhint) config.xml file
		if (basecallsConfig.size() == 0
				&& isFile(expDirHint + "/" + basecallConfigHint)
				&& isBasecallsConfig(expDirHint + "/" + basecallConfigHint)) {
			basecallsConfig = expDirHint + "/" + basecallConfigHint;
		}
		// full specified dir containing config.xml
		if (basecallsConfig.size() == 0
				&& isDir(basecallConfigHint)
				&& isBasecallsConfig(basecallConfigHint + "/config.xml")) {
			basecallsConfig = basecallConfigHint + "/config.xml";
		}
		// rel (to dirhint) dir containing config.xml
		if (basecallsConfig.size() == 0
				&& isDir(expDirHint + "/" + basecallConfigHint)
				&& isBasecallsConfig(expDirHint + "/" + basecallConfigHint + "/config.xml")) {
			basecallsConfig = expDirHint + "/" + basecallConfigHint + "/config.xml";
		}
	}
	if (basecallsConfig.size() == 0
			&& basecallsDir.size() != 0
			&& isFile(basecallsDir + "/config.xml")
			&& isBasecallsConfig(basecallsDir + "/config.xml")) {
		basecallsConfig = basecallsDir + "/config.xml";
	}

	/// find the Intensities config file ///
	if (intensitiesConfigHint.size() > 0) {

		// full spec config.xml file
		if (isFile(intensitiesConfigHint)
				&& isIntensitiesConfig(intensitiesConfigHint)) {
			intensitiesConfig = intensitiesConfigHint;
		}
		// relative (to bcdhint) config.xml file
		if (intensitiesConfig.size() == 0
				&& isFile(expDirHint + "/" + intensitiesConfigHint)
				&& isIntensitiesConfig(expDirHint + "/" + intensitiesConfigHint)) {
			intensitiesConfig = expDirHint + "/" + intensitiesConfigHint;
		}
		// full specified dir containing config.xml
		if (intensitiesConfig.size() == 0
				&& isDir(intensitiesConfigHint)
				&& isIntensitiesConfig(intensitiesConfigHint + "/config.xml")) {
			intensitiesConfig = intensitiesConfigHint + "/config.xml";
		}
		// rel (to bcdhint) dir containing config.xml
		if (intensitiesConfig.size() == 0
				&& isDir(expDirHint + "/" + intensitiesConfigHint)
				&& isIntensitiesConfig(expDirHint + "/" + intensitiesConfigHint + "/config.xml")) {
			intensitiesConfig = expDirHint + "/" + intensitiesConfigHint + "/config.xml";
		}
	}
	if (intensitiesConfig.size() == 0
			&& basecallsDir.size() != 0
			&& isFile(basecallsDir + "/../config.xml")
			&& isIntensitiesConfig(basecallsDir + "/../config.xml")) {
		intensitiesConfig = basecallsDir + "/../config.xml";
	}

	/// find the sample sheet ///
	Directory dir(basecallsDir.c_str());
	std::vector<std::string> csvs = dir.listDirectory(NULL, ".csv");
	if (csvs.size() == 1) {
		sampleSheet = basecallsDir + "/" + csvs[0];
	}
	//TODO: do a smarter search to sample sheets (i.e. filter for 'SampleSheet' and/or allow user to specify)
}

ExperimentLocator::~ExperimentLocator() {
}

/**
 * Retrieve the directory containing the BaseCalls
 */
std::string ExperimentLocator::getBasecallsDir() {
	return basecallsDir;
}

/**
 * Retrieve the filename of the Basecalls Config file
 */
std::string ExperimentLocator::getBasecallsConfig() {
	return basecallsConfig;
}

/**
 * Retrieve the filename of the Intensities Config file
 */
std::string ExperimentLocator::getIntensitiesConfig() {
	return intensitiesConfig;
}

/**
 * Retrieve the filename of the Sample Sheet file
 */
std::string ExperimentLocator::getSampleSheet() {
	return sampleSheet;
}

// ----- Support methods -----


/**
 * Returns true if the path exists and is a directory
 */
bool ExperimentLocator::isDir(const std::string &path) {
	struct stat st;
	return stat(path.c_str(),&st) == 0 && (st.st_mode & S_IFDIR) != 0;
}

/**
 * Returns true if a path exists and is a file
 */
bool ExperimentLocator::isFile(const std::string &path) {
	struct stat st;
	return stat(path.c_str(), &st) == 0;
}

/**
 * Checks if the named file is a Basecalls Config XML file.
 *
 * i.e. is it a valid XML file with a top level node 'BaseCallAnalysis'
 */
bool ExperimentLocator::isBasecallsConfig(const std::string &filename) {
	tinyxml2::XMLDocument doc;
	doc.LoadFile(filename.c_str());

	tinyxml2::XMLElement* nodeBaseCallAnalysis = doc.FirstChildElement("BaseCallAnalysis");
	return nodeBaseCallAnalysis != NULL;
}

/**
 * Checks if the named file is a Intensities Config XML file.
 *
 * i.e. is it a valid XML file with a top level node 'ImageAnalysis'
 */
bool ExperimentLocator::isIntensitiesConfig(const std::string &filename) {
	tinyxml2::XMLDocument doc;
	doc.LoadFile(filename.c_str());

	tinyxml2::XMLElement* nodeBaseCallAnalysis = doc.FirstChildElement("ImageAnalysis");
	return nodeBaseCallAnalysis != NULL;
}


} /* namespace bcltofastq */
