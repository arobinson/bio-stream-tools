/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ClocsReader.cpp
 *
 *  Created on: 24 Nov 2015
 *      Author: arobinson
 */

#include <iostream>

#include <cmath>

#include "ClocsReader.h"

namespace bcltofastq {

#define CLOCSREADER_IMAGE_WIDTH  2048
#define CLOCSREADER_BLOCK_SIZE   25
// MAX_X_BINS = ceil(IMAGE_WIDTH/BLOCK_SIZE)
#define CLOCSREADER_MAX_X_BINS   82


/**
 * Constructor
 */
ClocsReader::ClocsReader(const char* file)
		: LocationReader() {

	status = "No Header";

	// initialise
	xOffset = 0;
	yOffset = 0;
	numBins = 0;
	numBlocks = 0;
	iBin = 0;
	iBlock = 0;
	headerRead = false;

	// open CLOCS file
	pFile = fopen(file,"rb");
	if (pFile) {

		// read header
		unsigned char buf[4];
		size_t result;
		result = fread(&buf,1,1,pFile); // ignore first byte (unused)
		if (result != 1) {fclose(pFile); pFile = NULL; return;}

		// read number of bins
		result = fread(&buf,1,4,pFile);
		if (result != 4) {fclose(pFile); pFile = NULL; return;}
		numBins = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);

		headerRead = true;
		status = "OK";
	}
}

/**
 * Deconstructor
 */
ClocsReader::~ClocsReader() {
	if (pFile != NULL)
		fclose(pFile);
	pFile = NULL;
}

/**
 * Returns true if the file is openable and header read successfully
 */
bool ClocsReader::isOpen() {
	return headerRead;
}

/**
 * Gets the number of Blocks in the file
 */
unsigned int ClocsReader::getNumBins() {
	return numBins;
}

/**
 * Reads an XY pair from CLOCS file and places it in loc (expects 2 element array)
 */
bool ClocsReader::readLocation(unsigned int* loc) {

	if (pFile) {
		unsigned char buf[4];
		size_t result;

		// check if file is exhausted
		if (iBin > numBins) {
			status = "Bins Exhausted";
			return false;
		}

		// consume bins (if needed) until we find one with unread blocks
		while (iBlock >= numBlocks) {

			// update offsets (except first time)
			if (iBin > 0) {
				if (iBin % CLOCSREADER_MAX_X_BINS == 0) {
					xOffset = 0;
					yOffset += CLOCSREADER_BLOCK_SIZE;
				} else
					xOffset += CLOCSREADER_BLOCK_SIZE;
			}

			// read next bin's header
			iBin++;
			// check if file is exhausted
			if (iBin > numBins) {
				status = "Bins Exhausted 2";
				return false;
			}

			result = fread(&buf,1,1,pFile);
			if (result != 1) {status = "Missing block count (corrupt)";  return false;}
			numBlocks = (unsigned int) buf[0];
			iBlock = 0;

//			std::cout << "Bin#:" << iBin << " (" << numBlocks << " blocks)" << std::endl;
		}

		// read block
		++iBlock;

		// read relative location
		result = fread(&buf,1,2,pFile);
		if (result != 2) {status = "Missing Loc (corrupt)"; return false;}

		// convert to absolute location
		loc[0] = 10 * (buf[0]/10.0 + xOffset) + 1000;
		loc[1] = 10 * (buf[1]/10.0 + yOffset) + 1000;

		return true;
	}
	status = "File Closed";

	return false;
}

/**
 * Gets the current status of the reader
 */
std::string ClocsReader::getStatus() {
	return status;
}

/**
 * Gets the current bin number of the reader
 */
size_t ClocsReader::getBinNumber() {
	return iBin;
}

} /* namespace bcltofastq */

