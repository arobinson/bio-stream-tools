/* 
 *  Created on: 27/11/2015
 *      Author: arobinson
 */

#include <string>

#include <sys/stat.h>
#include <unistd.h>

#include "File.h"

namespace bcltofastq {

AbstractFile::AbstractFile(const char* filename) {
	this->filename = _cloneFilename(filename);
}

AbstractFile::AbstractFile(const AbstractFile &file) {
	this->filename = _cloneFilename(file.filename);
}

AbstractFile::~AbstractFile() {
	if (filename != NULL)
		delete filename;
	filename = NULL;
}

/**
 * Checks if filename exists or not
 */
bool AbstractFile::_exists(const char* filename) {
	struct stat st;
	return stat(filename, &st) == 0;
}

/**
 * Checks if filename is readable
 */
bool AbstractFile::_readable(const char* filename) {
	return access(filename, R_OK) == 0;
}

/**
 * Checks if filename is writable
 */
bool AbstractFile::_writable(const char* filename) {
	return access(filename, W_OK) == 0;
}

/**
 * Checks if filename is writable
 */
bool AbstractFile::_writableDir(const char* filename) {
	return access(filename, W_OK|X_OK) == 0;
}

/**
 * Checks if filename is a file
 */
bool AbstractFile::_file(const char* filename) {
	struct stat st;
	return stat(filename, &st) == 0 && S_ISREG(st.st_mode);
}

/**
 * Checks if filename is a directory
 */
bool AbstractFile::_directory(const char* filename) {
	struct stat st;
	return stat(filename, &st) == 0 && S_ISDIR(st.st_mode);
}

/**
 * Makes a copy of the filename
 */
char *AbstractFile::_cloneFilename(const char* filename) {
	char *result = NULL;
	if (filename != NULL) {
		size_t len = strlen(filename);
		result = new char[len+1];
		strcpy(result, filename);
	}
	return result;
}

// ----------------------------------------------------------------------------

File::File(const char* filename) : AbstractFile(filename) {
}

File::~File() {
}

/**
 * Checks if filename exists
 */
bool File::exists() {
	return this->_exists(filename);
}

/**
 * Returns the filename
 */
const char* File::name() {
	return filename;
}

/**
 * Gets filename as a string
 */
std::string File::str() {
	return std::string(filename);
}

} /* namespace bcltofastq */
