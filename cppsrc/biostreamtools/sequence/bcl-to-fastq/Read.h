/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Read.h
 *
 *  Created on: 7 Dec 2015
 *      Author: arobinson
 */

#include <cstring>

#include <libbiostreams/io/OutputBase.h>

#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_READ_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_READ_H_

namespace bcltofastq {

/**
 * Single or paired-end read storage.
 */
class Read {
protected:
	size_t readBytes;
	bool paired;
	char *read1;
	char *read2;
	char *qual1;
	char *qual2;

	size_t headerBytes;
	char *header;

	size_t barcodeBytes;
	unsigned short barcodeCount;
	char *barcodes[2];

	size_t headerReadNumberPosition;
	bool chastityFiltered;


public:
	Read(bool paired = false, unsigned short barcodeCount = 0, size_t readLength = 0, size_t headerLength = 0, size_t barcodeLength = 0);
	virtual ~Read();

	/**
	 * Checks if the read is paired or not
	 */
	bool isPaired();

	/**
	 * Returns the number of barcodes that are used
	 */
	unsigned short getBarcodeCount();

	/**
	 * Set the internal size of the buffers used to (at least) specified sizes.
	 *
	 * Note: sizes allocated will be at least one greater that sizes (for null-term char)
	 */
	void allocate(bool paired, unsigned short barcodeCount, size_t readLength, size_t headerLength, size_t barcodeLength);

	// Getters to access internal memory
	char *getRead1();
	char *getRead2();
	char *getQual1();
	char *getQual2();
	char *getHeader();
	char *getBarcode1();
	char *getBarcode2();

	bool getChastityFiltered();
	void setChastityFiltered(bool filt);

	/**
	 * Set the position where the Read number should be inserted into the header string
	 */
	void setHeaderReadNumberPosition(size_t offset);

	/**
	 * Writes Read 1 (FastQ format) to the output stream
	 */
	bool printRead1(OutputBase* outs);

	/**
	 * Writes Read 2 (FastQ format) to the output stream
	 */
	bool printRead2(OutputBase* outs);
};



} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_READ_H_ */
