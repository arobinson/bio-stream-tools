/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ConfigReader_unittest.cpp
 *
 *  Created on: 1 Dec 2015
 *      Author: arobinson
 */

#include "ExperimentLocator.h"
#include <gtest/gtest.h>


TEST(ExperimentLocator, NewDelete) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/");

	EXPECT_TRUE(exp != NULL);

	delete exp;
}

TEST(ExperimentLocator, NormalExperiment) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1");

	EXPECT_EQ("data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls", exp->getBasecallsDir());
	EXPECT_EQ("data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls/config.xml", exp->getBasecallsConfig());
	EXPECT_EQ("data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls/../config.xml", exp->getIntensitiesConfig());

	delete exp;
}

TEST(ExperimentLocator, AltExperiment1) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test2");

	EXPECT_EQ("data/test/ExperimentLocator/test2/Data/BaseCalls", exp->getBasecallsDir());
	EXPECT_EQ("data/test/ExperimentLocator/test2/Data/BaseCalls/config.xml", exp->getBasecallsConfig());
	EXPECT_EQ("data/test/ExperimentLocator/test2/Data/BaseCalls/../config.xml", exp->getIntensitiesConfig());

	delete exp;
}

TEST(ExperimentLocator, AltExperiment2) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test3");

	EXPECT_EQ("data/test/ExperimentLocator/test3/BaseCalls", exp->getBasecallsDir());
	EXPECT_EQ("data/test/ExperimentLocator/test3/BaseCalls/config.xml", exp->getBasecallsConfig());
	EXPECT_EQ("data/test/ExperimentLocator/test3/BaseCalls/../config.xml", exp->getIntensitiesConfig());

	delete exp;
}

TEST(ExperimentLocator, HintBaseCallConfigFileFullSpec) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "./data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls/alt-config.xml");

	EXPECT_EQ("./data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls/alt-config.xml", exp->getBasecallsConfig());

	delete exp;
}

TEST(ExperimentLocator, HintBaseCallConfigFileRelDirHint) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "Data/Intensities/BaseCalls/alt-config.xml");

	EXPECT_EQ("data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls/alt-config.xml", exp->getBasecallsConfig());

	delete exp;
}

TEST(ExperimentLocator, HintBaseCallConfigDirFullSpec) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "./data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls");

	EXPECT_EQ("./data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls/config.xml", exp->getBasecallsConfig());

	delete exp;
}

TEST(ExperimentLocator, HintBaseCallConfigDirRelDirHint) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "Data/Intensities/BaseCalls");

	EXPECT_EQ("data/test/ExperimentLocator/test1/Data/Intensities/BaseCalls/config.xml", exp->getBasecallsConfig());

	delete exp;
}

TEST(ExperimentLocator, HintIntensitiesConfigFileFullSpec) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "", "./data/test/ExperimentLocator/test1/Data/Intensities/alt-config.xml");

	EXPECT_EQ("./data/test/ExperimentLocator/test1/Data/Intensities/alt-config.xml", exp->getIntensitiesConfig());

	delete exp;
}

TEST(ExperimentLocator, HintIntensitiesConfigFileRelDirHint) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "", "Data/Intensities/alt-config.xml");

	EXPECT_EQ("data/test/ExperimentLocator/test1/Data/Intensities/alt-config.xml", exp->getIntensitiesConfig());

	delete exp;
}

TEST(ExperimentLocator, HintIntensitiesConfigDirFullSpec) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "", "./data/test/ExperimentLocator/test1/Data/Intensities");

	EXPECT_EQ("./data/test/ExperimentLocator/test1/Data/Intensities/config.xml", exp->getIntensitiesConfig());

	delete exp;
}

TEST(ExperimentLocator, HintIntensitiesConfigDirRelDirHint) {

	bcltofastq::ExperimentLocator *exp = new bcltofastq::ExperimentLocator("data/test/ExperimentLocator/test1", "", "Data/Intensities");

	EXPECT_EQ("data/test/ExperimentLocator/test1/Data/Intensities/config.xml", exp->getIntensitiesConfig());

	delete exp;
}

