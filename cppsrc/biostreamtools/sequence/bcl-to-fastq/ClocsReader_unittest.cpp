/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ClocsReader_unittest.cpp
 *
 *  Created on: 24 Nov 2015
 *      Author: arobinson
 */

#include "ClocsReader.h"
#include <gtest/gtest.h>


TEST(ClocsReader, NewDelete) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin5block0.clocs");

	EXPECT_TRUE(reader != NULL);

	delete reader;
}

TEST(ClocsReader, ReadHeader) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin5block0.clocs");

	EXPECT_TRUE(reader->isOpen());

	delete reader;
}

TEST(ClocsReader, BinCount1) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin5block0.clocs");

	EXPECT_EQ(reader->getNumBins(), 5);

	delete reader;
}

TEST(ClocsReader, BinCount2) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin1block2.clocs");

	EXPECT_EQ(reader->getNumBins(), 1);

	delete reader;
}

TEST(ClocsReader, BinCount3) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin0.clocs");

	EXPECT_TRUE(reader->isOpen());

	EXPECT_EQ(reader->getNumBins(), 0);

	delete reader;
}

TEST(ClocsReader, BinCountReal) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/s_1_2316.clocs");

	EXPECT_TRUE(reader->isOpen());
	EXPECT_EQ(reader->getNumBins(), 65600);

	unsigned int buf[2] = {0,0};
	size_t i;
	for (i = 0; reader->getBinNumber() < 65600; ++i) {
		if (!reader->readLocation(buf)) {
			break;
		}
	}

	EXPECT_EQ(65601, reader->getBinNumber());
	EXPECT_FALSE(reader->readLocation(buf));

	delete reader;
}

TEST(ClocsReader, BlockReadReturn) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin1block2.clocs");

	unsigned int buf[2] = {0,0};
	EXPECT_TRUE(reader->readLocation(buf));

	delete reader;
}

TEST(ClocsReader, BlockReadReturnError) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin0.clocs");

	unsigned int buf[2] = {0,0};
	EXPECT_FALSE(reader->readLocation(buf));

	delete reader;
}

TEST(ClocsReader, BlockReadResult1) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin1block2.clocs");

	unsigned int buf[2] = {0,0};
	reader->readLocation(buf);

	EXPECT_EQ(1012, buf[0]);
	EXPECT_EQ(1146, buf[1]);

	delete reader;
}

TEST(ClocsReader, BlockReadResult2) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin1block2.clocs");

	unsigned int buf[2] = {0,0};
	reader->readLocation(buf);
	reader->readLocation(buf);

	EXPECT_EQ(1132, buf[0]);
	EXPECT_EQ(1145, buf[1]);

	delete reader;
}

TEST(ClocsReader, BlockReadResult3Error) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin1block2.clocs");

	unsigned int buf[2] = {0,0};
	reader->readLocation(buf); // success
	reader->readLocation(buf); // success
	EXPECT_FALSE(reader->readLocation(buf)); // past end

	delete reader;
}

TEST(ClocsReader, BlockReadResult10) {

	bcltofastq::ClocsReader *reader = new bcltofastq::ClocsReader("data/test/ClocsReader/bin5block10.clocs");

	unsigned int buf[2] = {0,0};
	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));

	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));
	EXPECT_TRUE(reader->readLocation(buf));

	EXPECT_EQ(2083, buf[0]);
	EXPECT_EQ(1083, buf[1]);

	EXPECT_FALSE(reader->readLocation(buf));

	delete reader;
}

