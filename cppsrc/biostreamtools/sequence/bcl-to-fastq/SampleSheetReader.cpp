/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * SampleSheetReader.cpp
 *
 *  Created on: 18 Dec 2015
 *      Author: arobinson
 */

#include <algorithm>
#include <fstream>
#include <functional>
#include <iostream>
#include <locale>
#include <sstream>
#include <string>
#include <vector>

#include <cctype>

#include "SampleSheetReader.h"

namespace bcltofastq {

SampleSheetReader::SampleSheetReader(std::string filename) {
	this->filename = filename;

	status = NOTREAD;
	_isOpen = false;

	_open();
}

SampleSheetReader::~SampleSheetReader() {

	std::map<int, std::vector<Sample*> >::iterator lit;

	for (lit = samples.begin(); lit != samples.end(); ++lit) {
		std::vector<Sample*>::iterator sit;
		for (sit = lit->second.begin(); sit != lit->second.end(); ++sit)
			delete (*sit);
	}
}

// trim from start
static inline std::string &ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
        return s;
}

// trim from end
static inline std::string &rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
        return s;
}

// trim from both ends
static inline std::string &trim(std::string &s) {
        return ltrim(rtrim(s));
}

/**
 * Splits a string by and delimiter and trims the values
 */
std::vector<std::string> &split(const std::string &s, char delim, std::vector<std::string> &elems) {
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        elems.push_back(trim(item));
    }
    return elems;
}

/**
 * Open's the CSV file and reads it's contents
 */
bool SampleSheetReader::_open() {

	std::ifstream infile(filename.c_str());
	if (!infile.is_open()) {
		status = IOERROR;
		return false;
	}
	status = NOHEADER;

	std::string line;
	bool headerRead = false;
	std::vector<std::string> colNames;
	std::vector<std::string>::iterator cit;
	while (std::getline(infile, line)) {
		if (!headerRead) {
			split(line, ',', colNames);
			headerRead = true;
			status = NOSAMPLES;
		} else {
			std::vector<std::string> values;
			std::vector<std::string>::iterator vit;
			split(line, ',', values);

			Sample *s = new Sample();
			for (vit=values.begin(), cit=colNames.begin(); vit != values.end() && cit != colNames.end(); ++vit, ++cit) {
				s->addValue(*cit, *vit);
			}
			int sid = s->getLaneId();
			if (sid == 0)
				delete s;
			else {
				samples[sid].push_back(s);
				status = READ;
			}
		}
	}
	return true;
}

/**
 * Get the sample details for the selected lane.  Specify 0 for all lanes
 */
std::vector<Sample*> SampleSheetReader::samplesForLane(size_t laneNumber) {
	std::vector<Sample*> result;
	std::map<int, std::vector<Sample*> >::iterator lit;

	if (!_isOpen)
		_open();

	if (laneNumber > 0) {
		lit = samples.find(laneNumber);
		if (lit != samples.end())
			return lit->second;
		return result;
	} else if (laneNumber == 0) {
		for (lit = samples.begin(); lit != samples.end(); ++lit) {
			std::vector<Sample*>::iterator sit;
			for (sit = lit->second.begin(); sit != lit->second.end(); ++sit)
				result.push_back(*sit);
		}
		return result;
	}

	return result;
}

} /* namespace bcltofastq */
