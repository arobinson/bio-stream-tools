/*
 * MultiPartSequence.cpp
 *
 *  Created on: 26 Nov 2015
 *      Author: arobinson
 */

#include "MultiPartSequence.h"

namespace bcltofastq {

MultiPartSequence::MultiPartSequence(size_t headerLength, size_t length, size_t numberParts, size_t* partIndexes) {
	this->headerLength = headerLength;
	this->length = length;
	this->nParts = numberParts;

	// TODO: be safe with length vars

	// allocate memory
	header = new char[headerLength + 1];
	baseSequence = new char[length + numberParts];
	baseQuality = new char[length + numberParts];
	sequenceParts = new char*[numberParts];
	qualityParts = new char*[numberParts];

	// add EOL chars
	header[0] = '\0';
	baseSequence[length + numberParts - 1] = '\0';
	baseQuality[length + numberParts - 1] = '\0';

	// connect part offsets
	for (size_t i = 0; i < numberParts; ++i) {
		sequenceParts[i] = &baseSequence[partIndexes[i] + i];
		qualityParts[i] = &baseQuality[partIndexes[i] + i];
	}
}

MultiPartSequence::~MultiPartSequence() {
	delete header;
	delete baseSequence;
	delete baseQuality;
	delete sequenceParts;
	delete qualityParts;
}


Part::Part(MultiPartSequence *seq, size_t part) {
	this->seq = seq;
	this->part = part;
}

std::ostream &operator<<(std::ostream &out, bcltofastq::Part &c) {
	out << "@" << c.seq->header << std::endl;
	out << c.seq->sequenceParts[c.part] << std::endl;
	out << "+" << std::endl;
	out << c.seq->qualityParts[c.part] << std::endl;
    return out;
}
std::ostream &operator<<(std::ostream &out, bcltofastq::Part *c) {
	out << "@" << c->seq->header << std::endl;
	out << c->seq->sequenceParts[c->part] << std::endl;
	out << "+" << std::endl;
	out << c->seq->qualityParts[c->part] << std::endl;
    return out;
}

OutputBase &operator<<(OutputBase &out, bcltofastq::Part &c) {
	out << "@" << c.seq->header << std::endl;
	out << c.seq->sequenceParts[c.part] << std::endl;
	out << "+" << std::endl;
	out << c.seq->qualityParts[c.part] << std::endl;
    return out;
}
OutputBase &operator<<(OutputBase &out, bcltofastq::Part *c) {
	out << "@" << c->seq->header << std::endl;
	out << c->seq->sequenceParts[c->part] << std::endl;
	out << "+" << std::endl;
	out << c->seq->qualityParts[c->part] << std::endl;
    return out;
}

} /* namespace bcltofastq */
