/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * SampleSheetReader_test.cpp
 *
 *  Created on: 18 Dec 2015
 *      Author: arobinson
 */

#include "SampleSheetReader.h"

#include <vector>
#include <gtest/gtest.h>


TEST(SampleSheetReader, NewDelete) {

	bcltofastq::SampleSheetReader *obj = new bcltofastq::SampleSheetReader("data/test/SampleSheetReader/samples2.csv");

	EXPECT_TRUE(obj != NULL);

	delete obj;
}

TEST(SampleSheetReader, SamplesForLane1Samples2) {

	bcltofastq::SampleSheetReader obj("data/test/SampleSheetReader/samples2.csv");
	std::vector<bcltofastq::Sample*> smps = obj.samplesForLane(1);

	EXPECT_EQ(2, smps.size());
}

TEST(SampleSheetReader, SamplesForLane1MissingLaneColumn) {

	bcltofastq::SampleSheetReader obj("data/test/SampleSheetReader/samples2nolane.csv");
	std::vector<bcltofastq::Sample*> smps = obj.samplesForLane(1);

	EXPECT_EQ(0, smps.size());
}

TEST(SampleSheetReader, SamplesForLane0) {

	bcltofastq::SampleSheetReader obj("data/test/SampleSheetReader/samples2lanes2.csv");
	std::vector<bcltofastq::Sample*> smps = obj.samplesForLane(0);

	EXPECT_EQ(2, smps.size());
}

TEST(SampleSheetReader, SamplesForLane1ValuesSample1) {

	bcltofastq::SampleSheetReader obj("data/test/SampleSheetReader/samples2.csv");
	std::vector<bcltofastq::Sample*> smps = obj.samplesForLane(1);
	std::vector<bcltofastq::Sample*>::iterator it = smps.begin();
	EXPECT_NE(it, smps.end());
	if (it != smps.end()){
		EXPECT_EQ("C2UB9ACXX", (*it)->getValue("FCID"));
		EXPECT_EQ("1", (*it)->getValue("Lane"));
		EXPECT_EQ("", (*it)->getValue("SampleRef"));
		EXPECT_EQ("ATTACTCG-TATAGCCT", (*it)->getValue("Index"));
		EXPECT_EQ("Blue-Ring1", (*it)->getValue("Description"));
		EXPECT_EQ("N", (*it)->getValue("Control"));
		EXPECT_EQ("HiSEQ_120_8_8_120", (*it)->getValue("Recipe"));
		EXPECT_EQ("arobinson", (*it)->getValue("Operator"));
		EXPECT_EQ("UnderSea", (*it)->getValue("SampleProject"));
	}
}
