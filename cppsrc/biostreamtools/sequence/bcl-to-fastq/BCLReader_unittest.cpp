/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ClocsReader_unittest.cpp
 *
 *  Created on: 16 Dec 2015
 *      Author: arobinson
 */

#include "BCLReader.h"
#include <gtest/gtest.h>


TEST(BCLReader, NewDelete) {

	bcltofastq::BCLReader *reader = new bcltofastq::BCLReader("data/test/BCLReader/s_1_1101.bcl");

	EXPECT_TRUE(reader != NULL);

	delete reader;
}

TEST(BCLReader, Size1) {

	bcltofastq::BCLReader reader("data/test/BCLReader/1base.bcl");

	unsigned int size = 0;
	EXPECT_TRUE(reader.readHeader(&size));
	EXPECT_EQ(1, size);
}

TEST(BCLReader, Size2) {

	bcltofastq::BCLReader reader("data/test/BCLReader/2bases.bcl");

	unsigned int size = 0;
	EXPECT_TRUE(reader.readHeader(&size));
	EXPECT_EQ(2, size);
}

TEST(BCLReader, SizeReal) {

	bcltofastq::BCLReader reader("data/test/BCLReader/s_1_1101.bcl");

	unsigned int size = 0;
	EXPECT_TRUE(reader.readHeader(&size));
	EXPECT_EQ(734847, size);
}

TEST(BCLReader, ReadHeaderTwice) {

	bcltofastq::BCLReader reader("data/test/BCLReader/s_1_1101.bcl");

	unsigned int size = 0;
	EXPECT_TRUE(reader.readHeader(&size));
	EXPECT_FALSE(reader.readHeader(&size));
//	EXPECT_EQ(734847, size);
}

TEST(BCLReader, Read1) {

	bcltofastq::BCLReader reader("data/test/BCLReader/1base.bcl");

	char b=0, q=0;
	EXPECT_TRUE(reader.readBaseQual(&b, &q));
	EXPECT_EQ('G', b);
	EXPECT_EQ('%', q);
}

TEST(BCLReader, ReadPastEnd) {

	bcltofastq::BCLReader reader("data/test/BCLReader/1base.bcl");

	char b=0, q=0;
	EXPECT_TRUE(reader.readBaseQual(&b, &q));
	EXPECT_FALSE(reader.readBaseQual(&b, &q));
}

TEST(BCLReader, ReadReal) {

	bcltofastq::BCLReader reader("data/test/BCLReader/s_1_1101.bcl");

	char b=0, q=0;
	EXPECT_TRUE(reader.readBaseQual(&b, &q));
	EXPECT_EQ('T', b);
	EXPECT_EQ('1', q);
}

TEST(BCLReader, ReadWhile1) {

	bcltofastq::BCLReader reader("data/test/BCLReader/1base.bcl");

	int count = 0;
	char b=0, q=0;
	while (reader.readBaseQual(&b, &q))
		count++;

	EXPECT_EQ(1, count);
}

TEST(BCLReader, ReadWhile2) {

	bcltofastq::BCLReader reader("data/test/BCLReader/2bases.bcl");

	int count = 0;
	char b=0, q=0;
	while (reader.readBaseQual(&b, &q))
		count++;

	EXPECT_EQ(2, count);
}

TEST(BCLReader, ReadWhileReal) {

	bcltofastq::BCLReader reader("data/test/BCLReader/s_1_1101.bcl");

	int count = 0;
	char b=0, q=0;
	while (reader.readBaseQual(&b, &q))
		count++;

	EXPECT_EQ(734847, count);
}


