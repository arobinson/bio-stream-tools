/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Demultiplexer.cpp
 *
 *  Created on: 22 Dec 2015
 *      Author: arobinson
 */

#include <sstream>
#include <string>

#include <libbiostreams/io/PlainOutput.h>
#include <libbiostreams/io/ZLibOutput.h>

#include "Demultiplexer.h"

namespace bcltofastq {

/**
 * Removes all non-base characters from barcode
 */
std::string filterBarcode(std::string bc) {
	std::string result = "";
	std::string::iterator it;
	for (it = bc.begin(); it != bc.end(); ++it)
		if (*it == 'A' || *it == 'C' || *it == 'G' || *it == 'T')
			result += (*it);
	return result;
}

void replaceAll(std::string& str, const std::string& from, const std::string& to) {
    if(from.empty())
        return;
    size_t start_pos = 0;
    while((start_pos = str.find(from, start_pos)) != std::string::npos) {
        str.replace(start_pos, from.length(), to);
        start_pos += to.length();
    }
}

/**
 * Generates the filename for the given
 */
std::string generateFilename(std::string &templateFormat, bcltofastq::Sample* sample) {

	std::string filename = templateFormat;

	replaceAll(filename, "FCID", sample->getValue("FCID"));
	replaceAll(filename, "LANE", sample->getValue("Lane"));
	replaceAll(filename, "SAMPLEID", sample->getValue("Sample_ID"));
	replaceAll(filename, "SAMPLEREF", sample->getValue("SampleRef"));
	replaceAll(filename, "INDEX", sample->getValue("Index"));
	replaceAll(filename, "DESCRIPTION", sample->getValue("Description"));
	replaceAll(filename, "CONTROL", sample->getValue("Control"));
	replaceAll(filename, "RECIPE", sample->getValue("Recipe"));
	replaceAll(filename, "OPERATOR", sample->getValue("Operator"));
	replaceAll(filename, "SAMPLEPROJECT", sample->getValue("SampleProject"));

	return filename;
}

// A=0, C=1, G=2, T=3
const unsigned short lookup[] = {
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,

		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,

		0,0,0,1, 0,0,0,2, // @ A B C D E F G
		0,0,0,0, 0,0,0,0, // H I J K L M N O
		0,0,0,0, 3,0,0,0, // P Q R S T U V W
		0,0,0,0, 0,0,0,0, // X Y Z [ \ ] ^ _

		0,0,0,1, 0,0,0,2, // ` a b c d e f g
		0,0,0,0, 0,0,0,0, // h i j k l m n o
		0,0,0,0, 3,0,0,0, // p q r s t u v w
		0,0,0,0, 0,0,0,0, // x y z { | } ~ DEL

		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,

		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,

		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,

		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
		0,0,0,0, 0,0,0,0,
};

/**
 * Converts a barcode (pair) into a unsigned long
 */
unsigned long long indexBarcode(const char* b1, const char* b2) {
	const char *p = b1;
	unsigned long long index = 0;
	if (p != NULL) {
		while (p[0] != '\0') {
			index = index << 2;
			index += lookup[*p];
			p++;
		}
	}
	p = b2;
	if (p != NULL) {
		while (p[0] != '\0') {
			index = index << 2;
			index += lookup[*p];
			p++;
		}
	}
	return index;
}

std::string to_string(long v) {
	std::stringstream ss;
	ss << v;
	return ss.str();
}

Demultiplexer::Demultiplexer(Directory outputDir, std::vector<bcltofastq::Sample*> samples, unsigned short lane, unsigned short mismatches, bool paired) {

	// Create output streams for each barcode
//	std::string outFileTemplate1 = "Project_SAMPLEPROJECT_Sample_SAMPLEID_L00LANE_R1.fq.gz";
//	std::string outFileTemplate2 = "Project_SAMPLEPROJECT_Sample_SAMPLEID_L00LANE_R2.fq.gz";
	std::string outFileTemplate1 = "Project_SAMPLEPROJECT_Sample_SAMPLEID_L00LANE_R1.fq";
	std::string outFileTemplate2 = "Project_SAMPLEPROJECT_Sample_SAMPLEID_L00LANE_R2.fq";
	std::vector<bcltofastq::Sample*>::iterator it;
	for (it = samples.begin(); it != samples.end(); ++it) {

		// only get samples from this lane (or pooled ('0'))
		int li = (*it)->getLaneId();
		if (li == lane || li == 0) {
			std::string bc = filterBarcode((*it)->getValue("Index"));
			unsigned long long bci = indexBarcode(bc.c_str(), NULL);
			OutputFile *of = new OutputFile();
			of->barcode = bc;
//			of->outfile1 = new ZLibOutput(outputDir.newFile(generateFilename(outFileTemplate1, *it))->str());
			of->outfile1 = new PlainOutput(outputDir.newFile(generateFilename(outFileTemplate1, *it))->str());
			of->outfile1->open();
			if (paired) {
//				of->outfile2 = new ZLibOutput(outputDir.newFile(generateFilename(outFileTemplate2, *it))->str());
				of->outfile2 = new PlainOutput(outputDir.newFile(generateFilename(outFileTemplate2, *it))->str());
				of->outfile2->open();
			}
			else
				of->outfile2 = NULL;
			primaryFiles[bci] = of;
			std::cerr << "   - " << bc << " (" << of->outfile1->getFilename();
			if (paired)
				std::cerr << " " << of->outfile2->getFilename();
			std::cerr << ")" << std::endl;
		}
	}


	std::cerr << " - Undetermined Reads:" << std::endl;
	//	std::string undeterminedTemplate1 = "Undetermined_L00LANE_R1.fq.gz";
	//	std::string undeterminedTemplate2 = "Undetermined_L00LANE_R2.fq.gz";
	std::string undeterminedTemplate1 = "Undetermined_L00LANE_R1.fq";
	std::string undeterminedTemplate2 = "Undetermined_L00LANE_R2.fq";
	Sample samp;
	samp.addValue("Lane", to_string(lane));
	std::string undetermined1Filename = outputDir.newFile(generateFilename(undeterminedTemplate1, &samp))->str();
//	undetermined1 = new ZLibOutput(undetermined1Filename);
	undetermined1 = new PlainOutput(undetermined1Filename);
	undetermined1->open();
	std::cerr << "   - 1: " << undetermined1Filename << std::endl;
	if (paired) {
		std::string undetermined2Filename = outputDir.newFile(generateFilename(undeterminedTemplate2, &samp))->str();
//		undetermined2 = new ZLibOutput(undetermined2Filename);
		undetermined2 = new PlainOutput(undetermined2Filename);
		undetermined2->open();
		std::cerr << "   - 2: " << undetermined2Filename << std::endl;
	} else
		undetermined2 = NULL;

	// map each mismatched barcode to correct barcode
	//TODO: implement mismatch expansion

}

Demultiplexer::~Demultiplexer() {
	if (primaryFiles.size() > 0) {
		std::map<unsigned long long, OutputFile*>::iterator it;
		for (it = primaryFiles.begin(); it != primaryFiles.end(); ++it) {
			delete it->second;
		}
		primaryFiles.clear();
	}
}

/**
 * Writes the read into output file for the barcode
 */
bool Demultiplexer::demux(bcltofastq::Read &read) {

	std::map<unsigned long long, OutputFile*>::iterator it;
	std::stringstream ss;
	if (read.getBarcodeCount() >= 1) {
		ss << read.getBarcode1();
		if (read.getBarcodeCount() >= 2)
			ss << read.getBarcode2();
	}
	unsigned long long bci = indexBarcode(read.getBarcode1(), read.getBarcode2());

	it = primaryFiles.find(bci);
	if (it != primaryFiles.end()) {
//		std::cerr << "Read Index: " << read.getBarcode1() << std::endl;
		read.printRead1(it->second->outfile1);
		if (read.isPaired())
			read.printRead2(it->second->outfile2);
	} else { // undetermined
		read.printRead1(undetermined1);
		if (read.isPaired())
			read.printRead2(undetermined2);
	}

	return true;
}

} /* namespace bcltofastq */
