/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Read.cpp
 *
 *  Created on: 7 Dec 2015
 *      Author: arobinson
 */

#include <cstdlib>
#include <cstring>

#include "Read.h"

namespace bcltofastq {

Read::Read(bool paired, unsigned short barcodeCount, size_t readLength, size_t headerLength, size_t barcodeLength) {

	// Initialise
	readBytes = 0;
	this->paired = false;
	read1 = NULL;
	read2 = NULL;
	qual1 = NULL;
	qual2 = NULL;

	headerBytes = 0;
	header = NULL;

	this->barcodeBytes = 0;
	this->barcodeCount = 0;
	barcodes[0] = NULL;
	barcodes[1] = NULL;

	chastityFiltered = false;

	// allocate
	allocate(paired, barcodeCount, readLength, headerLength, barcodeLength);
}

Read::~Read() {
	if (read1 != NULL)
		delete [] read1;
	read1 = NULL;
	if (read2 != NULL)
		delete [] read2;
	read2 = NULL;
	if (qual1 != NULL)
		delete [] qual1;
	qual1 = NULL;
	if (qual2 != NULL)
		delete [] qual2;
	qual2 = NULL;
	if (header != NULL)
		delete [] header;
	header = NULL;
	if (barcodes[0] != NULL)
		delete [] barcodes[0];
	barcodes[0] = NULL;
	if (barcodes[1] != NULL)
		delete [] barcodes[1];
	barcodes[1] = NULL;
}

/**
 * Checks if the read is paired or not
 */
bool Read::isPaired() {
	return paired;
}

/**
 * Returns the number of barcodes that are used
 */
unsigned short Read::getBarcodeCount() {
	return barcodeCount;
}

/**
 * Set the internal size of the buffers used to (at least) specified sizes.
 *
 * Note: sizes allocated will be at least one greater that sizes (for null-term char)
 */
void Read::allocate(bool paired, unsigned short barcodeCount, size_t readLength, size_t headerLength, size_t barcodeLength) {

	if (barcodeCount > 2)
		barcodeCount = 2;

	// resize read/qual
	if (readBytes < (readLength + 1)) {
//		std::cerr << "Extending Read Length to: " << (readLength + 1) << std::endl;

		// resize read1
		char* rtmp = new char[readLength+1];
		char* qtmp = new char[readLength+1];
		rtmp[0] = '\0';
		qtmp[0] = '\0';
		if (read1 != NULL) {
			strncpy(rtmp, read1, readLength);
			delete [] read1;
		}
		if (qual1 != NULL) {
			strncpy(qtmp, qual1, readLength);
			delete [] qual1;
		}
		read1 = rtmp;
		qual1 = qtmp;

		// resize read2 (if necessary)
		if (paired) {
			rtmp = new char[readLength+1];
			qtmp = new char[readLength+1];
			rtmp[0] = '\0';
			qtmp[0] = '\0';
			if (read2 != NULL) {
				strncpy(rtmp, read2, readLength);
				delete [] read2;
			}
			if (qual2 != NULL) {
				strncpy(qtmp, qual2, readLength);
				delete [] qual2;
			}
			read2 = rtmp;
			qual2 = qtmp;
		}

		readBytes = readLength + 1;
	}

	// resize header
	if (headerBytes < (headerLength + 1)) {
//		std::cerr << "Extending Header Length to: " << (headerLength + 1) << std::endl;
		char* htmp = new char[headerLength+1];
		htmp[0] = '\0';
		if (header != NULL) {
			strncpy(htmp, header, headerLength);
			delete [] header;
		}
		header = htmp;

		headerBytes = headerLength + 1;
	}

	// resize barcodes
	if (barcodeBytes < (barcodeLength + 1)) {
//		std::cerr << "Extending Barcode Length to: " << (barcodeLength + 1) << std::endl;
		char* btmp;

		// barcode 1
		if (barcodeCount >= 1) {
			btmp = new char[barcodeLength+1];
			btmp[0] = '\0';
			if (barcodes[0] != NULL) {
				strncpy(btmp, barcodes[0], barcodeLength + 1);
				delete [] barcodes[0];
			}
			barcodes[0] = btmp;
		}
		// barcode 2
		if (barcodeCount == 2) {
			btmp = new char[barcodeLength+1];
			btmp[0] = '\0';
			if (barcodes[1] != NULL) {
				strncpy(btmp, barcodes[1], barcodeLength + 1);
				delete [] barcodes[1];
			}
			barcodes[1] = btmp;
		}

		barcodeBytes = barcodeLength + 1;
	}

	// create / delete 2nd read/seq
	if (this->paired != paired) {
//		std::cerr << "Changing to paired: " << (paired) << std::endl;
		if (paired) {
			if (read2 != NULL)
				read2 = new char[readBytes];
			if (qual2 != NULL)
				qual2 = new char[readBytes];
			read2[0] = '\0';
			qual2[0] = '\0';
		} else {
			if (read2 != NULL) {
				delete [] read2;
				read2 = NULL;
			}
			if (qual2 != NULL) {
				delete [] qual2;
				qual2 = NULL;
			}
		}
		this->paired = paired;
	}

	// create / delete barcode storage
	if (this->barcodeCount != barcodeCount) {
//		std::cerr << "Changing barcode count to: " << (barcodeCount) << std::endl;
		if (barcodeCount == 0) {
			if (barcodes[0] != NULL) {
				delete [] barcodes[0];
				barcodes[0] = NULL;
			}
			if (barcodes[1] != NULL) {
				delete [] barcodes[1];
				barcodes[1] = NULL;
			}
		} else if (barcodeCount == 1) {
			if (barcodes[1] != NULL) { // free barcode 2
				delete [] barcodes[1];
				barcodes[1] = NULL;
			}
			if (this->barcodeCount == 0) { // allocate barcode 1
				barcodes[0] = new char[barcodeBytes];
				barcodes[0][0] = '\0';
			}
		} else { // barcodeCount == 2
			if (this->barcodeCount == 0) { // allocate barcode 1
				barcodes[0] = new char[barcodeBytes];
				barcodes[0][0] = '\0';
			}
			if (this->barcodeCount <= 1) { // allocate barcode 2
				barcodes[1] = new char[barcodeBytes];
				barcodes[1][0] = '\0';
			}
		}

		this->barcodeCount = barcodeCount;
	}

}

char *Read::getRead1() {
	return read1;
}
char *Read::getRead2() {
	return read2;
}
char *Read::getQual1() {
	return qual1;
}
char *Read::getQual2() {
	return qual2;
}
char *Read::getHeader() {
	return header;
}
char *Read::getBarcode1() {
	return barcodes[0];
}
char *Read::getBarcode2() {
	return barcodes[1];
}


bool Read::getChastityFiltered() {
	return chastityFiltered;
}
void Read::setChastityFiltered(bool filt) {
	chastityFiltered = filt;
}


/**
 * Set the position where the Read number should be inserted into the header string
 */
void Read::setHeaderReadNumberPosition(size_t offset) {
	headerReadNumberPosition = offset;
}

/**
 * Writes Read 1 (FastQ format) to the output stream
 */
bool Read::printRead1(OutputBase* outs) {

	header[headerReadNumberPosition] = '1';
	*outs << header;
	if (barcodeCount >= 1 && barcodes[0] != NULL)
		*outs << barcodes[0];
	if (barcodeCount >= 2 && barcodes[1] != NULL)
		*outs << barcodes[1];

	*outs << std::endl << read1 << std::endl;
	*outs << "+" << std::endl;
	*outs << qual1 << std::endl;
	return true;
}

/**
 * Writes Read 2 (FastQ format) to the output stream
 */
bool Read::printRead2(OutputBase* outs) {

	if (read2 == NULL || qual2 == NULL)
		return false;

	header[headerReadNumberPosition] = '2';
	*outs << header;
	if (barcodeCount >= 1 && barcodes[0] != NULL)
		*outs << barcodes[0];
	if (barcodeCount >= 2 && barcodes[1] != NULL)
		*outs << barcodes[1];

	*outs << std::endl << read2 << std::endl;
	*outs << "+" << std::endl;
	*outs << qual2 << std::endl;
	return true;
}

} /* namespace bcltofastq */
