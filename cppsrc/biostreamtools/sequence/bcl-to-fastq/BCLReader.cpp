/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * BCLReader.cpp
 *
 *  Created on: 26 Nov 2015
 *      Author: arobinson
 */

//#include <iostream>

#include "BCLReader.h"

namespace bcltofastq {

BCLReader::BCLReader(const char* file) {

	if (file != NULL)
		this->pFile = fopen (file, "rb");
	else
		this->pFile = NULL;
	headerRead = false;
	baseCount = 0;
	basePos = 0;
}

BCLReader::~BCLReader() {
	if (pFile != NULL)
		fclose(pFile);
	pFile = NULL;
}

bool BCLReader::read4UBytes(unsigned char ** buffer) {
	size_t r;

	if (pFile == NULL)
		return false;

	r = fread(buffer,1,4,pFile);
	if (r == 4) {
		return true;
	}

	return false;
}

/**
 * Reads the header (i.e. the number of sequences in this BCL file)
 */
bool BCLReader::readHeader(unsigned int * result) {

	if (headerRead)
		return false;

	// read base count from header
	unsigned char buf[4];
	headerRead = true;
	if (read4UBytes((unsigned char **)&buf)) {
		baseCount = (unsigned)buf[0] + ((unsigned)buf[1] << 8) + ((unsigned)buf[2] << 16) + ((unsigned)buf[3] << 24);
		*result = baseCount;
		return true;
	}
	return false;
}

/**
 * Reads a single base (& qual) from BCL file
 *
 * Returns (via params) the base (ASCII encoded) and quality (Phred+33 encoded)
 */
bool BCLReader::readBaseQual(char *base, char *qual) {

	// try to read header if not already
	if (!headerRead) {
		unsigned int tmp;
		if (!readHeader(&tmp))
			return false;
	}

	// stop at end
	if (basePos >= baseCount)
		return false;
	basePos++;

	// read base
	size_t r;
	char buffer;
	r = fread(&buffer,1,1,pFile);
	if (r == 1) {
		*base = baseLookup[buffer & 3];
		*qual = ((buffer & 252) >> 2)+33;
		return true;
	}
	return false;
}

// ----------------------------------------------------------------------------

NullBCLReader::NullBCLReader() : BCLReader(NULL) {
	pFile = NULL;
}

NullBCLReader::~NullBCLReader() {
}

bool NullBCLReader::readHeader(unsigned int * result) {
	result = 0;
	return false;
}

bool NullBCLReader::readBaseQual(char *base, char *qual) {
	*base = '\0';
	*qual = '\0';
	return true;
}

} /* namespace bcltofastq */
