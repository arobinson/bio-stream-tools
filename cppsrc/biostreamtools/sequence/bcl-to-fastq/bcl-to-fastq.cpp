/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 17/09/2015
 *      Author: arobinson
 */

#include <algorithm>
#include <bitset>
#include <iostream>
#include <iterator>
#include <vector>
#include <set>
#include <string>

#include <cmath>
#include <cstdio>
#include <sys/stat.h>


#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/io/OutputBase.h>
#include <libbiostreams/readers/SequenceReader.h>
#include <libbiostreams/seq/complement.h>

#include <biostreamtools/version.h>

#include "tinyxml2/tinyxml2.h"

//#include "BCLReader.h"
#include "ConfigReader.h"
//#include "ClocsReader.h"
#include "Demultiplexer.h"
#include "Directory.h"
#include "ExperimentConfig.h"
#include "ExperimentLocator.h"
#include "LaneInfo.h"
#include "LocationReader.h"
//#include "MultiPartSequence.h"
#include "Read.h"
#include "Sample.h"
#include "SampleSheetReader.h"
#include "TileReader.h"




namespace bcltofastq {




/**
 * Returns true if the path exists and is a directory
 */
inline bool isDir(const char *path) {
	struct stat st;
	return stat(path,&st) == 0 && (st.st_mode & S_IFDIR) != 0;
}

/**
 * Returns true if a path exists and is a file
 */
inline bool isFile(const char *path) {
	struct stat st;
	return stat(path, &st) == 0;
}

/**
 * Makes sure val contains 1 or more digits only
 */
inline bool isInt(const std::string &val) {
	for (size_t i = 0; i < val.size(); i++) {
		char c = val.at(i);
		if (c < '0' || c > '9')
			return false;
	}
	return val.size() > 0;
}

/**
 * Extracts to and from values from a string range
 */
inline bool extractRange(const std::string &range, int &from, int &to, int def=0) {

	// check for empty range
	if (range.size() == 0) {
		from = def;
		to = def;
//		std::cout << "Pass: Empty Range" << std::endl;
		return true;
	}
	// check for range splitter
	size_t dashpos = range.find('-');
	if (dashpos == std::string::npos) {
		if (isInt(range)) {
			from = to = atoi(range.c_str());
//			std::cout << "Pass: Single Value" << std::endl;
			return true;
		}
//		std::cout << "Fail: Non-int single value" << std::endl;
		return false;
	}

	// check position makes sense
	if (dashpos == 0 || dashpos == range.size()-1) {
//		std::cout << "Fail: Dash at start or end" << std::endl;
		return false;
	}

	// split range
	std::string fromStr = range.substr(0,dashpos);
	std::string toStr = range.substr(dashpos+1);

	// check range values
	if (!isInt(fromStr) || ! isInt(toStr)) {
//		std::cout << "Fail: from or to non-int" << std::endl;
		return false;
	}

	// do conversion
	from = atoi(fromStr.c_str());
	to = atoi(toStr.c_str());

//	std::cout << "Pass: normal range" << std::endl;
	return true;
}



} /* namespace bcltofastq */


int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("test", 			't', 	new std::string("test"),	0, 1, "Check that all the parameters are correct");
	conf->addFlag("fulltest",		'\0', 	new std::string("full-test"),0,1, "Same as --test with additional check all files are present (takes a long time)");
	conf->addFlag("print", 			'\0', 	new std::string("print"),	0, 1, "Print configuration options on log stream");
	conf->addFlag("chastity",		'c', 	new std::string("keep-chastity"),0, 1, "Output chastity filtered reads as well");

//	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("FASTQ"),	"Set the output format, Accepts FASTQ, FASTA, RAW");
	conf->addOption("source", 		's', 	new std::string("source"), 				0, 1, new std::string("."),		"The root 'run' directory to process (should contain a 'Data' directory)");
	conf->addOption("output", 		'o', 	new std::string("output"), 				0, 1, new std::string("./Unaligned"), "The directory to place demultiplexed read files in");


	// override config options
	conf->addOption("lanes", 		'l', 	new std::string("lanes"), 				0, 1, new std::string("0"),		"Lane numbers to process  e.g. '1' OR, '1-4' etc."); // OR '1,3-8'
	conf->addOption("read1", 		'1', 	new std::string("read1"), 				0, 1, NULL,						"Cycle numbers for read 1  e.g. 1-120");
	conf->addOption("read2", 		'2', 	new std::string("read2"), 				0, 1, NULL,						"Cycle numbers for read 2  e.g. 137-256");
	conf->addOption("barcode1",		'b', 	new std::string("barcode1"), 			0, 1, NULL,						"Cycle numbers for barcode 1  e.g. 121-128");
	conf->addOption("barcode2",		'\0', 	new std::string("barcode2"), 			0, 1, NULL,						"Cycle numbers for barcode 2  e.g. 129-136");

//	conf->addOption("instrument",	'i', 	new std::string("instrument-name"),		0, 1, NULL,						"Override (or provide) the Instrument Name");
//	conf->addOption("runid", 		'r', 	new std::string("run-id"), 				0, 1, NULL,						"Override (or provide) the Run ID");
//	conf->addOption("flowcellid", 	'F', 	new std::string("flowcell-id"), 		0, 1, NULL,						"Override (or provide) the Flowcell ID");


	// streams
//	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),	"Status messages (warning, errors etc.)");

	conf->addSection("Typical Usage", "  bcl-to-fastq --source TOP_LEVEL_EXPERIMENT_DIRECTORY\n");

	conf->setPreamble("Convert BCL files from an Illumina HiSeq or MiSeq run to FastQ format and demultiplex each sample"); // (FastA, or RAW)
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;
	bool scanDirectory = conf->isFlagSet("fulltest");
	bool outputChastity = conf->isFlagSet("chastity");

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 2;

//	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}


	/// do the work ///

	// find the location of files needed
	bcltofastq::ExperimentLocator *expLocator = new bcltofastq::ExperimentLocator(conf->getOptionString("source")); //TODO: add option for specifying config file locations
	std::string basecallsDir = expLocator->getBasecallsDir();

	if (basecallsDir.size() == 0) {
		*logs << std::endl << "Error: unable to find BaseCalls directory.  Exiting." << std::endl;
		*logs << "       I tried:" << std::endl;
		*logs << "        - " << conf->getOptionString("source") << "/Data/Intensities/BaseCalls" << std::endl;
		*logs << "        - " << conf->getOptionString("source") << "/Data/BaseCalls" << std::endl;
		*logs << "        - " << conf->getOptionString("source") << "/Intensities/BaseCalls" << std::endl;
		*logs << "        - " << conf->getOptionString("source") << "/BaseCalls" << std::endl;

		delete expLocator;
		delete conf;
		return 3;
	}

	/// read BaseCalls XML File (if found)
	std::vector<int> lanes;
	std::vector<std::string> tiles;
	std::vector<int> cycles;
	std::string instrument;
	std::string runNumber;
	std::string flowcell;

	std::string basecallsConfigFilename = expLocator->getBasecallsConfig();
	if (basecallsConfigFilename.size() > 0) {
		bcltofastq::ConfigReader *configReader = new bcltofastq::ConfigReader(basecallsConfigFilename.c_str());

		lanes = configReader->getLanes();
		tiles = configReader->getTiles();
		cycles = configReader->getCycles();
		instrument = configReader->getInstrumentName();
		runNumber = configReader->getRunFolderId();
		flowcell = configReader->getFlowcellId();

		delete configReader;
	} else {
		*logs << std::endl << "Error: unable to find config.xml file.  Exiting." << std::endl;
		*logs << "       I tried:" << std::endl;
		*logs << "        - " << basecallsDir << "/config.xml" << std::endl;

		delete expLocator;
		delete conf;
		return 3;
	}

	bcltofastq::Read read;
	bcltofastq::ExperimentConfig expConf(instrument, runNumber, flowcell);
	expConf.baseCallsDir = expLocator->getBasecallsDir();
	expConf.locationDir = expLocator->getBasecallsDir() + "/..";

	// TODO: get these values from config file and/or params
	bcltofastq::extractRange(conf->getOptionString("read1"), expConf.r1start, expConf.r1end);
	bcltofastq::extractRange(conf->getOptionString("read2"), expConf.r2start, expConf.r2end);
	bcltofastq::extractRange(conf->getOptionString("barcode1"), expConf.b1start, expConf.b1end);
	bcltofastq::extractRange(conf->getOptionString("barcode2"), expConf.b2start, expConf.b2end);

	// apply lane filter
	//TODO: allow multiple ranges
	int lfrom = 0, lto = 0;
	bcltofastq::extractRange(conf->getOptionString("lanes"), lfrom, lto);
	if (lfrom > 0) {
		std::vector<int> newlanes;
		std::vector<int>::iterator it;
		for (it=lanes.begin(); it != lanes.end(); ++it)
			if (*it >= lfrom && *it <= lto)
				newlanes.push_back(*it);
		lanes = newlanes;
	}

	/// sort values
	std::sort(lanes.begin(), lanes.end());
	std::sort(cycles.begin(), cycles.end());
//	std::reverse(tiles.begin(), tiles.end());

	/// sample sheet information
	if (expLocator->getSampleSheet().size() == 0) {
		*logs << std::endl << "Error: unable to locate samplesheet.  Exiting." << std::endl;
		*logs << "       Looked in basecall directory for a single '.csv' file" << std::endl;
		*logs << "       '" << basecallsDir << "'" << std::endl;
		*logs << "       If you have two in there I got confused." << std::endl;

		delete expLocator;
		delete conf;
		return 3;
	}
	bcltofastq::SampleSheetReader ssr(expLocator->getSampleSheet());
	if (ssr.getStatus() != bcltofastq::READ && ssr.getStatus() != bcltofastq::NOSAMPLES) {
		*logs << std::endl << "Error: unable to read samplesheet.  Exiting." << std::endl;
		*logs << "       Tried: '" << expLocator->getSampleSheet() << "'" << std::endl;
		switch (ssr.getStatus()) {
		case bcltofastq::IOERROR:
			*logs << "       An IO Error occurred. Maybe it isn't readable or a dead link" << std::endl;
			break;
		case bcltofastq::NOHEADER:
			*logs << "       File appears to be empty or has no header" << std::endl;
			break;
		case bcltofastq::NOTREAD:
			*logs << "       File wasn't read for some reason" << std::endl;
			break;
		default:
			*logs << "       Other unknown reason" << std::endl;
		}

		delete expLocator;
		delete conf;
		return 3;
	}

	/// Print setup
	*logs << "Experiment setup:" << std::endl;
	*logs << " - Config File: " << basecallsConfigFilename << std::endl;
	*logs << " - Lanes:       " << lanes.size() << std::endl;
	std::vector<int>::iterator it;
	for (it=lanes.begin(); it != lanes.end(); ++it)
		*logs << "   - " << (*it) << std::endl;
	*logs << " - Cycles:      " << cycles.size() << std::endl;
	if (expConf.r1start > 0 && expConf.r1start <= expConf.r1end)
		*logs << "   - Read 1:      " << expConf.r1start << "-" << expConf.r1end << std::endl;
	if (expConf.r2start > 0 && expConf.r2start <= expConf.r2end)
		*logs << "   - Read 2:      " << expConf.r2start << "-" << expConf.r2end << std::endl;
	if (expConf.b1start > 0 && expConf.b1start <= expConf.b1end)
		*logs << "   - Barcode 1:   " << expConf.b1start << "-" << expConf.b1end << std::endl;
	if (expConf.b2start > 0 && expConf.b2start <= expConf.b2end)
		*logs << "   - Barcode 2:   " << expConf.b2start << "-" << expConf.b2end << std::endl;
	*logs << " - Tiles:       " << tiles.size() << std::endl;

	// read sample sheet
	std::vector<bcltofastq::Sample*> samples = ssr.samplesForLane(0); // 0 = all
	*logs << " - Samples:     " << samples.size() << std::endl;
	for (it=lanes.begin(); it != lanes.end(); ++it) {
		*logs << "   - Lane " << (*it) << ":" << std::endl;
		std::vector<bcltofastq::Sample*>::iterator sampit;
		for (sampit=samples.begin(); sampit != samples.end(); ++sampit) {
			if ((*it) == (*sampit)->getLaneId()) {
				*logs << "     - " << (*sampit)->getValue("SampleProject") << " -> " << (*sampit)->getValue("Sample_ID") << " (" << (*sampit)->getValue("Index") << ")" << std::endl;
			}
		}
	}


	/// check all files exist
	if (scanDirectory) {
		//TODO: move this into a class
		// ./Data/Intensities/BaseCalls/L001/C1.1/s_1_1101.bcl
		// ./Data/Intensities/BaseCalls/L001/C1.1/s_1_1101.stats
		// ./Data/Intensities/BaseCalls/L001/s_1_1101.control
		// ./Data/Intensities/BaseCalls/L001/s_1_1101.filter
		char lanePath[211];
		strncpy(lanePath, basecallsDir.c_str(), 200);
		size_t lanePathOffset = strlen(lanePath);

		std::vector<int>::iterator itLane;
		for (itLane=lanes.begin(); itLane != lanes.end(); ++itLane) {
			std::vector<int>::iterator itCycle;
			char laneNum[10];

			snprintf(laneNum, 10, "L%03d", *itLane);
			snprintf(&lanePath[lanePathOffset], 10, "/L%03d", *itLane);

			if (bcltofastq::isDir(lanePath)) {
				*logs << "Checking Lane: " << laneNum << " (" << lanePath << ")" << std::endl;

				char cyclePath[222];
				strncpy(cyclePath, lanePath, 211);
				size_t cyclePathOffset = strlen(cyclePath);

				// for each cycle
				for (itCycle=cycles.begin(); itCycle != cycles.end(); ++itCycle) {
					char cycleNum[10];

					snprintf(cycleNum, 10, "C%i.1", *itCycle);
					snprintf(&cyclePath[cyclePathOffset], 10, "/C%i.1", *itCycle);

					if (bcltofastq::isDir(cyclePath)) {

						char bclstatsPath[238];
						strncpy(bclstatsPath, cyclePath, 211);
						size_t bclstatsPathOffset = strlen(bclstatsPath);
						snprintf(&bclstatsPath[bclstatsPathOffset], 11, "/s_%i_", *itLane); bclstatsPathOffset = strlen(bclstatsPath);
						size_t bclstatsPathExtOffset = bclstatsPathOffset + 4;

						// for each tile
						std::vector<std::string>::iterator its;
						for (its=tiles.begin(); its != tiles.end(); ++its) {

							strcpy(&bclstatsPath[bclstatsPathOffset], (*its).c_str());

							strcpy(&bclstatsPath[bclstatsPathExtOffset], ".bcl");
							if (!bcltofastq::isFile(bclstatsPath)) {
								*logs << "Error: BCL file missing   (" << bclstatsPath << ")" << std::endl;
							}

							strcpy(&bclstatsPath[bclstatsPathExtOffset], ".stats");
							if (!bcltofastq::isFile(bclstatsPath)) {
								*logs << "Error: Stats file missing (" << bclstatsPath << ")" << std::endl;
							}
						}
					} else {
						*logs << "Error: Cycle directory missing (" << cyclePath << ")" << std::endl;
					}
				}

				char controlfilterPath[238];
				strncpy(controlfilterPath, lanePath, 211);
				size_t controlfilterPathOffset = strlen(controlfilterPath);
				snprintf(&controlfilterPath[controlfilterPathOffset], 11, "/s_%i_", *itLane); controlfilterPathOffset = strlen(controlfilterPath);
				size_t controlfilterPathExtOffset = controlfilterPathOffset + 4;

				// for each tile
				std::vector<std::string>::iterator its;
				for (its=tiles.begin(); its != tiles.end(); ++its) {

					strcpy(&controlfilterPath[controlfilterPathOffset], (*its).c_str());

					strcpy(&controlfilterPath[controlfilterPathExtOffset], ".control");
					if (!bcltofastq::isFile(controlfilterPath)) {
						*logs << "Error: Control file missing   (" << controlfilterPath << ")" << std::endl;
					}

					strcpy(&controlfilterPath[controlfilterPathExtOffset], ".filter");
					if (!bcltofastq::isFile(controlfilterPath)) {
						*logs << "Error: Filter file missing (" << controlfilterPath << ")" << std::endl;
					}
				}

			} else {
				*logs << "Error: Lane directory missing (" << lanePath << ")" << std::endl;
			}
		}
	}

	if (conf->isFlagSet("test")) {
		*logs << "Test complete (Pass)" << std::endl;
	}
	else {
		/// ----- Process the files -----

		bcltofastq::Directory outdir(conf->getOptionString("output").c_str());

		std::vector<int>::iterator itLane;
		for (itLane=lanes.begin(); itLane != lanes.end(); ++itLane) {

			*logs << "Processing lane: " << *itLane << std::endl;

			*logs << " - Indexes:" << std::endl;
			bcltofastq::Demultiplexer demulti(outdir, samples, *itLane, 0, true);

			bcltofastq::LaneInfo lane(*itLane, cycles);

			// process each tile's sequences
			std::vector<std::string>::iterator its;
			for (its=tiles.begin(); its != tiles.end(); ++its) {
				std::string tile = *its;

				*logs << "Processing tile: " << tile << std::endl;

				// extract reads (i.e. for each read)
				bcltofastq::TileReader tileReader(tile, &lane, &expConf);
				*logs << " - Reads:  " << tileReader.size() << std::endl;

				size_t outputCount = 0;
				while (tileReader.next(&read) != NULL) {

					if (!read.getChastityFiltered() || outputChastity) {
						if (demulti.demux(read))
							outputCount++;
	//					std::cout << read.getRead1() << std::endl;
					}
				}

				*logs << " - Output: " << outputCount << std::endl;

//				break; // only do first tile (testing)
			}
//			break; // only do first lane (testing)
		}

	}

	delete expLocator;






	/*

	/// read LOCS file test
	FILE *pFile = fopen ("../../L001/s_1_1101.locs","rb");
	char buf[4];
	size_t result;
	long lSize;
	unsigned int size = 0;

	result = fread(&buf,1,4,pFile);
	size = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
	std::cout << std::bitset<32>(size) << std::endl;
	result = fread(&buf,1,4,pFile);
	size = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
	std::cout << std::bitset<32>(size) << std::endl;

	result = fread(&buf,1,4,pFile);
	size = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
	std::cout << "Size: " << std::bitset<32>(size) << " " << size << std::endl;

	float pos;
	unsigned posUL;

	union {
	  float f;
	  unsigned char b[4];
	} u;

	for (int r = 0; r < READS; ++r) {
		result = fread(&buf,1,4,pFile);
		u.b[3] = buf[3];
		u.b[2] = buf[2];
		u.b[1] = buf[1];
		u.b[0] = buf[0];
		size = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
		posUL = round(10 * u.f + 1000);
		std::cout << r << ": " << posUL;

		result = fread(&buf,1,4,pFile);
		u.b[3] = buf[3];
		u.b[2] = buf[2];
		u.b[1] = buf[1];
		u.b[0] = buf[0];
		size = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
		posUL = round(10 * u.f + 1000);
		std::cout << ":" << posUL << std::endl;
	}

//	result = fread(&buf,1,4,pFile);
//	u.b[3] = buf[3];
//	u.b[2] = buf[2];
//	u.b[1] = buf[1];
//	u.b[0] = buf[0];
//	size = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
//	posUL = round(10 * u.f + 1000);
//	std::cout << "X2: " << std::bitset<32>(size) << " " << size << " " << posUL << std::endl;
//
//	result = fread(&buf,1,4,pFile);
//	u.b[3] = buf[3];
//	u.b[2] = buf[2];
//	u.b[1] = buf[1];
//	u.b[0] = buf[0];
//	size = buf[0] + (buf[1] << 8) + (buf[2] << 16) + (buf[3] << 24);
//	posUL = round(10 * u.f + 1000);
//	std::cout << "Y2: " << std::bitset<32>(size) << " " << size << " " << posUL << std::endl;

	fclose(pFile);

	std::cout << "--------" << std::endl;

	// */

	*logs << "OK, Done now" << std::endl;

	delete conf;

    return 0;
}

