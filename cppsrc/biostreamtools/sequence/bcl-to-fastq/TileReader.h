/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * TileReader.h
 *
 *  Created on: 7 Dec 2015
 *      Author: arobinson
 */

#include <string>

#include <cstdlib>

#include "BCLReader.h"
#include "ExperimentConfig.h"
#include "FilterReader.h"
#include "LaneInfo.h"
#include "LocationReader.h"
#include "Read.h"

#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_TILEREADER_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_TILEREADER_H_

namespace bcltofastq {

class TileReader {
protected:
	std::string tileName;
	LaneInfo* lane;
	ExperimentConfig *exp;

	const char missingBase;
	const char missingQual;

	bool open;			///< true, if the files have been opened ready to extract reads
	size_t readCount;	///< the total number of reads in the readers
	size_t readNumber;	///< the current position
	bool paired;					///< cache for paired end reads?
	unsigned short barcodeCount;	///< cache for number of barcodes
	size_t maxReadLength;			///< cache for read length
	size_t maxBarcodeLength;		///< cache for barcode length
	size_t headerLength;			///< cache for header length

	std::string readHeaderBase;		///< the common part of sequence ID

	std::vector<bcltofastq::BCLReader*> readersRead1;	///< readers for each BCL file within each Cycle
	std::vector<bcltofastq::BCLReader*> readersRead2;	///< readers for each BCL file within each Cycle
	std::vector<bcltofastq::BCLReader*> readersBarcode1;	///< readers for each BCL file within each Cycle
	std::vector<bcltofastq::BCLReader*> readersBarcode2;	///< readers for each BCL file within each Cycle
	FilterReader *filterReader;						///< the reader for the filter file (chastity)
	LocationReader* locationReader;					///< the read location

	/**
	 * Opens the Tile's files for reading and reads the number of reads that
	 * should be contained within.
	 */
	bool openTile();

	bool isFile(const char* filename);

public:
	TileReader(std::string tileName, LaneInfo* lane, ExperimentConfig *exp);
	virtual ~TileReader();

	/**
	 * Returns the number of Reads in this Tile.
	 */
	size_t size();

	/**
	 * Gets the Read from the Reader.
	 *
	 * Returns NULL if reading read failed (or files exhausted)
	 * If 'out' was provided read information is added to it otherwise and new
	 * instance of PairedRead or SingleRead is returned (and caller must delete it)
	 */
	Read *next(Read* out=NULL);
};

} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_TILEREADER_H_ */
