/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Demultiplexer.h
 *
 *  Created on: 22 Dec 2015
 *      Author: arobinson
 */

#include <map>
#include <string>
#include <vector>

#include <libbiostreams/io/OutputBase.h>

#include "Directory.h"
#include "Read.h"
#include "Sample.h"

#ifndef CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_DEMULTIPLEXER_H_
#define CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_DEMULTIPLEXER_H_

namespace bcltofastq {

std::string filterBarcode(std::string bc);
void replaceAll(std::string& str, const std::string& from, const std::string& to);
std::string generateFilename(std::string &templateFormat, bcltofastq::Sample* sample);
unsigned long long indexBarcode(const char* b1, const char* b2);

class Demultiplexer {
protected:

	struct OutputFile {
		std::string barcode;
		OutputBase *outfile1;
		OutputBase *outfile2;
	};

	std::map<unsigned long long, OutputFile*> primaryFiles;
	OutputBase* undetermined1;
	OutputBase* undetermined2;

public:
	/**
	 * Constructor
	 *
	 * outputDir	directory name to write demultiplexed reads into
	 * samples		vector of samples to demultiplex
	 * mismatches	number of base mismatches to accept in barcodes and still map to a barcode
	 */
	Demultiplexer(Directory outputDir, std::vector<bcltofastq::Sample*> samples, unsigned short lane, unsigned short mismatches, bool paired);
	virtual ~Demultiplexer();

	/**
	 * Writes the read into output file for the barcode
	 */
	bool demux(bcltofastq::Read &read);
};

} /* namespace bcltofastq */

#endif /* CPPSRC_BIOSTREAMTOOLS_SEQUENCE_BCL_TO_FASTQ_DEMULTIPLEXER_H_ */
