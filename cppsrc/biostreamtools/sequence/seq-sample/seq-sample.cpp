/**
 * Copyright (c) 2016, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * seq-sample.cpp
 *
 *  Created on: 05 Sept 2016
 *      Author: arobinson
 */


#include <iostream>
#include <sstream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);

	conf->addFlag("quiet",			'q', 	new std::string("quiet"),				0, 1, "Don't print anything on log stream unless something fails");
	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"Set the input format, Accepts AUTO (default), FASTQ, FASTA, RAW, RAWM");
//	conf->addOption("name", 		'n', 	new std::string("name"), 				0, 1, NULL,						"Override the name used to describe the sequences (in stats)");
//	conf->addOption("quality", 		'q', 	new std::string("quality-offset"),		0, 1, new std::string("33"),	"Set the quality offset (33=Solexa/Ilumina 1.8+, 64=Older)");

	// output control
	conf->addOption("nth", 			'n', 	new std::string("nth"), 				0, 1, new std::string("1"),		"Select the nth sequence from input");

	conf->addOption("offset",		'O', 	new std::string("offset"), 				0, 1, new std::string("0"),		"Skip X number of sequences");
	conf->addOption("max",			'm', 	new std::string("max"), 				0, 1, new std::string("0"),		"Stop outputting after X sequences are output. 0 = no limit");

	// streams
	conf->addInStream("input", 		'\0', 	NULL, 				                    0, 1, NULL,	                "Input Sequences");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'l', 	new std::string("log"), 				0, 1, new std::string("="),	"Statistics summary");

	conf->addAnonymousOption("input", 1, 1, NULL, "Sequence file to operate on.");

	conf->setPreamble("Filters a subset of sequences based on a number of methods:\n"
			" - N'th sequence (with optional starting offset)\n");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0) {
		delete conf;
		return rc - 1;
	}

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 4;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
	std::string format = conf->getOptionString("format");
	if (format != "AUTO" &&
			format != "FASTQ" &&
			format != "FASTA" &&
			format != "RAW" &&
			format != "RAWM") {
		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
		format = "AUTO";
	}

	int nth = conf->getOptionInt("nth");
	if (nth < 1) {
		*logs << "Error: --nth option must be 1 or greater." << std::endl;
		delete conf;
		return 5;
	}
	int offset = conf->getOptionInt("offset");
	if (offset < 0) {
		*logs << "Error: --offset option must be 0 or greater." << std::endl;
		delete conf;
		return 5;
	}
	int max = conf->getOptionInt("max");
	if (max < 0) {
		*logs << "Error: --max option must be 0 or greater." << std::endl;
		delete conf;
		return 5;
	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	bool quiet = conf->isFlagSet("quiet");

	/// do the work ///
	Sequence *seq = NULL;
	SequenceReader *sr = new SequenceReader(ins);
	sr->setType(format);


	unsigned long nTotal = 0;
	unsigned long nTotalConsider = 0;
	unsigned long nOutput = 0;
	bool stopped = false;

	// loop over sequences
	while ((seq = (Sequence*)sr->next()) != NULL) {

		nTotal++;

		// retransmit sequence if required
		if (nTotal > offset) {
			if (nTotalConsider % nth == 0) {
				*outs << seq;
				nOutput++;
			}
			nTotalConsider++;
		}

		delete seq;

		// stop when maximum sequences are output
		if (max > 0 && nOutput >= max) {

			// check if we finished
			stopped = (seq = (Sequence*)sr->next()) != NULL;
			if (seq != NULL)
				delete seq;

			// stop
			break;
		}
	}

	bool complete = true;
	if (max > 0 && nOutput < max)
		complete = false;

	// output stats
	if (!quiet) {
		*logs << "Subset complete:" << std::endl;
		*logs << " - Input:    " << nTotal << (stopped? " *": "") << std::endl;
		*logs << " - Assessed: " << nTotalConsider << (stopped? " *": "") << std::endl;
		*logs << " - Output:   " << nOutput << (complete? "": " $") << std::endl;
		if (stopped)
			*logs << "   * stopped before reading all" << std::endl;
		if (!complete)
			*logs << "   $ less than maximum (" << max <<  ") were output" << std::endl;
	}

	delete sr;
	delete conf;

    return 0;
}



