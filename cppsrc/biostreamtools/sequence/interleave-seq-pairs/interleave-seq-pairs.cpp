/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <iostream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, 	"Print configuration options on log (and continue)");
	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"The format received on input:   [default: AUTO]\n AUTO: detect from first character\n FASTQ\n FASTA\n RAW: one sequence per line\n RAWM: single multi-line raw sequence");

	conf->addInStream("input", 		'\0', 	NULL, 				2, CONFIGURATOR_UNLIMITED, NULL, 					"An even number of sequence file pairs");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				0, 1, new std::string("-"),		"Output stream/file           [default: -]\n -: standard output\n =: standard error");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),		"Log message stream/file      [default: =]\n -: standard output\n =: standard error");

	conf->addAnonymousOption("input", 		2, CONFIGURATOR_UNLIMITED, NULL, 				"N/a");

	conf->setPreamble("Interleave paired end sequence data into a single stream of reads.\n"
			"e.g. <file1seq1> <file2seq1> <file1seq2> <file2seq2> ...\n"
			"\n"
			"Note: If you provide more than one pair of files then it will process\n"
			"the later pairs once the first pair is complete.");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams(OUT|LOG) != 0)
		return 4;

	// treat each input filename uniquely
	conf->unsetCacheInStreams();

	OutputBase* logs = conf->getLogStream("log");
	OutputBase* outs = conf->getOutStream("output");

	/// validate options ///
	std::vector<InputBase*> infiles = conf->getInStreams("input");
	if ((infiles.size() % 2) != 0) { // even number of files
		conf->printUsageSummary();
		*logs << "Error: Expects an even number of input files" << std::endl;
		return 3;
	}
	std::string format = conf->getOptionString("format");
	if (format != "AUTO" &&
			format != "FASTQ" &&
			format != "FASTA" &&
			format != "RAW" &&
			format != "RAWM") {
		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
		format = "AUTO";
	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	unsigned long count = 0;

	for (size_t i = 0; i+1 < infiles.size(); i+=2){
		Sequence *seq1 = NULL;
		Sequence *seq2 = NULL;
		SequenceReader *sr1 = new SequenceReader(infiles[i]);
		SequenceReader *sr2 = new SequenceReader(infiles[i+1]);
		sr1->setType(format);
		sr2->setType(format);

		*logs << "Interleaving " << infiles[i]->getFilename() << " and " << infiles[i+1]->getFilename() << std::endl;

		seq1 = (Sequence*)sr1->next();
		seq2 = (Sequence*)sr2->next();
		while (seq1 != NULL && seq2 != NULL) {
			*outs << seq1;
			*outs << seq2;
			count++;
			delete seq1;
			delete seq2;
			seq1 = (Sequence*)sr1->next();
			seq2 = (Sequence*)sr2->next();
		}
		if (seq1 != NULL) {
			*logs << "Warning: forward file '" << infiles[i]->getFilename() << "' is longer than its pair" << std::endl;
			delete seq1;
		}
		if (seq2 != NULL) {
			*logs << "Warning: reverse file '" << infiles[i+1]->getFilename() << "' is longer than its pair" << std::endl;
			delete seq2;
		}

		delete sr1;
		delete sr2;
	}

	*logs << "Interleaved " << count << " sequence pairs" << std::endl;

	delete conf;

    return 0;
}

