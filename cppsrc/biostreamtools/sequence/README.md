# Tools #
This source directory contains one sub-directory per c++ command line tool.

## Current tools ##

### deinterleave-seq-pairs ###

De-interleave paired end sequence data into two streams of sequences.

  e.g. [input]   <file1seq1> <file2seq1> <file1seq2> <file2seq2> ...
       [output1] <file1seq1> <file1seq2> ...
       [output2] <file2seq1> <file2seq2> ...

Options:

* -i, --input, an interleaved stream of sequence records.  Defaults to stdin (-)
    * --input-compress, set the decompression used on input stream [AUTO (default), PLAIN, BZ2, GZ, XZ]
* output1 output2, a pair of output streams where sequences are placed.  No default
    * --output-compress, set the compression used on output stream [PLAIN (default), BZ2, GZ, XZ]
* -e, --log, the log stream, defaults to strerr (=), to place any log/error messages to.
    * --log-compress, set the compression used on error stream [PLAIN (default), BZ2, GZ, XZ]
* -f, --format, stream format. [AUTO (default), FASTQ, FASTA, RAW, RAWM]
    * AUTO: detect format from first char (@=FASTQ, >= FASTA, Otherwise RAW)
    * FASTQ: standard fastq format
    * FASTA: standard fasta format (handles multiline fasta)
    * RAW: raw dna sequences (each line is a sequences)
    * RAWM: raw dna sequences (one sequence per file, new line chars removed)
* -h, --help, print usage summary and detailed description of application
* --print-config, prints a summary of all options (provided and defaults) on --log stream

### interleave-seq-pairs ###

Takes pairs of sequence files and produces a single stream of output sequences.  Processes
one pair of files at a time and outputs in the form: 

  <file1seq1> <file2seq1> <file1seq2> <file2seq2> ...

Options:

* input, pairs of the input files from which to obtain the sequences from.  No default
    * --input-compress, set the decompression used on input stream [AUTO (default), PLAIN, BZ2, GZ, XZ]
* -o, --output, the output stream, defaults to stdout (-), to which to place processed sequences
    * --output-compress, set the compression used on output stream [PLAIN (default), BZ2, GZ, XZ]
* -e, --log, the log stream, defaults to strerr (=), to place any log/error messages to.
    * --log-compress, set the compression used on error stream [PLAIN (default), BZ2, GZ, XZ]
* -f, --format, stream format. [AUTO (default), FASTQ, FASTA, RAW, RAWM]
    * AUTO: detect format from first char (@=FASTQ, >= FASTA, Otherwise RAW)
    * FASTQ: standard fastq format
    * FASTA: standard fasta format (handles multiline fasta)
    * RAW: raw dna sequences (each line is a sequences)
    * RAWM: raw dna sequences (one sequence per file, new line chars removed)
* -h, --help, print usage summary and detailed description of application
* --print-config, prints a summary of all options (provided and defaults) on --log stream

### nextseq-trim ###

Trims the A..G.. tails from sequences that are typically found on NextSeqTM data.  It can operate
in a paired-end (file pair), paired-end (interleaved) or single-end mode.

When using paired data and the length filter, if either read fails, its pair will be discarded as well.

Usage examples:

```text
// an single end reads (these do the same thing)
$ cat single-end.fq | nextseq-trim > output.fq
$ nextseq-trim single-end.fq > output.fq
$ nextseq-trim single-end.fq --output output.fq

// interleaved paired-end reads
$ cat interleave.fq | nextseq-trim --interleaved > output.fq
$ nextseq-trim --interleaved -o output.fq interleave.fq

// file-pair paired end (interleaved output)
$ nextseq-trim --paired read1.fq read2.fq > output.fq
$ interleave-seq-pairs read1.fq read2.fq | nextseq-trim --interleaved > output.fq
 
// file-pair paired-end (file-pair output)
$ nextseq-trim --paired read1.fq read2.fq | deinterleave-seq-pairs output1.fq output2.fq
```

Options:

* input, files (or pairs of files) from which to obtain the sequences from.  Defaults to stdin (-)
    * --input-compress, set the decompression used on input stream [AUTO (default), PLAIN, BZ2, GZ, XZ]
* -o, --output, the output stream, defaults to stdout (-), to which to place processed sequences
    * --output-compress, set the compression used on output stream [PLAIN (default), BZ2, GZ, XZ]
* -e, --log, the log stream, defaults to strerr (=), to place any log/error messages to.
    * --log-compress, set the compression used on error stream [PLAIN (default), BZ2, GZ, XZ]
* -f, --format, stream format. [AUTO (default), FASTQ, FASTA, RAW, RAWM]
    * AUTO: detect format from first char (@=FASTQ, >= FASTA, Otherwise RAW)
    * FASTQ: standard fastq format
    * FASTA: standard fasta format (handles multiline fasta)
    * RAW: raw dna sequences (each line is a sequences)
    * RAWM: raw dna sequences (one sequence per file, new line chars removed)
* -h, --help, print usage summary and detailed description of application (and exit)
* --print-config, prints a summary of all options (provided and defaults) on --log stream (and continue)
* -I, --interleaved, input files are interleaved paired-end format
* -p, --paired, input files are file-pair paired-end format
* -m, --max-errors, the maximum base mismatches to tolerate in A..G.. tail.  Defaults to 5
* -l, --min-length, the minimum length sequence to keep after trimming.  Defaults to 0 (no length filter)


### reverse-complement ###

Generates a reverse AND/OR complement of input sequences.

Options:

* -i, --input, the input stream, defaults to stdin (-), from which to obtain the sequences from
    * --input-compress, set the decompression used on input stream [AUTO (default), PLAIN, BZ2, GZ, XZ]
* -o, --output, the output stream, defaults to stdout (-), to which to place processed sequences
    * --output-compress, set the compression used on output stream [PLAIN (default), BZ2, GZ, XZ]
* -e, --error, the error stream, defaults to strerr (=), to place any error messages to.
    * --error-compress, set the compression used on error stream [PLAIN (default), BZ2, GZ, XZ]
* -f, --format, stream format. [AUTO (default), FASTQ, FASTA, RAW, RAWM]
    * AUTO: detect format from first char (@=FASTQ, >= FASTA, Otherwise RAW)
    * FASTQ: standard fastq format
    * FASTA: standard fasta format (handles multiline fasta)
    * RAW: raw dna sequences (each line is a sequences)
    * RAWM: raw dna sequences (one sequence per file, new line chars removed)
* --format-out, overides output format, defaults to -f option.
* -h, --help, print usage summary and detailed description of application
* --print-config, prints a summary of all options (provided and defaults) on --log stream

## Development ##

To add additional tools:

 * Add a new sub-directory
 * Add this sub-directory to CMakeLists.txt file
 * Add a new section above to describe your tool



