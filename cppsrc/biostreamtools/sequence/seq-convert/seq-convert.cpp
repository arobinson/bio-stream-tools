/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * seq-convert.cpp
 *
 *  Created on: 3 Aug 2015
 *      Author: arobinson
 *
 *  A tool that converts one or more input sequence files (default stdin) to a
 *  different sequence format and outputs as a single file (default stdout)
 */


#include <iostream>
#include <sstream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>

#include <biostreamtools/version.h>

std::string asFASTQ(Sequence *seq);
std::string asFASTA(Sequence *seq);
std::string asRAW(Sequence *seq);

int main(int argc, char ** argv) {

	/// setup configuration ///
	int returncode = 0;
	Configurator *conf = new Configurator(argc, argv);

	conf->addFlag("quiet",			'q', 	new std::string("quiet"),				0, 1, "Don't print status/progress messages on log stream");
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, "Print configuration options on log (and continue)");

	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("FASTQ"),	"Set the output format, Accepts FASTQ (default), FASTA, RAW");
	conf->addOption("quality", 		'\0', 	new std::string("quality-string"),		0, 1, new std::string("0"),		"Set the quality string when upgrading to fastq.  You can specify \nas many characters as you want; wraps if seq is longer than provided string.\nDefault: '0' (Phred score 15)");

	conf->addInStream("input", 		'\0', 	NULL, 				                    0, 1, NULL,	                "Input Sequences");
	conf->addOutStream("output", 	'o', 	new std::string("output"), 				1, 1, new std::string("-"),	"Output Sequences");
	conf->addLogStream("log", 		'l', 	new std::string("log"), 				0, 1, new std::string("="),	"Log and error messages");

	conf->addAnonymousOption("input", 0, CONFIGURATOR_UNLIMITED, NULL, "0 or more sequence files to operate on.");

	conf->setPreamble(
			"A tool that converts one or more input sequence files (default stdin) to a\n"
			"different sequence format and outputs as a single file (default stdout)");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams(OUT | LOG) != 0)
		return 4;

	InputBase* ins = conf->getInStream("input");
	OutputBase* outs = conf->getOutStream("output");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
	std::string format = conf->getOptionString("format");
	std::string (*outFormat)(Sequence *seq);
	if (format == "FASTQ")
		outFormat = asFASTQ;
	else if (format == "FASTA")
		outFormat = asFASTA;
	else if (format == "RAW")
		outFormat = asRAW;
	else {
		*logs << "Warning: unknown format '" << format << "'.  Changed to FASTQ" << std::endl;
		outFormat = asFASTQ;
		format = FASTQ;
	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	if (!conf->isFlagSet("quiet"))
		*logs << "Format:     " << format << std::endl;

	/// do the work ///
	unsigned long seqCount = 0;
	std::vector<InputBase*> instreams = conf->getInStreams("input");
	std::vector<InputBase*>::iterator iss;

	if (format == "FASTQ") {
		std::string qualstr = conf->getOptionString("quality");
		size_t quallen = qualstr.size();

		if (!conf->isFlagSet("quiet"))
			*logs << "Quality String: " << qualstr << std::endl;

		for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
			if (!conf->isFlagSet("quiet"))
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

			SequenceReader *sr = new SequenceReader(*iss);
			Sequence *seq = NULL;

			// loop over sequences
			while ((seq = (Sequence*)sr->next()) != NULL) {

				// create the quality score if needed
				size_t seqLen = seq->seq.size();
				std::string &qual = seq->qual;
				qual.reserve(seqLen);
				while (qual.size() < seqLen) {
					qual.append(qualstr);
				}
				if (qual.size() > seqLen)
					qual.resize(seqLen);

				// output the sequence
				*outs << asFASTQ(seq);

				seqCount++;
				delete seq;
			}

			delete sr;
		}
	} else {
		for (iss = instreams.begin(); iss != instreams.end(); ++iss) {
			if (!conf->isFlagSet("quiet"))
				*logs << "Processing: " << (*iss)->getFilename() << std::endl;

			SequenceReader *sr = new SequenceReader(*iss);
			Sequence *seq = NULL;

			// loop over sequences
			while ((seq = (Sequence*)sr->next()) != NULL) {

				// output the sequence
				*outs << outFormat(seq);

				seqCount++;
				delete seq;
			}

			delete sr;
		}
	}

	if (!conf->isFlagSet("quiet"))
		*logs << "Processed: " << instreams.size() << " files, " << seqCount << " sequences" << std::endl;

	// cleanup
	delete conf;

    return returncode;
}


/**
 * Returns a stringstream version of sequence in FASTQ format
 */
std::string asFASTQ(Sequence *seq) {
	std::stringstream out;
    out << "@" << seq->id;
    if (seq->annotation.length() > 0)
    	out << " " << seq->annotation;
    out << std::endl << seq->seq << std::endl << "+" << std::endl << seq->qual << std::endl;
    return out.str();
}

/**
 * Returns a stringstream version of sequence in FASTA format
 */
std::string asFASTA(Sequence *seq) {
	std::stringstream out;
    out << ">" << seq->id;
    if (seq->annotation.length() > 0)
    	out << " " << seq->annotation;
    out << std::endl << seq->seq << std::endl;
    return out.str();
}

/**
 * Returns a stringstream version of sequence in RAW format
 */
std::string asRAW(Sequence *seq) {
	std::stringstream out;
    out << seq->seq << std::endl;
    return out.str();
}

