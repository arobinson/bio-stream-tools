/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * deinterleave-seq-pairs.cpp
 *
 *  Created on: 29 Oct 2014
 *      Author: arobinson
 */

#include <cstdio>

#include <iostream>

#include <libbiostreams/config/Configurator.h>
#include <libbiostreams/datatypes/Sequence.h>
#include <libbiostreams/readers/SequenceReader.h>

#include <biostreamtools/version.h>

int main(int argc, char ** argv) {

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, 	"Print configuration options on log (and continue)");
	conf->addOption("format", 		'f', 	new std::string("format"), 				0, 1, new std::string("AUTO"),	"The format received on input:    [default: AUTO]\n AUTO: detect from first character\n FASTQ\n FASTA\n RAW: one sequence per line\n RAWM: single multi-line raw sequence");

	conf->addInStream("input", 		'i', 	new std::string("input"),				0, 1, new std::string("-"), 	"An interleaved sequence stream/file [default: -]\n -: standard input");
	conf->addOutStream("output", 	'\0', 	NULL, 									2, 2, NULL,						"Pair of output stream/files         [default: -]\n -: standard output\n =: standard error");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),		"Log message stream/file             [default: =]\n -: standard output\n =: standard error");

	conf->addAnonymousOption("output", 		2, 2, 									NULL, 							"N/a");

	conf->setPreamble("De-interleave paired end sequence data into two streams of reads.\n"
			"e.g. [input]   <file1seq1> <file2seq1> <file1seq2> <file2seq2> ...\n"
			"     [output1] <file1seq1> <file1seq2> ...\n"
			"     [output2] <file2seq1> <file2seq2> ...");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 4;

	InputBase*  ins = conf->getInStream("input");
	OutputBase* logs = conf->getLogStream("log");

	std::vector<OutputBase*> outs = conf->getOutStreams("output");
	if (outs.size() != 2)
		return 5;
	OutputBase* outs1 = outs[0];
	OutputBase* outs2 = outs[1];

	/// validate options ///
	std::string format = conf->getOptionString("format");
	if (format != "AUTO" &&
			format != "FASTQ" &&
			format != "FASTA" &&
			format != "RAW" &&
			format != "RAWM") {
		*logs << "Warning: unknown format '" << format << "'.  Changed to AUTO" << std::endl;
		format = "AUTO";
	}

	if (conf->isFlagSet("print")) {
		conf->printConfig(logs);
	}

	/// do the work ///
	*logs << "De-Interleaving to " << outs1->getFilename() << " and " << outs2->getFilename() << std::endl;

	unsigned long count = 0;
	Sequence *seq1 = NULL;
	Sequence *seq2 = NULL;

	SequenceReader *sr = new SequenceReader(ins);

	seq1 = (Sequence*)sr->next();
	seq2 = (Sequence*)sr->next();
	while (seq1 != NULL && seq2 != NULL) {
		*outs1 << seq1;
		*outs2 << seq2;
		count++;
		delete seq1;
		delete seq2;
		seq1 = (Sequence*)sr->next();
		seq2 = (Sequence*)sr->next();
	}
	if (seq1 != NULL) {
		*logs << "Warning: an odd number of sequences in '" << ins->getFilename() << "'." << std::endl;
		delete seq1;
	}

	*logs << "De-Interleaved " << count << " sequence pairs" << std::endl;

	/// clean up ///
	delete sr;
	delete conf;

	return 0;
}




