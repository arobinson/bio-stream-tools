/**
 * Copyright (c) 2015, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * demultiplexer.cpp: splits incoming stream (line-by-line) into a number of files based on an identifier.
 *
 *  Created on: 06 Feb 2015
 *      Author: arobinson
 */

#include <cstdio>

#include <iostream>
#include <fstream>

#include <libbiostreams/config/Configurator.h>

#include <biostreamtools/version.h>

struct StreamCount {
	std::ofstream* out = NULL;
	unsigned long count;

	StreamCount(std::ofstream* out, unsigned long count) {this->out = out; this->count = count;}
};

// forward defs //
bool writeLine(std::string &identifier, std::string &line, std::map<std::string, StreamCount> *outfiles, std::string &format);
std::string makeFilename(const std::string &format, const std::string &id);



/**
 *
 */
int main(int argc, char ** argv) {

	int returncode = 0;

	/// setup configuration ///
	Configurator *conf = new Configurator(argc, argv);
	conf->addFlag("print",			'\0', 	new std::string("print-config"),		0, 1, "Print configuration options on log (and continue)");
	conf->addFlag("quiet",			'q', 	new std::string("quiet"),				0, 1, "Don't print status/progress messages on log stream");
	conf->addOption("barcode", 		'b', 	new std::string("barcode"),	  0, CONFIGURATOR_UNLIMITED, NULL,	"If provided, limit barcodes to value.  Can provide more than once.");
	conf->addOption("barcodealpha",	'a', 	new std::string("barcode-alphabet"),	0, 1, new std::string("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"),	"The characters that are in barcodes.  Used to expand options with the --barcode-errors option.");
	conf->addOption("barcodeerror",	'E', 	new std::string("barcode-errors"),		0, 1, new std::string("0"),	"The number of errors to accept in barcodes.");
	conf->addOption("naming", 		'n', 	new std::string("naming"), 				0, 1, new std::string("output_{ID}.txt"),	"Filename format to use for demultiplexed content. [default: 'output_{ID}.txt'] \n{ID} will be replaced with the identifier of the line.");
	conf->addOption("unknownbarcode", 'u', 	new std::string("unknown-barcode"),		0, 1, new std::string(""),	"Used with --barcode to store lines with other barcode under this value.  Set to \"\" to discard unknown barcodes [default: \"\"]");
	conf->addOption("separator", 	's', 	new std::string("separator"),			0, 1, new std::string("\t"),	"String used to separate ID from LINE  [default: '\\t']\n Note: separator CAN exist within LINE but not IDENTIFIER.");

	conf->addInStream("input", 		'i', 	new std::string("input"),				0, 1, new std::string("-"), 	"Input lines to demultiplex [default: -]\n -: standard input");
	//conf->addOutStream("output", 	'\0', 	NULL, 									2, 2, NULL,						"Pair of output stream/files         [default: -]\n -: standard output\n =: standard error");
	conf->addLogStream("log", 		'e', 	new std::string("log"), 				0, 1, new std::string("="),		"Log message and counts stream/file             [default: =]\n -: standard output\n =: standard error");

	//conf->addAnonymousOption("output", 		2, 2, 									NULL, 							"N/a");

	conf->setPreamble("Splits input lines into a number of files based on an identifier at beginning of each line.\n\n"
			"Incoming lines need to be of the following format:\n"
			" IDENTIFIERseparatorLINE\n\n"
			"Where:\n"
			" IDENTIFIER: is the string to separate the lines by\n"
			" separator: is a constant that is used to separate IDENTIFIER from LINE (-s option)\n"
			" LINE: the string to write to the file\n\n"
			"Typical usage:\n"
			" awk '{print $4\"\\t\"$0}' INFILE.txt | demultiplexer --naming \"OUTPUT_{ID}.txt\"");
	conf->setPostamble("Developed by Andrew Robinson");

	conf->setVersionNumber(BIO_STREAM_TOOLS_MAJOR, BIO_STREAM_TOOLS_MINOR, BIO_STREAM_TOOLS_PATCH, QUOTE(BIO_STREAM_TOOLS_VERSION_DATE));

	// parse the options
	unsigned short rc = 0;
	if ((rc = conf->processesOptions()) != 0)
		return rc - 1;

	/// prepare streams ///
	if (conf->connectStreams() != 0)
		return 4;

	InputBase*  ins = conf->getInStream("input");
	OutputBase* logs = conf->getLogStream("log");

	/// validate options ///
	const std::vector<std::string> barcodes = conf->getOptionStrings("barcode");
	int barcodeErrorCount = conf->getOptionInt("barcodeerror");
	std::string barcodesAlpha = conf->getOptionString("barcodealpha");
	if (barcodeErrorCount < 0) {
		*logs << "Error: barcode-errors must be a positive number" << std::endl;
		returncode = 5;
	} else if (barcodeErrorCount > 5) {
		*logs << "Error: barcode-errors less than or equal to 5" << std::endl;
		returncode = 5;
	} else if (barcodeErrorCount > 0 && barcodes.size() == 0) {
		*logs << "Error: you must specify barcodes to use barcode-errors" << std::endl;
		returncode = 6;
	}


	if (returncode == 0) {

		if (conf->isFlagSet("print")) {
			conf->printConfig(logs);
		}



		/// do the work ///
		unsigned long count = 0;
		std::map<std::string, StreamCount> outfiles;
		std::string sep = conf->getOptionString("separator");
		std::string naming = conf->getOptionString("naming");

		if (!conf->isFlagSet("quiet"))
			*logs << "Demultiplexing to " << naming << std::endl;

		// check barcode mode (limited or not)
		if (barcodes.size() > 0) { // limited to set range

			// add exact barcodes
			std::map<std::string, std::string> barcodeMapping;
			std::map<std::string, std::string>::iterator mit;
			std::vector<std::string>::const_iterator bit = barcodes.begin();
			for (; bit != barcodes.end(); ++bit)
				barcodeMapping[*bit] = *bit;

			// add incrementally more errors to barcodes
			if (barcodeErrorCount > 0) {
				std::map<std::string, std::string> barcodeMappingNew;
				for (int i = 0; i < barcodeErrorCount; i++) {
					mit = barcodeMapping.begin();
					for (; mit != barcodeMapping.end(); ++mit) {
						std::string barcodeOld = mit->first;
						for (int pos = 0; pos < barcodeOld.size(); pos++) {
							std::string thisBarcode = barcodeOld;
							for (int a = 0; a < barcodesAlpha.size(); a++) {
								thisBarcode[pos] = barcodesAlpha[a];
								barcodeMappingNew[thisBarcode] = mit->second;
							}
						}
					}
				}
				barcodeMapping.insert(barcodeMappingNew.begin(), barcodeMappingNew.end());
				barcodeMappingNew.clear();
			}

			// testing (print barcode table)
			mit = barcodeMapping.begin();
			*logs << "Barcode Mapping:" << std::endl;
			for (; mit != barcodeMapping.end(); ++mit) {
				*logs << " - " << mit->first << " => " << mit->second << std::endl;
			}

			// for each file in input
			std::string linebuffer = ins->readLine();
			while (!ins->eof()) {

				// split line
				size_t pos = linebuffer.find(sep);
				std::string id = linebuffer.substr(0, pos);
				std::string line = linebuffer.substr(pos+1);

				// lookup actual barcode
				id = barcodeMapping[id];

				// write line to file
				if (!writeLine(id, line, &outfiles, naming)) {
					*logs << "Error: Failed to write to " << makeFilename(naming, id) << std::endl;
					count++; // so the total output can be used to identify the bad line
					break;
				}

				// count and get next line
				count ++;
				linebuffer = ins->readLine();
			}
		}
		else { // expand all codes

			// for each file in input
			std::string linebuffer = ins->readLine();
			while (!ins->eof()) {

				// split line
				size_t pos = linebuffer.find(sep);
				std::string id = linebuffer.substr(0, pos);
				std::string line = linebuffer.substr(pos+1);

				// write line to file
				if (!writeLine(id, line, &outfiles, naming)) {
					*logs << "Error: Failed to write to " << makeFilename(naming, id) << std::endl;
					count++; // so the total output can be used to identify the bad line
					break;
				}

				// count and get next line
				count ++;
				linebuffer = ins->readLine();
			}
		}

		// report stats and close output files
		if (!conf->isFlagSet("quiet"))
			*logs << "Demultiplexed " << count << " lines" << std::endl;
		std::map<std::string, StreamCount>::iterator it;
		for (it = outfiles.begin(); it != outfiles.end(); ++it) {
			if (!conf->isFlagSet("quiet"))
				*logs << (*it).first << ": " << (*it).second.count << std::endl;
			(*it).second.out->close();
			delete (*it).second.out;
		}
	}

	/// clean up ///
	// NOTE: conf must be deleted last as it cleans up stream objects too.
	delete conf;

	return returncode;
}

/**
 * Writes a line to the required file based on its barcode
 */
bool writeLine(std::string &identifier, std::string &line, std::map<std::string, StreamCount> *outfiles, std::string &format) {

	std::map<std::string, StreamCount>::iterator it = outfiles->find(identifier);
	std::ofstream *out;
	if (it == outfiles->end()) {
		std::string filename = makeFilename(format, identifier);
		out = new std::ofstream(filename.c_str());
		if (!out->is_open())
			return false;
		outfiles->insert(std::pair<std::string, StreamCount>(identifier, StreamCount(out, 1)));
	} else {
		(*it).second.count++;
		out = (*it).second.out;
	}
	*out << line << std::endl;

	return true;
}

/**
 * Constructs a filename given its format and id
 *
 * @param format, std::string: the format string
 * @param id, std::string: the identifier to insert into format
 * @return std::string: formatted filename
 */
std::string makeFilename(const std::string &format, const std::string &id) {
	std::string result = format;
	std::string::size_type n = 0;
	const std::string s = "{ID}";
	while ( ( n = result.find( s, n ) ) != std::string::npos )
	{
		result.replace( n, s.size(), id );
	    n += id.size();
	}
	return result;
}
